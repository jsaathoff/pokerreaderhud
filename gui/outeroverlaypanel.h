/////////////////////////////////////////////////////////////////////////////
// Name:        outeroverlaypanel.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     22/06/2011 15:48:52
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _OUTEROVERLAYPANEL_H_
#define _OUTEROVERLAYPANEL_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_OUTEROVERLAYPANEL 10001
#define SYMBOL_OUTEROVERLAYPANEL_STYLE wxFRAME_FLOAT_ON_PARENT|wxFRAME_NO_TASKBAR
#define SYMBOL_OUTEROVERLAYPANEL_TITLE _T("OuterOverlayPanel")
#define SYMBOL_OUTEROVERLAYPANEL_IDNAME ID_OUTEROVERLAYPANEL
#define SYMBOL_OUTEROVERLAYPANEL_SIZE wxDefaultSize
#define SYMBOL_OUTEROVERLAYPANEL_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * OuterOverlayPanel class declaration
 */

class OuterOverlayPanel: public wxFrame
{    
    DECLARE_CLASS( OuterOverlayPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    OuterOverlayPanel();
    OuterOverlayPanel( wxWindow* parent, wxWindowID id = SYMBOL_OUTEROVERLAYPANEL_IDNAME, const wxString& caption = SYMBOL_OUTEROVERLAYPANEL_TITLE, const wxPoint& pos = SYMBOL_OUTEROVERLAYPANEL_POSITION, const wxSize& size = SYMBOL_OUTEROVERLAYPANEL_SIZE, long style = SYMBOL_OUTEROVERLAYPANEL_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_OUTEROVERLAYPANEL_IDNAME, const wxString& caption = SYMBOL_OUTEROVERLAYPANEL_TITLE, const wxPoint& pos = SYMBOL_OUTEROVERLAYPANEL_POSITION, const wxSize& size = SYMBOL_OUTEROVERLAYPANEL_SIZE, long style = SYMBOL_OUTEROVERLAYPANEL_STYLE );

    /// Destructor
    ~OuterOverlayPanel();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// set the Pokerparent
	void SetPokerParent(HWND pokerparent);

////@begin OuterOverlayPanel event handler declarations

    /// wxEVT_LEFT_DOWN event handler for ID_OUTEROVERLAYPANEL
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_OUTEROVERLAYPANEL
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_OUTEROVERLAYPANEL
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_OUTEROVERLAYPANEL
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_OUTEROVERLAYPANEL
    void OnMotion( wxMouseEvent& event );

////@end OuterOverlayPanel event handler declarations

////@begin OuterOverlayPanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end OuterOverlayPanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin OuterOverlayPanel member variables
////@end OuterOverlayPanel member variables

	wxPoint  m_delta;

	HWND m_PokerParent;
};

#endif
    // _OUTEROVERLAYPANEL_H_
