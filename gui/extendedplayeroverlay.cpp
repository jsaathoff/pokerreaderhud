/////////////////////////////////////////////////////////////////////////////
// Name:        extendedplayeroverlay.cpp
// Purpose:
// Author:
// Modified by:
// Created:     03/02/2012 15:08:51
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "extendedplayeroverlay.h"
#include "../pokerreaderhudapp.h"
#include "../../pokerreadercommon/helper/imagehandler.h"
#include "pokerreaderoverlay.h"

////@begin XPM images
////@end XPM images

/*
 * ExtPlayerOverlay type definition
 */

IMPLEMENT_DYNAMIC_CLASS( ExtPlayerOverlay, PlayerOverlayPanel )

/*
 * ExtPlayerOverlay event table definition
 */

BEGIN_EVENT_TABLE( ExtPlayerOverlay, PlayerOverlayPanel )

////@begin ExtPlayerOverlay event table entries
    EVT_LEFT_DOWN( ExtPlayerOverlay::OnLeftDown )
    EVT_LEFT_UP( ExtPlayerOverlay::OnLeftUp )
    EVT_RIGHT_DOWN( ExtPlayerOverlay::OnRightDown )
    EVT_RIGHT_UP( ExtPlayerOverlay::OnRightUp )
    EVT_MOTION( ExtPlayerOverlay::OnMotion )

////@end ExtPlayerOverlay event table entries

END_EVENT_TABLE()

/*
 * ExtPlayerOverlay constructors
 */

ExtPlayerOverlay::ExtPlayerOverlay()
{
    Init();
}

ExtPlayerOverlay::ExtPlayerOverlay( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, pos, size, style);
}

/*
 * ExtPlayerOverlay creator
 */

bool ExtPlayerOverlay::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
////@begin ExtPlayerOverlay creation
    PlayerOverlayPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end ExtPlayerOverlay creation
    return true;
}

/*
 * ExtPlayerOverlay destructor
 */

ExtPlayerOverlay::~ExtPlayerOverlay()
{
////@begin ExtPlayerOverlay destruction
////@end ExtPlayerOverlay destruction
}

/*
 * Member initialisation
 */

void ExtPlayerOverlay::Init()
{
////@begin ExtPlayerOverlay member initialisation
    m_imgPlayerType = NULL;
    m_txtValue7 = NULL;
    m_imgValue1 = NULL;
    m_txtValue1 = NULL;
    m_imgValue2 = NULL;
    m_txtValue2 = NULL;
    m_imgValue3 = NULL;
    m_txtValue3 = NULL;
    m_imgValue4 = NULL;
    m_txtValue4 = NULL;
    m_imgValue5 = NULL;
    m_txtValue5 = NULL;
    m_imgValue6 = NULL;
    m_txtValue6 = NULL;
////@end ExtPlayerOverlay member initialisation
}

/*
 * Control creation for ExtPlayerOverlay
 */

void ExtPlayerOverlay::CreateControls()
{
////@begin ExtPlayerOverlay content construction
    ExtPlayerOverlay* itemPlayerOverlayPanel1 = this;

    this->SetBackgroundColour(wxColour(0, 0, 0));
    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
    itemPlayerOverlayPanel1->SetSizer(itemBoxSizer2);

    wxBoxSizer* itemBoxSizer3 = new wxBoxSizer(wxVERTICAL);
    itemBoxSizer2->Add(itemBoxSizer3, 0, wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgPlayerType = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATIC, wxNullBitmap, wxDefaultPosition, wxSize(27, 19), wxNO_BORDER );
    m_imgPlayerType->SetBackgroundColour(wxColour(0, 0, 0));
    itemBoxSizer3->Add(m_imgPlayerType, 0, wxALIGN_CENTER_HORIZONTAL|wxLEFT, 0);

    m_txtValue7 = new wxStaticText( itemPlayerOverlayPanel1, ID_STATICTEXT, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue7->SetForegroundColour(wxColour(80, 142, 189));
    m_txtValue7->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue7->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemBoxSizer3->Add(m_txtValue7, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 0);

    wxFlexGridSizer* itemFlexGridSizer6 = new wxFlexGridSizer(2, 6, 2, 3);
    itemFlexGridSizer6->AddGrowableRow(0);
    itemFlexGridSizer6->AddGrowableRow(1);
    itemFlexGridSizer6->AddGrowableCol(1);
    itemFlexGridSizer6->AddGrowableCol(3);
    itemFlexGridSizer6->AddGrowableCol(5);
    itemBoxSizer2->Add(itemFlexGridSizer6, 1, wxGROW|wxALL, 0);

    m_imgValue1 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP1, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue1->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue1, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue1 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC1, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue1->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue1->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue1, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue2 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP2, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue2->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue2, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue2 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC2, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue2->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue2->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue2, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue3 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP3, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue3->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue3, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue3 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC3, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue3->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue3->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue3, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue4 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP4, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue4->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue4, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL|wxFIXED_MINSIZE, 0);

    m_txtValue4 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC4, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue4->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue4->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue4, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue5 = new wxStaticBitmap( itemPlayerOverlayPanel1, ID_STATICBITMAP5, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue5->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue5, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL|wxFIXED_MINSIZE, 0);

    m_txtValue5 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC5, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue5->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue5->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue5->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue5, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue6 = new wxStaticBitmap( itemPlayerOverlayPanel1, ID_STATICBITMAP6, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue6->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer6->Add(m_imgValue6, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL|wxFIXED_MINSIZE, 0);

    m_txtValue6 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC6, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue6->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue6->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue6->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer6->Add(m_txtValue6, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    // Connect events and objects
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue7->Connect(ID_STATICTEXT, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue7->Connect(ID_STATICTEXT, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue7->Connect(ID_STATICTEXT, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue7->Connect(ID_STATICTEXT, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue7->Connect(ID_STATICTEXT, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue5->Connect(ID_STATICBITMAP5, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue5->Connect(ID_STATICBITMAP5, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue5->Connect(ID_STATICBITMAP5, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue5->Connect(ID_STATICBITMAP5, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue5->Connect(ID_STATICBITMAP5, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue5->Connect(wxID_STATIC5, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue5->Connect(wxID_STATIC5, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue5->Connect(wxID_STATIC5, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue5->Connect(wxID_STATIC5, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue5->Connect(wxID_STATIC5, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_imgValue6->Connect(ID_STATICBITMAP6, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue6->Connect(ID_STATICBITMAP6, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue6->Connect(ID_STATICBITMAP6, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue6->Connect(ID_STATICBITMAP6, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue6->Connect(ID_STATICBITMAP6, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
    m_txtValue6->Connect(wxID_STATIC6, wxEVT_LEFT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue6->Connect(wxID_STATIC6, wxEVT_LEFT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue6->Connect(wxID_STATIC6, wxEVT_RIGHT_DOWN, wxMouseEventHandler(ExtPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue6->Connect(wxID_STATIC6, wxEVT_RIGHT_UP, wxMouseEventHandler(ExtPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue6->Connect(wxID_STATIC6, wxEVT_MOTION, wxMouseEventHandler(ExtPlayerOverlay::OnMotion), NULL, this);
////@end ExtPlayerOverlay content construction
}

/*
 * wxEVT_LEFT_DOWN event handler for ID_PLAYEROVERLAY
 */

void ExtPlayerOverlay::OnLeftDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_DOWN event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DOWN event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
	*/

	PokerReaderOverlay* theparent;
	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		// get the parent instance
		theparent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);
		theparent->GetEasyDataPopup()->SetValues(&GetPlayerValue());
		theparent->GetEasyDataPopup()->SetPosition(wxPoint( this->GetScreenPosition().x, (this->GetScreenPosition().y + this->GetSize().GetHeight())));
		theparent->GetEasyDataPopup()->Show(true);
		theparent->GetEasyDataPopup()->Raise();
		theparent->Refresh();
		DragDone();
	}

	PlayerOverlayPanel::OnLeftDown(event);
}

/*
 * wxEVT_LEFT_UP event handler for ID_PLAYEROVERLAY
 */

void ExtPlayerOverlay::OnLeftUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_UP event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_UP event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
	*/

	PlayerOverlayPanel::OnLeftUp(event);
}

/*
 * wxEVT_RIGHT_DOWN event handler for ID_PLAYEROVERLAY
 */

void ExtPlayerOverlay::OnRightDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_DOWN event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_DOWN event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
	*/

	PokerReaderOverlay* theparent;
	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		// get the parent instance
		theparent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);
		theparent->GetEasyDataPopup()->SetValues(&GetPlayerValue());
		theparent->GetEasyDataPopup()->SetPosition(wxPoint( this->GetScreenPosition().x, (this->GetScreenPosition().y + this->GetSize().GetHeight())));
		theparent->GetEasyDataPopup()->Show(true);
		theparent->GetEasyDataPopup()->Raise();
		theparent->Refresh();
		DragDone();
	}
	PlayerOverlayPanel::OnRightDown(event);
}

/*
 * wxEVT_RIGHT_UP event handler for ID_PLAYEROVERLAY
 */

void ExtPlayerOverlay::OnRightUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_UP event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_UP event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
	*/
	PlayerOverlayPanel::OnRightUp(event);
}

/*
 * wxEVT_MOTION event handler for ID_PLAYEROVERLAY
 */

void ExtPlayerOverlay::OnMotion( wxMouseEvent& event )
{
	/*
////@begin wxEVT_MOTION event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_MOTION event handler for ID_PLAYEROVERLAY in ExtPlayerOverlay.
	*/

	PlayerOverlayPanel::OnMotion(event);
}

/*
 * Should we show tooltips?
 */

bool ExtPlayerOverlay::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap ExtPlayerOverlay::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin ExtPlayerOverlay bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end ExtPlayerOverlay bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon ExtPlayerOverlay::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin ExtPlayerOverlay icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end ExtPlayerOverlay icon retrieval
}

// sets the username
void ExtPlayerOverlay::FillUserName()
{
	if(!UserName().IsEmpty())
	{
		m_imgPlayerType->SetToolTip(wxString::Format(_T("%s\nSitz: %d\nVisual Seat: %d"), UserName(), this->GetPlayerValue().seat, GetVisualSeat())/*+_T("\n***Bildbeschreibung***")*/);
	}
}

// Paints the standard window
void ExtPlayerOverlay::PreparePlayerOverlay()
{
	/* MOD 24.08.09 JS:
	// get the right bitmap
	wxGetApp().m_Common.GetPlayerImage(m_PlayerStruct->playertype);
	*/
	/*
	// set the bitmap
	m_imgPlayerType->SetBitmap(imgPlayerType);
	*/

	/*if(m_PlayerStruct.isownplayer)
	{
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.imgPlayerEasy);
	}
	else
	{
	// MOD 01.09.09 - show bitmap after 10 hands played
	if(m_PlayerStruct.handcount >= 10)
	{
	// set the bitmap MOD 24.08.09 JS: get bitmap by playertype
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.GetEasyPlayerOverlayImage(m_PlayerStruct.playertype));
	}
	else // show nodata
	{
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.GetEasyPlayerOverlayImage(0));
	}
	}*/

	//Fit(); // MOD 24.08.09 JS: old code

	/* MOD 24.08.09 JS: old code
	if(!bGotName)
	FillUserName();
	*/

	if( (wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") == 0) && (GetPlayerValue().isownplayer > 0))
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetExtendedPlayerOverlayImage(GetPlayerValue().playertype));
	else if( !(wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") == 0) && (GetPlayerValue().isownplayer > 0))
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetExtendedPlayerOverlayImage(13));
	else
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetExtendedPlayerOverlayImage(GetPlayerValue().playertype));

	FillUserName();

	m_txtValue1->SetLabel(GetValue(_T("vpip")));
	m_txtValue2->SetLabel(GetValue(_T("pfr")));
	m_txtValue3->SetLabel(GetValue(_T("ats")));
	m_txtValue4->SetLabel(GetValue(_T("afqf")));
	m_txtValue5->SetLabel(GetValue(_T("afqt")));
	m_txtValue6->SetLabel(GetValue(_T("afqr")));
	m_txtValue7->SetLabel(GetValue(_T("handcount")));

	m_imgValue1->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/vpip.png"))));
	m_imgValue2->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/pfr.png"))));
	m_imgValue3->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/ats.png"))));
	m_imgValue4->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/aff.png"))));
	m_imgValue5->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/aft.png"))));
	m_imgValue6->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/afr.png"))));

	wxLogMessage(wxString::Format(_T("Playername: %s, Seat: %d, Visual-Seat: %d, Position: %dx/%dy"), UserName(), GetSeatNumber(), GetVisualSeat(), GetPosition().x, GetPosition().y));

	SetToolTips();
	Layout();
	Fit();
	Refresh(true);
}

// update the tooltips
void ExtPlayerOverlay::SetToolTips()
{
	m_imgValue1->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("vpip")), LABELVPIP, DESCVPIP));
	m_imgValue2->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("pfr")), LABELPFR, DESCPFR));
	m_imgValue3->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("ats")), LABELATS, DESCATS));
	m_imgValue4->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqf")), LABELAFQF, DESCAFQF));
	m_imgValue5->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqt")), LABELAFQT, DESCAFQT));
	m_imgValue6->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqr")), LABELAFQR, DESCAFQR));

	m_txtValue1->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("vpip")), LABELVPIP, DESCVPIP));
	m_txtValue2->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("pfr")), LABELPFR, DESCPFR));
	m_txtValue3->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("ats")), LABELATS, DESCATS));
	m_txtValue4->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqf")), LABELAFQF, DESCAFQF));
	m_txtValue5->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqt")), LABELAFQT, DESCAFQT));
	m_txtValue6->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("afqr")), LABELAFQR, DESCAFQR));
	m_txtValue7->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("handcount")), LABELHANDS, DESCHANDS));

	m_imgPlayerType->SetToolTip(wxString::Format(_T("%s\n%s"), UserName(), GetPlayerDesc(GetPlayerValue().playertype)));
	SetToolTip(wxString::Format(_T("%s"), UserName()));
}