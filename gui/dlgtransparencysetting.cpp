/////////////////////////////////////////////////////////////////////////////
// Name:        dlgtransparencysetting.cpp
// Purpose:     
// Author:      zsr Verlag
// Modified by: 
// Created:     04/06/2009 14:59:49
// RCS-ID:      
// Copyright:   Copyright (c) 2008 zsr Verlag
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "dlgtransparencysetting.h"
#include "pokerreaderoverlay.h"
#include "../pokerreaderhudapp.h"

////@begin XPM images
////@end XPM images




/*!
 * TransparencySettingDialog type definition
 */

IMPLEMENT_DYNAMIC_CLASS( TransparencySettingDialog, wxDialog )


/*!
 * TransparencySettingDialog event table definition
 */

BEGIN_EVENT_TABLE( TransparencySettingDialog, wxDialog )

////@begin TransparencySettingDialog event table entries
    EVT_SLIDER( ID_SLIDER, TransparencySettingDialog::OnSliderUpdated )

////@end TransparencySettingDialog event table entries

END_EVENT_TABLE()


/*!
 * TransparencySettingDialog constructors
 */

TransparencySettingDialog::TransparencySettingDialog()
{
    Init();
}

TransparencySettingDialog::TransparencySettingDialog( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, caption, pos, size, style);
}


/*!
 * CDlgTransparencySetting creator
 */

bool TransparencySettingDialog::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin TransparencySettingDialog creation
    SetExtraStyle(wxWS_EX_BLOCK_EVENTS);
    wxDialog::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end TransparencySettingDialog creation
   CenterOnParent();

    return true;
}


/*!
 * TransparencySettingDialog destructor
 */

TransparencySettingDialog::~TransparencySettingDialog()
{
////@begin TransparencySettingDialog destruction
////@end TransparencySettingDialog destruction
}


/*!
 * Member initialisation
 */

void TransparencySettingDialog::Init()
{
////@begin TransparencySettingDialog member initialisation
    m_Slider = NULL;
////@end TransparencySettingDialog member initialisation
}


/*!
 * Control creation for CDlgTransparencySetting
 */

void TransparencySettingDialog::CreateControls()
{    
////@begin TransparencySettingDialog content construction
    TransparencySettingDialog* itemDialog1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemDialog1->SetSizer(itemBoxSizer2);

    m_Slider = new wxSlider( itemDialog1, ID_SLIDER, 35, 35, 255, wxDefaultPosition, wxDefaultSize, wxSL_SELRANGE );
    itemBoxSizer2->Add(m_Slider, 1, wxGROW|wxALL, 0);

    wxStdDialogButtonSizer* itemStdDialogButtonSizer4 = new wxStdDialogButtonSizer;

    itemBoxSizer2->Add(itemStdDialogButtonSizer4, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);
    wxButton* itemButton5 = new wxButton( itemDialog1, wxID_OK, _("&OK"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer4->AddButton(itemButton5);

    wxButton* itemButton6 = new wxButton( itemDialog1, wxID_CANCEL, _("&Abbrechen"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer4->AddButton(itemButton6);

    itemStdDialogButtonSizer4->Realize();

////@end TransparencySettingDialog content construction
	CenterOnParent();
}


/*!
 * Should we show tooltips?
 */

bool TransparencySettingDialog::ShowToolTips()
{
    return true;
}

/*!
 * Get bitmap resources
 */

wxBitmap TransparencySettingDialog::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin TransparencySettingDialog bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end TransparencySettingDialog bitmap retrieval
}

/*!
 * Get icon resources
 */

wxIcon TransparencySettingDialog::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin TransparencySettingDialog icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end TransparencySettingDialog icon retrieval
}


/*!
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER
 */

void TransparencySettingDialog::OnSliderUpdated( wxCommandEvent& event )
{
////@begin wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER in CDlgTransparencySetting.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER in CDlgTransparencySetting.
	
	// get parent cast
	PokerReaderOverlay* theParent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);

	// update the transparency
	if(theParent->CanSetTransparent())
	{
		theParent->setWindowTransparency((wxByte)m_Slider->GetValue());
	}
}

void TransparencySettingDialog::SetParentHWND(HWND wnd)
{
	m_ParentHWND = wnd;
}
