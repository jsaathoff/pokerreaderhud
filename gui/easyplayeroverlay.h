/////////////////////////////////////////////////////////////////////////////
// Name:        playeroverlay.h
// Purpose:
// Author:      zsr Verlag
// Modified by:
// Created:     09.01.2009 15:07:28
// RCS-ID:
// Copyright:   Copyright (c) 2008 zsr Verlag
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _PLAYEROVERLAY_H_
#define _PLAYEROVERLAY_H_

/*!
 * Includes
 */

////@begin includes
////@end includes
#include "../../pokerreadercommon/helper/playerrecord.h"
#include "playeroverlaypanel.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_PLAYEROVERLAY 10007
#define wxID_STATICBMP1 10008
#define wxID_STATIC1 10009
#define wxID_STATICBMP2 10010
#define wxID_STATIC2 10011
#define wxID_STATICBMP3 10012
#define wxID_STATIC3 10013
#define wxID_STATICBMP4 10014
#define wxID_STATIC4 10015
#define SYMBOL_EASYPLAYEROVERLAY_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_EASYPLAYEROVERLAY_TITLE _T("PlayerOverlay")
#define SYMBOL_EASYPLAYEROVERLAY_IDNAME ID_PLAYEROVERLAY
#define SYMBOL_EASYPLAYEROVERLAY_SIZE wxDefaultSize
#define SYMBOL_EASYPLAYEROVERLAY_POSITION wxDefaultPosition
////@end control identifiers

/*!
 * EasyPlayerOverlay class declaration
 */

class EasyPlayerOverlay: public PlayerOverlayPanel
{
    DECLARE_DYNAMIC_CLASS( EasyPlayerOverlay )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    EasyPlayerOverlay();
    EasyPlayerOverlay( wxWindow* parent, wxWindowID id = SYMBOL_EASYPLAYEROVERLAY_IDNAME, const wxPoint& pos = SYMBOL_EASYPLAYEROVERLAY_POSITION, const wxSize& size = SYMBOL_EASYPLAYEROVERLAY_SIZE, long style = SYMBOL_EASYPLAYEROVERLAY_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_EASYPLAYEROVERLAY_IDNAME, const wxPoint& pos = SYMBOL_EASYPLAYEROVERLAY_POSITION, const wxSize& size = SYMBOL_EASYPLAYEROVERLAY_SIZE, long style = SYMBOL_EASYPLAYEROVERLAY_STYLE );

    /// Destructor
    ~EasyPlayerOverlay();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// Paints the standard window
	void PreparePlayerOverlay();

	// sets the username
	void FillUserName();

	// update the tooltips
	void SetToolTips();

////@begin EasyPlayerOverlay event handler declarations

    /// wxEVT_LEFT_DOWN event handler for ID_PLAYEROVERLAY
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_PLAYEROVERLAY
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_PLAYEROVERLAY
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_PLAYEROVERLAY
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_PLAYEROVERLAY
    void OnMotion( wxMouseEvent& event );

////@end EasyPlayerOverlay event handler declarations

////@begin EasyPlayerOverlay member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end EasyPlayerOverlay member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin EasyPlayerOverlay member variables
    wxStaticBitmap* m_imgPlayerType;
    wxStaticBitmap* m_imgValue1;
    wxStaticText* m_txtValue1;
    wxStaticBitmap* m_imgValue2;
    wxStaticText* m_txtValue2;
    wxStaticBitmap* m_imgValue3;
    wxStaticText* m_txtValue3;
    wxStaticBitmap* m_imgValue4;
    wxStaticText* m_txtValue4;
////@end EasyPlayerOverlay member variables
private:

	// the image for the player-type
	wxBitmap imgPlayerType;
};

#endif
    // _PLAYEROVERLAY_H_