/////////////////////////////////////////////////////////////////////////////
// Name:        transparencydialog.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     14/10/2011 15:24:03
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _TRANSPARENCYDIALOG_H_
#define _TRANSPARENCYDIALOG_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes
#include "outeroverlaypanel.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_TRANSPARENCYDIALOG 10017
#define ID_SLIDER 10005
#define SYMBOL_TRANSPARENCYDIALOG_STYLE wxFRAME_FLOAT_ON_PARENT|wxFRAME_NO_TASKBAR
#define SYMBOL_TRANSPARENCYDIALOG_TITLE _T("Transparencysetting")
#define SYMBOL_TRANSPARENCYDIALOG_IDNAME ID_TRANSPARENCYDIALOG
#define SYMBOL_TRANSPARENCYDIALOG_SIZE wxDefaultSize
#define SYMBOL_TRANSPARENCYDIALOG_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * TransparencyDialog class declaration
 */

class TransparencyDialog: public OuterOverlayPanel
{    
    DECLARE_CLASS( TransparencyDialog )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    TransparencyDialog();
    TransparencyDialog( wxWindow* parent, wxWindowID id = SYMBOL_TRANSPARENCYDIALOG_IDNAME, const wxString& caption = SYMBOL_TRANSPARENCYDIALOG_TITLE, const wxPoint& pos = SYMBOL_TRANSPARENCYDIALOG_POSITION, const wxSize& size = SYMBOL_TRANSPARENCYDIALOG_SIZE, long style = SYMBOL_TRANSPARENCYDIALOG_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_TRANSPARENCYDIALOG_IDNAME, const wxString& caption = SYMBOL_TRANSPARENCYDIALOG_TITLE, const wxPoint& pos = SYMBOL_TRANSPARENCYDIALOG_POSITION, const wxSize& size = SYMBOL_TRANSPARENCYDIALOG_SIZE, long style = SYMBOL_TRANSPARENCYDIALOG_STYLE );

    /// Destructor
    ~TransparencyDialog();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	bool Show(bool show = true);

////@begin TransparencyDialog event handler declarations

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER
    void OnSliderUpdated( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
    void OnOkClick( wxCommandEvent& event );

    /// wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
    void OnCancelClick( wxCommandEvent& event );

////@end TransparencyDialog event handler declarations

////@begin TransparencyDialog member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end TransparencyDialog member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin TransparencyDialog member variables
    wxSlider* m_Slider;
////@end TransparencyDialog member variables
	byte oldtrans;
	byte trans;
};

#endif
    // _TRANSPARENCYDIALOG_H_
