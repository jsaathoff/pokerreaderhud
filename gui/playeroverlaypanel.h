/////////////////////////////////////////////////////////////////////////////
// Name:        playeroverlaypanel.h
// Purpose:
// Author:
// Modified by:
// Created:     10/03/2010 15:19:23
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _PLAYEROVERLAYPANEL_H_
#define _PLAYEROVERLAYPANEL_H_

#include "overlaypanel.h"
/*!
 * Includes
 */

////@begin includes
////@end includes

#include "../../pokerreadercommon/helper/playerrecord.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_PLAYEROVERLAYPANEL 10016
#define SYMBOL_PLAYEROVERLAYPANEL_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_PLAYEROVERLAYPANEL_TITLE _T("PlayerOverlayPanel")
#define SYMBOL_PLAYEROVERLAYPANEL_IDNAME ID_PLAYEROVERLAYPANEL
#define SYMBOL_PLAYEROVERLAYPANEL_SIZE wxSize(100, 40)
#define SYMBOL_PLAYEROVERLAYPANEL_POSITION wxDefaultPosition
////@end control identifiers

/*!
 * PlayerOverlayPanel class declaration
 */

class PlayerOverlayPanel: public OverlayPanel
{
    DECLARE_DYNAMIC_CLASS( PlayerOverlayPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    PlayerOverlayPanel();
    PlayerOverlayPanel( wxWindow* parent, wxWindowID id = SYMBOL_PLAYEROVERLAYPANEL_IDNAME, const wxPoint& pos = SYMBOL_PLAYEROVERLAYPANEL_POSITION, const wxSize& size = SYMBOL_PLAYEROVERLAYPANEL_SIZE, long style = SYMBOL_PLAYEROVERLAYPANEL_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_PLAYEROVERLAYPANEL_IDNAME, const wxPoint& pos = SYMBOL_PLAYEROVERLAYPANEL_POSITION, const wxSize& size = SYMBOL_PLAYEROVERLAYPANEL_SIZE, long style = SYMBOL_PLAYEROVERLAYPANEL_STYLE );

    /// Destructor
    ~PlayerOverlayPanel();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// return the visual seat/pref-seat
	int GetVisualSeat();

	// return the visual seat/pref-seat
	void SetVisualSeat(int seat);

	// sets the username
	void FillUserName();

	// set the window-username
	void SetUserName(wxString value);

	// Paints the standard window
	void PreparePlayerOverlay();

	// get value from struct
	wxString GetValue(wxString getvalue);

	// update the tooltips
	void SetToolTips();

	int GetSeatNumber() const { return m_nSeatNumber ; }

	void SetSeatNumber(int value) { m_nSeatNumber = value ; }

	wxString UserName() const { return m_sUserName ; }

	PlayerValueRecord GetPlayerValue();

	void SetPlayerValues(PlayerValueRecord playervalues);

////@begin PlayerOverlayPanel event handler declarations

    /// wxEVT_LEFT_DOWN event handler for ID_PLAYEROVERLAYPANEL
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_PLAYEROVERLAYPANEL
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_PLAYEROVERLAYPANEL
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_PLAYEROVERLAYPANEL
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_PLAYEROVERLAYPANEL
    void OnMotion( wxMouseEvent& event );

////@end PlayerOverlayPanel event handler declarations

////@begin PlayerOverlayPanel member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end PlayerOverlayPanel member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

	void OnEraseBackground( wxEraseEvent& event );

	void DragDone();

////@begin PlayerOverlayPanel member variables
////@end PlayerOverlayPanel member variables

private:
	int m_nVisualSeat;
	int m_nSeatNumber;
	wxString m_sUserName;

	// struct with playervalues
	PlayerValueRecord m_PlayerStruct;
};

#endif
    // _PLAYEROVERLAYPANEL_H_