/////////////////////////////////////////////////////////////////////////////
// Name:        transparencydialog.cpp
// Purpose:
// Author:
// Modified by:
// Created:     14/10/2011 15:24:03
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "transparencydialog.h"
#include "pokerreaderoverlay.h"
#include "../pokerreaderhudapp.h"

////@begin XPM images
////@end XPM images

/*
 * TransparencyDialog type definition
 */

IMPLEMENT_CLASS( TransparencyDialog, OuterOverlayPanel )

/*
 * TransparencyDialog event table definition
 */

BEGIN_EVENT_TABLE( TransparencyDialog, OuterOverlayPanel )

////@begin TransparencyDialog event table entries
    EVT_SLIDER( ID_SLIDER, TransparencyDialog::OnSliderUpdated )

    EVT_BUTTON( wxID_OK, TransparencyDialog::OnOkClick )

    EVT_BUTTON( wxID_CANCEL, TransparencyDialog::OnCancelClick )

////@end TransparencyDialog event table entries

END_EVENT_TABLE()

/*
 * TransparencyDialog constructors
 */

TransparencyDialog::TransparencyDialog()
{
    Init();
}

TransparencyDialog::TransparencyDialog( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}

/*
 * TransparencyDialog creator
 */

bool TransparencyDialog::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin TransparencyDialog creation
    OuterOverlayPanel::Create( parent, id, caption, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end TransparencyDialog creation
    return true;
}

/*
 * TransparencyDialog destructor
 */

TransparencyDialog::~TransparencyDialog()
{
////@begin TransparencyDialog destruction
////@end TransparencyDialog destruction
}

/*
 * Member initialisation
 */

void TransparencyDialog::Init()
{
////@begin TransparencyDialog member initialisation
    m_Slider = NULL;
////@end TransparencyDialog member initialisation
}

/*
 * Control creation for TransparencyDialog
 */

void TransparencyDialog::CreateControls()
{
////@begin TransparencyDialog content construction
    TransparencyDialog* itemOuterOverlayPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemOuterOverlayPanel1->SetSizer(itemBoxSizer2);

    m_Slider = new wxSlider( itemOuterOverlayPanel1, ID_SLIDER, 35, 35, 255, wxDefaultPosition, wxDefaultSize, wxSL_SELRANGE );
    itemBoxSizer2->Add(m_Slider, 1, wxGROW|wxALL, 0);

    wxStdDialogButtonSizer* itemStdDialogButtonSizer4 = new wxStdDialogButtonSizer;

    itemBoxSizer2->Add(itemStdDialogButtonSizer4, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, 5);
    wxButton* itemButton5 = new wxButton( itemOuterOverlayPanel1, wxID_OK, _("&OK"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer4->AddButton(itemButton5);

    wxButton* itemButton6 = new wxButton( itemOuterOverlayPanel1, wxID_CANCEL, _("&Abbrechen"), wxDefaultPosition, wxDefaultSize, 0 );
    itemStdDialogButtonSizer4->AddButton(itemButton6);

    itemStdDialogButtonSizer4->Realize();

////@end TransparencyDialog content construction

	oldtrans = static_cast<byte>(_wtoi(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.c_str()));
}

/*
 * Should we show tooltips?
 */

bool TransparencyDialog::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap TransparencyDialog::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin TransparencyDialog bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end TransparencyDialog bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon TransparencyDialog::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin TransparencyDialog icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end TransparencyDialog icon retrieval
}

/*
 * wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER
 */

void TransparencyDialog::OnSliderUpdated( wxCommandEvent& event )
{
	PokerReaderOverlay * overlay = wxStaticCast(GetParent(), PokerReaderOverlay);
	trans = (byte)m_Slider->GetValue();
	overlay->setWindowTransparency(trans);
/*
////@begin wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER in TransparencyDialog.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER in TransparencyDialog.
*/
}

/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK
 */

void TransparencyDialog::OnOkClick( wxCommandEvent& event )
{
	wchar_t slidervalue[MAX_PATH];
	PokerReaderOverlay * overlay = wxStaticCast(GetParent(), PokerReaderOverlay);
	wxGetApp().ConfigSetting[CONF_HUDTRANS].value.clear();
	_itow(m_Slider->GetValue(), slidervalue, 10);
	wxGetApp().ConfigSetting[CONF_HUDTRANS].value = slidervalue;
	wxGetApp().ConfigSetting[CONF_HUDTRANS].settingname = CONF_HUDTRANS;
	overlay->OnConfigChange(wxGetApp().ConfigSetting[CONF_HUDTRANS]);
	Hide();
/*
////@begin wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK in TransparencyDialog.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_OK in TransparencyDialog.
*/
}

/*
 * wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL
 */

void TransparencyDialog::OnCancelClick( wxCommandEvent& event )
{
	wchar_t oldtransbuffer[MAX_PATH];
	_itow(static_cast<int>(oldtrans), oldtransbuffer, 10);
	wxGetApp().ConfigSetting[CONF_HUDTRANS].value = oldtransbuffer;
	Hide();
/*
////@begin wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL in TransparencyDialog.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_COMMAND_BUTTON_CLICKED event handler for wxID_CANCEL in TransparencyDialog.
*/
}

bool TransparencyDialog::Show(bool show)
{
	m_Slider->SetValue(_wtoi(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.c_str()));
	oldtrans = static_cast<byte>(_wtoi(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.c_str()));
	CenterOnParent();
	OuterOverlayPanel::Show(show);

	return show;
}