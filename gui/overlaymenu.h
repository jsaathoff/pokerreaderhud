/////////////////////////////////////////////////////////////////////////////
// Name:        overlaymenu.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     09/03/2010 15:46:07
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _OVERLAYMENU_H_
#define _OVERLAYMENU_H_


/*!
 * Includes
 */

////@begin includes
////@end includes
#include "overlaypanel.h"
//#include "../icons/menu_normal.xpm"

/*!
 * Forward declarations
 */

////@begin forward declarations
class OverlayMenu;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_PANELMENUOVERLAY 10002
#define wxID_MENUBITMAP 10003
#define SYMBOL_OVERLAYMENU_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_OVERLAYMENU_IDNAME ID_PANELMENUOVERLAY
#define SYMBOL_OVERLAYMENU_SIZE wxSize(29, 25)
#define SYMBOL_OVERLAYMENU_POSITION wxPoint(150, 150)
////@end control identifiers


/*!
 * OverlayMenu class declaration
 */

class OverlayMenu: public OverlayPanel
{    
    DECLARE_DYNAMIC_CLASS( OverlayMenu )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    OverlayMenu();
    OverlayMenu(wxWindow* parent, wxWindowID id = ID_PANELMENUOVERLAY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSUNKEN_BORDER|wxTAB_TRAVERSAL);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = ID_PANELMENUOVERLAY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxSUNKEN_BORDER|wxTAB_TRAVERSAL);

    /// Destructor
    ~OverlayMenu();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin OverlayMenu event handler declarations

    /// wxEVT_PAINT event handler for ID_PANELMENUOVERLAY
    void OnPaint( wxPaintEvent& event );

    /// wxEVT_ERASE_BACKGROUND event handler for ID_PANELMENUOVERLAY
    void OnEraseBackground( wxEraseEvent& event );

    /// wxEVT_LEFT_DOWN event handler for ID_PANELMENUOVERLAY
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_PANELMENUOVERLAY
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_PANELMENUOVERLAY
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_PANELMENUOVERLAY
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_PANELMENUOVERLAY
    void OnMotion( wxMouseEvent& event );

    /// wxEVT_LEFT_DCLICK event handler for wxID_MENUBITMAP
    void OnLeftDClick( wxMouseEvent& event );

////@end OverlayMenu event handler declarations

////@begin OverlayMenu member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end OverlayMenu member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin OverlayMenu member variables
    wxStaticBitmap* m_bmpMenu;
////@end OverlayMenu member variables

};

#endif
    // _OVERLAYMENU_H_
