/////////////////////////////////////////////////////////////////////////////
// Name:        extendedplayeroverlay.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     03/02/2012 15:08:51
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _EXTENDEDPLAYEROVERLAY_H_
#define _EXTENDEDPLAYEROVERLAY_H_


/*!
 * Includes
 */

////@begin includes
////@end includes

#include "playeroverlaypanel.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_EXTPLAYEROVERLAY 10007
#define ID_STATICTEXT 10000
#define wxID_STATICBMP1 10008
#define wxID_STATIC1 10009
#define wxID_STATICBMP2 10010
#define wxID_STATIC2 10011
#define wxID_STATICBMP3 10012
#define wxID_STATIC3 10013
#define wxID_STATICBMP4 10014
#define wxID_STATIC4 10015
#define ID_STATICBITMAP5 10001
#define wxID_STATIC5 10002
#define ID_STATICBITMAP6 10003
#define wxID_STATIC6 10004
#define SYMBOL_EXTPLAYEROVERLAY_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_EXTPLAYEROVERLAY_TITLE _T("ExtendedPlayerOverlay")
#define SYMBOL_EXTPLAYEROVERLAY_IDNAME ID_EXTPLAYEROVERLAY
#define SYMBOL_EXTPLAYEROVERLAY_SIZE wxDefaultSize
#define SYMBOL_EXTPLAYEROVERLAY_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * ExtPlayerOverlay class declaration
 */

class ExtPlayerOverlay: public PlayerOverlayPanel
{    
    DECLARE_DYNAMIC_CLASS( ExtPlayerOverlay )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    ExtPlayerOverlay();
    ExtPlayerOverlay( wxWindow* parent, wxWindowID id = SYMBOL_EXTPLAYEROVERLAY_IDNAME, const wxPoint& pos = SYMBOL_EXTPLAYEROVERLAY_POSITION, const wxSize& size = SYMBOL_EXTPLAYEROVERLAY_SIZE, long style = SYMBOL_EXTPLAYEROVERLAY_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_EXTPLAYEROVERLAY_IDNAME, const wxPoint& pos = SYMBOL_EXTPLAYEROVERLAY_POSITION, const wxSize& size = SYMBOL_EXTPLAYEROVERLAY_SIZE, long style = SYMBOL_EXTPLAYEROVERLAY_STYLE );

    /// Destructor
    ~ExtPlayerOverlay();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// Paints the standard window
	void PreparePlayerOverlay();

	// sets the username
	void FillUserName();

	// update the tooltips
	void SetToolTips();

////@begin ExtPlayerOverlay event handler declarations

    /// wxEVT_LEFT_DOWN event handler for ID_EXTPLAYEROVERLAY
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_EXTPLAYEROVERLAY
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_EXTPLAYEROVERLAY
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_EXTPLAYEROVERLAY
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_EXTPLAYEROVERLAY
    void OnMotion( wxMouseEvent& event );

////@end ExtPlayerOverlay event handler declarations

////@begin ExtPlayerOverlay member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end ExtPlayerOverlay member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin ExtPlayerOverlay member variables
    wxStaticBitmap* m_imgPlayerType;
    wxStaticText* m_txtValue7;
    wxStaticBitmap* m_imgValue1;
    wxStaticText* m_txtValue1;
    wxStaticBitmap* m_imgValue2;
    wxStaticText* m_txtValue2;
    wxStaticBitmap* m_imgValue3;
    wxStaticText* m_txtValue3;
    wxStaticBitmap* m_imgValue4;
    wxStaticText* m_txtValue4;
    wxStaticBitmap* m_imgValue5;
    wxStaticText* m_txtValue5;
    wxStaticBitmap* m_imgValue6;
    wxStaticText* m_txtValue6;
////@end ExtPlayerOverlay member variables
};

#endif
    // _EXTENDEDPLAYEROVERLAY_H_
