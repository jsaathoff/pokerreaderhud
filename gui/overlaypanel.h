/////////////////////////////////////////////////////////////////////////////
// Name:        overlaypanel.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     04/03/2010 15:55:06
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _OVERLAYPANEL_H_
#define _OVERLAYPANEL_H_


/*!
 * Includes
 */
#include <wx/dcbuffer.h>

/*!
 * Forward declarations
 */

class OverlayPanel;


/*!
 * Control identifiers
 */


#define ID_OVERLAYPANEL 10001
#define SYMBOL_WXOVERLAYPANEL_STYLE wxNO_BORDER|wxTAB_TRAVERSAL
#define SYMBOL_WXOVERLAYPANEL_IDNAME ID_OVERLAYPANEL
#define SYMBOL_WXOVERLAYPANEL_SIZE wxSize(29, 25)
#define SYMBOL_WXOVERLAYPANEL_POSITION wxPoint(50, 50)



/*!
 * wxOverlayPanel class declaration
 */

class OverlayPanel: public wxPanel
{    
    DECLARE_DYNAMIC_CLASS( OverlayPanel )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    OverlayPanel();
    OverlayPanel(wxWindow* parent, wxWindowID id = ID_OVERLAYPANEL, const wxPoint& pos = wxPoint(0, 0), const wxSize& size = wxSize(40, 40), long style = wxNO_BORDER|wxTAB_TRAVERSAL);

    /// Creation
    bool Create(wxWindow* parent, wxWindowID id = ID_OVERLAYPANEL, const wxPoint& pos = wxPoint(0, 0), const wxSize& size = wxSize(40, 40), long style = wxNO_BORDER|wxTAB_TRAVERSAL);

    /// Destructor
    ~OverlayPanel();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// get mouse reverse value
	bool IsMouseReversed();

	// get mouse reverse value
	void ReverseMouse(bool reverse = true);

	double GetXCoord();
	
	void SetXCoord(double value); 
	
	double GetYCoord();
	
	void SetYCoord(double value); 

	wxRegion GetWindowRegion();

	virtual void SetPosition(const wxPoint& pt);

    /// wxEVT_PAINT event handler for ID_OVERLAYPANEL
    void OnPaint( wxPaintEvent& event );

    /// wxEVT_IDLE event handler for ID_OVERLAYPANEL
    void OnIdle( wxIdleEvent& event );

    /// wxEVT_ERASE_BACKGROUND event handler for ID_OVERLAYPANEL
    void OnEraseBackground( wxEraseEvent& event );

    /// wxEVT_LEFT_DOWN event handler for ID_OVERLAYPANEL
    void OnLeftDown( wxMouseEvent& event );

    /// wxEVT_LEFT_UP event handler for ID_OVERLAYPANEL
    void OnLeftUp( wxMouseEvent& event );

    /// wxEVT_RIGHT_DOWN event handler for ID_OVERLAYPANEL
    void OnRightDown( wxMouseEvent& event );

    /// wxEVT_RIGHT_UP event handler for ID_OVERLAYPANEL
    void OnRightUp( wxMouseEvent& event );

    /// wxEVT_MOTION event handler for ID_OVERLAYPANEL
    void OnMotion( wxMouseEvent& event );

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );

    /// Should we show tooltips?
    static bool ShowToolTips();

private:

	// reverse mouse
	bool m_bmousereversed;

	wxPoint m_start_object_pos;
	wxPoint m_start_mouse_pos;

	//int m_nXCoord;
	//int m_nYCoord; 

	double m_nYCoord;
	double m_nXCoord;

protected:
	bool m_bPosCheck;

};

#endif
    // _OVERLAYPANEL_H_
