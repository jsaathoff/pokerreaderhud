/////////////////////////////////////////////////////////////////////////////
// Name:        playeroverlay.cpp
// Purpose:
// Author:      zsr Verlag
// Modified by:
// Created:     09.01.2009 15:07:28
// RCS-ID:
// Copyright:   Copyright (c) 2008 zsr Verlag
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"
#include "../pokerreaderhudapp.h"
#include "../../pokerreadercommon/helper/imagehandler.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "easyplayeroverlay.h"
#include "pokerreaderoverlay.h"

/*
////@begin XPM images
////@end XPM images
*/

#include "../pokerreaderhudapp.h"

/*
 * EasyPlayerOverlay type definition
 */

IMPLEMENT_DYNAMIC_CLASS( EasyPlayerOverlay, PlayerOverlayPanel )

/*
 * EasyPlayerOverlay event table definition
 */

BEGIN_EVENT_TABLE( EasyPlayerOverlay, PlayerOverlayPanel )

////@begin EasyPlayerOverlay event table entries
    EVT_LEFT_DOWN( EasyPlayerOverlay::OnLeftDown )
    EVT_LEFT_UP( EasyPlayerOverlay::OnLeftUp )
    EVT_RIGHT_DOWN( EasyPlayerOverlay::OnRightDown )
    EVT_RIGHT_UP( EasyPlayerOverlay::OnRightUp )
    EVT_MOTION( EasyPlayerOverlay::OnMotion )

////@end EasyPlayerOverlay event table entries

END_EVENT_TABLE()

/*
 * EasyPlayerOverlay constructors
 */

EasyPlayerOverlay::EasyPlayerOverlay()
{
    Init();
}

EasyPlayerOverlay::EasyPlayerOverlay( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, pos, size, style);
}

/*
 * CEasyPlayerOverlay creator
 */

bool EasyPlayerOverlay::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
////@begin EasyPlayerOverlay creation
    PlayerOverlayPanel::Create( parent, id, pos, size, style );

    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
    Centre();
////@end EasyPlayerOverlay creation
    return true;
}

/*
 * EasyPlayerOverlay destructor
 */

EasyPlayerOverlay::~EasyPlayerOverlay()
{
////@begin EasyPlayerOverlay destruction
////@end EasyPlayerOverlay destruction
}

/*
 * Member initialisation
 */

void EasyPlayerOverlay::Init()
{
////@begin EasyPlayerOverlay member initialisation
    m_imgPlayerType = NULL;
    m_imgValue1 = NULL;
    m_txtValue1 = NULL;
    m_imgValue2 = NULL;
    m_txtValue2 = NULL;
    m_imgValue3 = NULL;
    m_txtValue3 = NULL;
    m_imgValue4 = NULL;
    m_txtValue4 = NULL;
////@end EasyPlayerOverlay member initialisation
}

/*
 * Control creation for CEasyPlayerOverlay
 */

void EasyPlayerOverlay::CreateControls()
{
////@begin EasyPlayerOverlay content construction
    EasyPlayerOverlay* itemPlayerOverlayPanel1 = this;

    this->SetBackgroundColour(wxColour(0, 0, 0));
    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
    itemPlayerOverlayPanel1->SetSizer(itemBoxSizer2);

    m_imgPlayerType = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATIC, wxNullBitmap, wxDefaultPosition, wxSize(58, 39), wxNO_BORDER );
    m_imgPlayerType->SetBackgroundColour(wxColour(0, 0, 0));
    itemBoxSizer2->Add(m_imgPlayerType, 1, wxALIGN_TOP|wxLEFT, 0);

    wxFlexGridSizer* itemFlexGridSizer4 = new wxFlexGridSizer(2, 4, 2, 3);
    itemFlexGridSizer4->AddGrowableRow(0);
    itemFlexGridSizer4->AddGrowableRow(1);
    itemFlexGridSizer4->AddGrowableCol(1);
    itemFlexGridSizer4->AddGrowableCol(3);
    itemBoxSizer2->Add(itemFlexGridSizer4, 1, wxGROW|wxALL, 0);

    m_imgValue1 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP1, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue1->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer4->Add(m_imgValue1, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue1 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC1, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue1->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue1->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer4->Add(m_txtValue1, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue2 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP2, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue2->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer4->Add(m_imgValue2, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue2 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC2, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue2->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue2->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer4->Add(m_txtValue2, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue3 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP3, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue3->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer4->Add(m_imgValue3, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL, 0);

    m_txtValue3 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC3, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue3->SetForegroundColour(wxColour(255, 255, 255));
    m_txtValue3->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer4->Add(m_txtValue3, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    m_imgValue4 = new wxStaticBitmap( itemPlayerOverlayPanel1, wxID_STATICBMP4, wxNullBitmap, wxDefaultPosition, wxSize(11, 17), 0 );
    m_imgValue4->SetBackgroundColour(wxColour(0, 0, 0));
    itemFlexGridSizer4->Add(m_imgValue4, 0, wxALIGN_LEFT|wxALIGN_TOP|wxALL|wxFIXED_MINSIZE, 0);

    m_txtValue4 = new wxStaticText( itemPlayerOverlayPanel1, wxID_STATIC4, _("0"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
    m_txtValue4->SetForegroundColour(wxColour(80, 142, 189));
    m_txtValue4->SetBackgroundColour(wxColour(0, 0, 0));
    m_txtValue4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    itemFlexGridSizer4->Add(m_txtValue4, 1, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxALL, 0);

    // Connect events and objects
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_imgPlayerType->Connect(wxID_STATIC, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue1->Connect(wxID_STATICBMP1, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue1->Connect(wxID_STATIC1, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue2->Connect(wxID_STATICBMP2, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue2->Connect(wxID_STATIC2, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue3->Connect(wxID_STATICBMP3, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue3->Connect(wxID_STATIC3, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_imgValue4->Connect(wxID_STATICBMP4, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_LEFT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnLeftDown), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_LEFT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnLeftUp), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_RIGHT_DOWN, wxMouseEventHandler(EasyPlayerOverlay::OnRightDown), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_RIGHT_UP, wxMouseEventHandler(EasyPlayerOverlay::OnRightUp), NULL, this);
    m_txtValue4->Connect(wxID_STATIC4, wxEVT_MOTION, wxMouseEventHandler(EasyPlayerOverlay::OnMotion), NULL, this);
////@end EasyPlayerOverlay content construction
}

/*
 * Should we show tooltips?
 */

bool EasyPlayerOverlay::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap EasyPlayerOverlay::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin EasyPlayerOverlay bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end EasyPlayerOverlay bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon EasyPlayerOverlay::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin EasyPlayerOverlay icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end EasyPlayerOverlay icon retrieval
}

// sets the username
void EasyPlayerOverlay::FillUserName()
{
	if(!UserName().IsEmpty())
	{
		m_imgPlayerType->SetToolTip(wxString::Format(_T("%s\nSitz: %d\nVisual Seat: %d"), UserName(), this->GetPlayerValue().seat, GetVisualSeat())/*+_T("\n***Bildbeschreibung***")*/);
	}
}

// Paints the standard window
void EasyPlayerOverlay::PreparePlayerOverlay()
{
	/* MOD 24.08.09 JS:
	// get the right bitmap
	wxGetApp().m_Common.GetPlayerImage(m_PlayerStruct->playertype);
	*/
	/*
	// set the bitmap
	m_imgPlayerType->SetBitmap(imgPlayerType);
	*/

	/*if(m_PlayerStruct.isownplayer)
	{
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.imgPlayerEasy);
	}
	else
	{
	// MOD 01.09.09 - show bitmap after 10 hands played
	if(m_PlayerStruct.handcount >= 10)
	{
	// set the bitmap MOD 24.08.09 JS: get bitmap by playertype
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.GetEasyPlayerOverlayImage(m_PlayerStruct.playertype));
	}
	else // show nodata
	{
	m_imgPlayerType->SetBitmap(wxGetApp().m_Common.GetEasyPlayerOverlayImage(0));
	}
	}*/

	//Fit(); // MOD 24.08.09 JS: old code

	/* MOD 24.08.09 JS: old code
	if(!bGotName)
	FillUserName();
	*/

	if( (wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") == 0) && (GetPlayerValue().isownplayer > 0))
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetEasyPlayerOverlayImage(GetPlayerValue().playertype));
	else if( !(wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") == 0) && (GetPlayerValue().isownplayer > 0))
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetEasyPlayerOverlayImage(13));
	else
		m_imgPlayerType->SetBitmap(wxGetApp().GetImages()->GetEasyPlayerOverlayImage(GetPlayerValue().playertype));

	FillUserName();

	m_txtValue1->SetLabel(GetValue(_T("vpip")));
	m_txtValue2->SetLabel(GetValue(_T("pfr")));
	m_txtValue3->SetLabel(GetValue(_T("af")));
	m_txtValue4->SetLabel(GetValue(_T("handcount")));

	m_imgValue1->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/vpip.png"))));
	m_imgValue2->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/pfr.png"))));
	m_imgValue3->SetBitmap(wxBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/af.png"))));
	m_imgValue4->SetBitmap(wxBitmap(wxNullBitmap));

	wxLogMessage(wxString::Format(_T("Playername: %s, Seat: %d, Visual-Seat: %d, Position: %dx/%dy"), UserName(), GetSeatNumber(), GetVisualSeat(), GetPosition().x, GetPosition().y));

	SetToolTips();
	Layout();
	Fit();
	Refresh(true);
}

/*
 * wxEVT_LEFT_DOWN event handler for ID_CPLAYEROVERLAY
 */

void EasyPlayerOverlay::OnLeftDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_DOWN event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DOWN event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
	*/

	PokerReaderOverlay* theparent;
	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		// get the parent instance
		theparent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);
		theparent->GetEasyDataPopup()->SetValues(&GetPlayerValue());
		theparent->GetEasyDataPopup()->SetPosition(wxPoint( this->GetScreenPosition().x, (this->GetScreenPosition().y + this->GetSize().GetHeight())));
		theparent->GetEasyDataPopup()->Show(true);
		theparent->GetEasyDataPopup()->Raise();
		theparent->Refresh();
		DragDone();
	}

	PlayerOverlayPanel::OnLeftDown(event);
}

/*
 * wxEVT_LEFT_UP event handler for ID_CPLAYEROVERLAY
 */

void EasyPlayerOverlay::OnLeftUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_UP event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_UP event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
	*/
	PlayerOverlayPanel::OnLeftUp(event);
}

/*
 * wxEVT_RIGHT_DOWN event handler for ID_CPLAYEROVERLAY
 */

void EasyPlayerOverlay::OnRightDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_DOWN event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_DOWN event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
	*/

	PokerReaderOverlay* theparent;
	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		// get the parent instance
		theparent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);
		theparent->GetEasyDataPopup()->SetValues(&GetPlayerValue());
		theparent->GetEasyDataPopup()->SetPosition(wxPoint( this->GetScreenPosition().x, (this->GetScreenPosition().y + this->GetSize().GetHeight())));
		theparent->GetEasyDataPopup()->Show(true);
		theparent->GetEasyDataPopup()->Raise();
		theparent->Refresh();
		DragDone();
	}
	PlayerOverlayPanel::OnRightDown(event);
}

/*
 * wxEVT_RIGHT_UP event handler for ID_CPLAYEROVERLAY
 */

void EasyPlayerOverlay::OnRightUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_UP event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_UP event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
	*/
	PlayerOverlayPanel::OnRightUp(event);
}

/*
 * wxEVT_MOTION event handler for ID_CPLAYEROVERLAY
 */

void EasyPlayerOverlay::OnMotion( wxMouseEvent& event )
{
	/*
////@begin wxEVT_MOTION event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_MOTION event handler for ID_CPLAYEROVERLAY in CEasyPlayerOverlay.
	*/
	PlayerOverlayPanel::OnMotion(event);
}

// update the tooltips
void EasyPlayerOverlay::SetToolTips()
{
	m_imgValue1->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("vpip")), LABELVPIP, DESCVPIP));
	m_imgValue2->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("pfr")), LABELPFR, DESCPFR));
	m_imgValue3->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("af")), LABELAF, DESCAF));
	m_imgValue4->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("handcount")), LABELHANDS, DESCHANDS));

	m_txtValue1->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("vpip")), LABELVPIP, DESCVPIP));
	m_txtValue2->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("pfr")), LABELPFR, DESCPFR));
	m_txtValue3->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("af")), LABELAF, DESCAF));
	m_txtValue4->SetToolTip(wxString::Format(_T("%s\n%s %s \n%s"), UserName(), GetValue(_T("handcount")), LABELHANDS, DESCHANDS));

	m_imgPlayerType->SetToolTip(wxString::Format(_T("%s\n%s"), UserName(), GetPlayerDesc(GetPlayerValue().playertype)));
	SetToolTip(wxString::Format(_T("%s"), UserName()));
}