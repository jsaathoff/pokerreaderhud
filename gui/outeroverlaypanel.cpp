/////////////////////////////////////////////////////////////////////////////
// Name:        outeroverlaypanel.cpp
// Purpose:
// Author:
// Modified by:
// Created:     22/06/2011 15:48:52
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "../pokerreaderhudapp.h"
#include "outeroverlaypanel.h"
#include <wx/config.h>
#include "../../pokerreadercommon/helper/definitions.h"

////@begin XPM images
////@end XPM images

/*
 * OuterOverlayPanel type definition
 */

IMPLEMENT_CLASS( OuterOverlayPanel, wxFrame )

/*
 * OuterOverlayPanel event table definition
 */

BEGIN_EVENT_TABLE( OuterOverlayPanel, wxFrame )

////@begin OuterOverlayPanel event table entries
    EVT_LEFT_DOWN( OuterOverlayPanel::OnLeftDown )
    EVT_LEFT_UP( OuterOverlayPanel::OnLeftUp )
    EVT_RIGHT_DOWN( OuterOverlayPanel::OnRightDown )
    EVT_RIGHT_UP( OuterOverlayPanel::OnRightUp )
    EVT_MOTION( OuterOverlayPanel::OnMotion )

////@end OuterOverlayPanel event table entries

END_EVENT_TABLE()

/*
 * OuterOverlayPanel constructors
 */

OuterOverlayPanel::OuterOverlayPanel()
{
    Init();
}

OuterOverlayPanel::OuterOverlayPanel( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}

/*
 * OuterOverlayPanel creator
 */

bool OuterOverlayPanel::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin OuterOverlayPanel creation
    wxFrame::Create( parent, id, caption, pos, size, style );

    this->SetForegroundColour(wxColour(255, 255, 255));
    this->SetBackgroundColour(wxColour(0, 0, 0));
    CreateControls();
////@end OuterOverlayPanel creation
    return true;
}

/*
 * OuterOverlayPanel destructor
 */

OuterOverlayPanel::~OuterOverlayPanel()
{
////@begin OuterOverlayPanel destruction
////@end OuterOverlayPanel destruction
}

/*
 * Member initialisation
 */

void OuterOverlayPanel::Init()
{
////@begin OuterOverlayPanel member initialisation
////@end OuterOverlayPanel member initialisation
}

/*
 * Control creation for OuterOverlayPanel
 */

void OuterOverlayPanel::CreateControls()
{
////@begin OuterOverlayPanel content construction
    OuterOverlayPanel* itemFrame1 = this;

////@end OuterOverlayPanel content construction
	SetName(_T("OUTEROVERLAY"));
}

/*
 * Should we show tooltips?
 */

bool OuterOverlayPanel::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap OuterOverlayPanel::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin OuterOverlayPanel bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end OuterOverlayPanel bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon OuterOverlayPanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin OuterOverlayPanel icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end OuterOverlayPanel icon retrieval
}

/*
 * wxEVT_LEFT_DOWN event handler for ID_OUTEROVERLAYPANEL
 */

void OuterOverlayPanel::OnLeftDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_DOWN event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DOWN event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
	*/

	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		CaptureMouse();
		wxPoint pos = ClientToScreen(event.GetPosition());
		wxPoint origin = GetPosition();
		int dx =  pos.x - origin.x;
		int dy = pos.y - origin.y;
		m_delta = wxPoint(dx, dy);
	}
	else
	{
		if(IsShown())
			Show(false);
	}
}

/*
 * wxEVT_LEFT_UP event handler for ID_OUTEROVERLAYPANEL
 */

void OuterOverlayPanel::OnLeftUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_UP event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_UP event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
	*/

	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		if (HasCapture())
		{
			ReleaseMouse();
		}
	}
}

/*
 * wxEVT_RIGHT_DOWN event handler for ID_OUTEROVERLAYPANEL
 */

void OuterOverlayPanel::OnRightDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_DOWN event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_DOWN event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
	*/

	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		if(IsShown())
			Show(false);
	}
	else
	{
		CaptureMouse();
		wxPoint pos = ClientToScreen(event.GetPosition());
		wxPoint origin = GetPosition();
		int dx =  pos.x - origin.x;
		int dy = pos.y - origin.y;
		m_delta = wxPoint(dx, dy);
	}
}

/*
 * wxEVT_RIGHT_UP event handler for ID_OUTEROVERLAYPANEL
 */

void OuterOverlayPanel::OnRightUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_UP event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_UP event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
	*/

	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		if (HasCapture())
		{
			ReleaseMouse();
		}
	}
}

/*
 * wxEVT_MOTION event handler for ID_OUTEROVERLAYPANEL
 */

void OuterOverlayPanel::OnMotion( wxMouseEvent& event )
{
	/*
////@begin wxEVT_MOTION event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_MOTION event handler for ID_OUTEROVERLAYPANEL in OuterOverlayPanel.
	*/

	wxPoint pt = event.GetPosition();
	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		if (event.Dragging() && event.LeftIsDown())
		{
			wxPoint pos = ClientToScreen(pt);
			Move(wxPoint(pos.x - m_delta.x, pos.y - m_delta.y));
		}
	}
	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		if (event.Dragging() && event.RightIsDown())
		{
			wxPoint pos = ClientToScreen(pt);
			Move(wxPoint(pos.x - m_delta.x, pos.y - m_delta.y));
		}
	}
}

// set the Pokerparent
void OuterOverlayPanel::SetPokerParent(HWND pokerparent)
{
#ifdef __WXMSW__
	m_PokerParent = pokerparent;
	//::SetWindowLong(GetHwnd(), GWL_EXSTYLE, GetWindowLong(GetHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	//::SetLayeredWindowAttributes(GetHwnd(), RGB(255, 0, 255), wxGetApp().alpha, LWA_COLORKEY | LWA_ALPHA);
	::SetWindowLongPtr(GetHwnd(),GWLP_HWNDPARENT,(long)m_PokerParent);
	//m_parentchild = ::GetWindow(GetParentHWND(), GW_HWNDFIRST);
	::SetWindowPos(::GetWindow(m_PokerParent, GW_HWNDFIRST), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE);
	//m_EasyDataPopup->SetNewParent(m_PokerParent);
	//::SetWindowPos(GetPokerParent(), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOSIZE);

#endif
}