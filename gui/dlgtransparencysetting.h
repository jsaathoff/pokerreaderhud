/////////////////////////////////////////////////////////////////////////////
// Name:        dlgtransparencysetting.h
// Purpose:     
// Author:      zsr Verlag
// Modified by: 
// Created:     04/06/2009 14:59:49
// RCS-ID:      
// Copyright:   Copyright (c) 2008 zsr Verlag
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _DLGTRANSPARENCYSETTING_H_
#define _DLGTRANSPARENCYSETTING_H_


/*!
 * Includes
 */

////@begin includes
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_TRANSPARENCYSETTINGDIALOG 10004
#define ID_SLIDER 10005
#define SYMBOL_TRANSPARENCYSETTINGDIALOG_STYLE wxSYSTEM_MENU|wxSTAY_ON_TOP|wxCLOSE_BOX|wxTAB_TRAVERSAL
#define SYMBOL_TRANSPARENCYSETTINGDIALOG_TITLE _("Transparenz")
#define SYMBOL_TRANSPARENCYSETTINGDIALOG_IDNAME ID_TRANSPARENCYSETTINGDIALOG
#define SYMBOL_TRANSPARENCYSETTINGDIALOG_SIZE wxDefaultSize
#define SYMBOL_TRANSPARENCYSETTINGDIALOG_POSITION wxDefaultPosition
////@end control identifiers


/*!
 * TransparencySettingDialog class declaration
 */

class TransparencySettingDialog: public wxDialog
{    
    DECLARE_DYNAMIC_CLASS( TransparencySettingDialog )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    TransparencySettingDialog();
    TransparencySettingDialog( wxWindow* parent, wxWindowID id = SYMBOL_TRANSPARENCYSETTINGDIALOG_IDNAME, const wxString& caption = SYMBOL_TRANSPARENCYSETTINGDIALOG_TITLE, const wxPoint& pos = SYMBOL_TRANSPARENCYSETTINGDIALOG_POSITION, const wxSize& size = SYMBOL_TRANSPARENCYSETTINGDIALOG_SIZE, long style = SYMBOL_TRANSPARENCYSETTINGDIALOG_STYLE );

    /// Creation
    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_TRANSPARENCYSETTINGDIALOG_IDNAME, const wxString& caption = SYMBOL_TRANSPARENCYSETTINGDIALOG_TITLE, const wxPoint& pos = SYMBOL_TRANSPARENCYSETTINGDIALOG_POSITION, const wxSize& size = SYMBOL_TRANSPARENCYSETTINGDIALOG_SIZE, long style = SYMBOL_TRANSPARENCYSETTINGDIALOG_STYLE );

    /// Destructor
    ~TransparencySettingDialog();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

////@begin TransparencySettingDialog event handler declarations

    /// wxEVT_COMMAND_SLIDER_UPDATED event handler for ID_SLIDER
    void OnSliderUpdated( wxCommandEvent& event );

////@end TransparencySettingDialog event handler declarations

////@begin TransparencySettingDialog member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end TransparencySettingDialog member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

#ifdef __WXMSW__
	HWND m_ParentHWND;
	
	void SetParentHWND(HWND wnd);
#endif
	



////@begin TransparencySettingDialog member variables
    wxSlider* m_Slider;
////@end TransparencySettingDialog member variables
};


#endif
    // _DLGTRANSPARENCYSETTING_H_
