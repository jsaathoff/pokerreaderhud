/////////////////////////////////////////////////////////////////////////////
// Name:        overlaypanel.cpp
// Purpose:
// Author:
// Modified by:
// Created:     04/03/2010 15:55:06
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "overlaypanel.h"
#include "../pokerreaderhudapp.h"
#include "pokerreaderoverlay.h"

/*
 * wxOverlayPanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( OverlayPanel, wxPanel )

/*
 * wxOverlayPanel event table definition
 */

BEGIN_EVENT_TABLE( OverlayPanel, wxPanel )

    //EVT_PAINT( OverlayPanel::OnPaint )
    EVT_IDLE( OverlayPanel::OnIdle )
    EVT_ERASE_BACKGROUND( OverlayPanel::OnEraseBackground )
    EVT_LEFT_DOWN( OverlayPanel::OnLeftDown )
    EVT_LEFT_UP( OverlayPanel::OnLeftUp )
    EVT_RIGHT_DOWN( OverlayPanel::OnRightDown )
    EVT_RIGHT_UP( OverlayPanel::OnRightUp )
    EVT_MOTION( OverlayPanel::OnMotion )

END_EVENT_TABLE()

/*
 * wxOverlayPanel constructors
 */

OverlayPanel::OverlayPanel()
{
    Init();
}

OverlayPanel::OverlayPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
    Init();
    Create(parent, id, pos, size, style);
}

/*
 * wxOverlayPanel creator
 */

bool OverlayPanel::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
	m_bmousereversed = false;
	m_bPosCheck = true;
    wxPanel::Create(parent, id, pos, size, style);
    CreateControls();
	//SetDoubleBuffered(true);

    return true;
}

/*
 * wxOverlayPanel destructor
 */

OverlayPanel::~OverlayPanel()
{
}

/*
 * Member initialisation
 */

void OverlayPanel::Init()
{
}

/*
 * Control creation for wxOverlayPanel
 */

void OverlayPanel::CreateControls()
{
    //this->SetBackgroundColour(wxColour(255, 0, 255));
}

/*
 * Should we show tooltips?
 */

bool OverlayPanel::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap OverlayPanel::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
}

/*
 * Get icon resources
 */

wxIcon OverlayPanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
}

/*
 * wxEVT_IDLE event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnIdle( wxIdleEvent& event )
{
    // Before editing this code, remove the block markers.
    event.Skip();
}

/*
 * wxEVT_LEFT_DOWN event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnLeftDown( wxMouseEvent& event )
{
	/*
    // Before editing this code, remove the block markers.
    event.Skip();
	*/
	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		CaptureMouse();
		m_start_object_pos = this->GetPosition();
		::wxGetMousePosition(&m_start_mouse_pos.x, &m_start_mouse_pos.y);
	}
}

/*
 * wxEVT_LEFT_UP event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnLeftUp( wxMouseEvent& event )
{
	/*
    // Before editing this code, remove the block markers.
    event.Skip();
	*/

	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		if(HasCapture())
			ReleaseMouse();
	}
}

/*
 * wxEVT_RIGHT_DOWN event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnRightDown( wxMouseEvent& event )
{
	/*
    // Before editing this code, remove the block markers.
    event.Skip();
	*/
	//COverlayContainer* theparent;

	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		CaptureMouse();
		m_start_object_pos = this->GetPosition();
		::wxGetMousePosition(&m_start_mouse_pos.x, &m_start_mouse_pos.y);
	}
}

/*
 * wxEVT_RIGHT_UP event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnRightUp( wxMouseEvent& event )
{
	/*
    // Before editing this code, remove the block markers.
    event.Skip();
	*/

	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		if(HasCapture())
			ReleaseMouse();
	}
}

/*
 * wxEVT_MOTION event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnMotion( wxMouseEvent& event )
{
	/*
    // Before editing this code, remove the block markers.
    event.Skip();
	*/
	wxPoint current_mouse_pos;
	double mposx = 0;
	double mposy = 0;
	double parentheight = 0;
	double parentwidth = 0;
	PokerReaderOverlay* theParent = wxDynamicCast(this->GetParent(), PokerReaderOverlay);
	wxPoint newpos;

	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		if(event.Dragging() && event.RightIsDown())
		{
			wxPoint current_mouse_pos;
			::wxGetMousePosition(&current_mouse_pos.x, &current_mouse_pos.y);

			newpos = m_start_object_pos + (current_mouse_pos - m_start_mouse_pos);
			//GetWindowRegion().Offset(newpos);
			this->SetPosition(newpos);

			parentwidth=(theParent->getPokerParentRect().right - theParent->getPokerParentRect().left);
			parentheight=(theParent->getPokerParentRect().bottom - theParent->getPokerParentRect().top);

			mposx=this->GetPosition().x-100;
			mposy=this->GetPosition().y-100;

			this->SetXCoord(mposx/parentwidth);
			this->SetYCoord(mposy/parentheight);
		}
	}
	else if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		if(event.Dragging() && event.LeftIsDown())
		{
			wxPoint current_mouse_pos;
			::wxGetMousePosition(&current_mouse_pos.x, &current_mouse_pos.y);

			newpos = m_start_object_pos + (current_mouse_pos - m_start_mouse_pos);
			//GetWindowRegion().Offset(newpos);
			this->SetPosition(newpos);

			parentwidth=(theParent->getPokerParentRect().right - theParent->getPokerParentRect().left);
			parentheight=(theParent->getPokerParentRect().bottom - theParent->getPokerParentRect().top);

			mposx=this->GetPosition().x-100;
			mposy=this->GetPosition().y-100;

			this->SetXCoord(mposx/parentwidth);
			this->SetYCoord(mposy/parentheight);
		}
	}
}

// get mouse reverse value
bool OverlayPanel::IsMouseReversed()
{
	return m_bmousereversed;
}

// get mouse reverse value
void OverlayPanel::ReverseMouse(bool reverse)
{
	m_bmousereversed = reverse;
}

double OverlayPanel::GetXCoord()
{
	return m_nXCoord;
}

void OverlayPanel::SetXCoord(double value)
{
	m_nXCoord = value ;
}

double OverlayPanel::GetYCoord()
{
	return m_nYCoord;
}

void OverlayPanel::SetYCoord(double value)
{
	m_nYCoord = value ;
}

/*
 * wxEVT_PAINT event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnPaint( wxPaintEvent& event )
{
    // Before editing this code, remove the block markers.
    wxBufferedPaintDC dc(this);
	//event.Skip();
}

/*
 * wxEVT_ERASE_BACKGROUND event handler for ID_OVERLAYPANEL
 */

void OverlayPanel::OnEraseBackground( wxEraseEvent& event )
{
    // Before editing this code, remove the block markers.
    event.Skip();
}

wxRegion OverlayPanel::GetWindowRegion()
{
	wxRegion m_WinRegion(GetRect());
	return m_WinRegion;
}

void OverlayPanel::SetPosition(const wxPoint& pt)
{
	if(m_bPosCheck)
	{
		if( ((pt.x > 0) && (pt.x < (GetParent()->GetSize().GetWidth() - GetSize().GetWidth()) )) && ((pt.y > 0) && (pt.y < (GetParent()->GetSize().GetHeight() - GetSize().GetHeight() )))  )
		{
			wxPanel::SetPosition(pt);
		}
	}
	else
	{
		wxPanel::SetPosition(pt);
	}
}