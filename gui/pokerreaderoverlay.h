/////////////////////////////////////////////////////////////////////////////
// Name:        okerreaderoverlay.h
// Purpose:
// Author:
// Modified by:
// Created:     10/03/2010 15:15:29
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _OKERREADEROVERLAY_H_
#define _OKERREADEROVERLAY_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

#include "../common/providerconf.h"
//#include "easyplayeroverlay.h"
//#include "easyplayerdatapopup.h"
#include "transparencydialog.h"
#include "simpledatapopup.h"
#include "../../pokerreadercommon/helper/providerpathutilities.h"
#include "../../pokerreadercommon/helper/huddata.h"
#include "../../pokerreadercommon/helper/tabledata.h"
#include "../../pokerreadercommon/helper/configrecord.h"
#include "../../pokerreadercommon/helper/playerrecord.h"

/*!
 * Forward declarations
 */
class wxFileConfig;
////@begin forward declarations
class OverlayMenu;
////@end forward declarations

class EasyPlayerOverlay;
class ExtPlayerOverlay;

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_POKERREADEROVERLAY 10000
#define SYMBOL_POKERREADEROVERLAY_STYLE wxFRAME_NO_TASKBAR|wxNO_BORDER|wxFRAME_SHAPED
#define SYMBOL_POKERREADEROVERLAY_TITLE _T("PokerReaderOverlay")
#define SYMBOL_POKERREADEROVERLAY_IDNAME ID_POKERREADEROVERLAY
#define SYMBOL_POKERREADEROVERLAY_SIZE wxSize(400, 300)
#define SYMBOL_POKERREADEROVERLAY_POSITION wxDefaultPosition
////@end control identifiers

#define ID_MENUSAVELAYOUT 10990
#define ID_MENUITEMRESETLAYOUT 10991
//#define ID_TIMER6 10997 // MOD 24.08.2009 JS: deactivate timer id
#define ID_MENUSETTRANSPARENCY 10992
#define ID_MENUSETREVERSEMOUSEBUTTONS 10993
#define ID_MENUSETNEWHEROSEAT 10994
#define ID_MENUSETOWNPLAYERSYMBOL 10995
#define ID_MENUSETSWITCHOVERLAYVIEW 10996
#define ID_MENUSETEASYPLAYEROVERLAY 10997
#define ID_MENUSETEXTENDEDEASYPLAYEROVERLAY 10998

/*!
 * PokerReaderOverlay class declaration
 */

class PokerReaderOverlay: public wxFrame
{
    DECLARE_CLASS( PokerReaderOverlay )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    PokerReaderOverlay();
    PokerReaderOverlay( wxWindow* parent, wxWindowID id = SYMBOL_POKERREADEROVERLAY_IDNAME, const wxString& caption = SYMBOL_POKERREADEROVERLAY_TITLE, const wxPoint& pos = SYMBOL_POKERREADEROVERLAY_POSITION, const wxSize& size = SYMBOL_POKERREADEROVERLAY_SIZE, long style = SYMBOL_POKERREADEROVERLAY_STYLE );

    bool Create(wxWindow* parent, wxWindowID id = SYMBOL_POKERREADEROVERLAY_IDNAME, const wxString& caption = SYMBOL_POKERREADEROVERLAY_TITLE, const wxPoint& pos = SYMBOL_POKERREADEROVERLAY_POSITION, const wxSize& size = SYMBOL_POKERREADEROVERLAY_SIZE, long style = SYMBOL_POKERREADEROVERLAY_STYLE );

	WX_DEFINE_ARRAY_PTR(EasyPlayerOverlay*, EasyOverlay);

	WX_DEFINE_ARRAY_PTR(ExtPlayerOverlay*, ExtOverlay);

    /// Destructor
    ~PokerReaderOverlay();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	void FindAndOverlay();

	// update coords for all seats
	void UpdateSeatCoordinates();

	void setProvider(int provider);

	int getProvider();

	// remove all overlays
	void removeOverlays();

	// update the position of controls
	void UpdateControlPositions();

	void PrepareToShow();

	wxRegion getRegion();

	void UpdatePlayerOverlayData();

	// returns true if window title of parent/poker window changed...
	bool ParentTitleChanged();

	void setPokerParentWindowname(wxString windowname);

	// return the virtual seat
	//int GetVisualSeat();

	// return player data-popup
	SimpleDataPopup* GetEasyDataPopup();

	// initialize the menu-coordinates
	void InitMenuCoordinates(bool fromfile);

	// initialize the seat-coordinates
	void InitSeatCoordinates(bool fromfile);

	// get the window name
	wxString getWindowName();

	// set the huddata
	void setHudData(HudData data);

	// get the huddata
	HudData getHudData();

	// set the visual seat for hero
	void setVisualHeroSeat(int vseat);

	// get the visual seat for hero
	int getVisualHeroSeat();

	// return the visual seat for original seat
	int getVisualSeat(int seatdistance, int originalseat, int tablemax);

	// calc and return the seatdistance
	int getHeroSeatDistance(int visualseat, int originalseat, int tablemax);

	// set seat distance
	void setSeatDistance(int distance);

	// get the distance
	int getSeatDistance();

	// set max player on table
	void setTableMax(int max);

	// max player at table
	int getTableMax();

#ifdef __WXMSW__

	void setPokerParent(HWND parent);

	HWND getPokerParent();

	RECT getPokerParentRect();

	void wxMSWWindowPositioning();

#endif

	void OnMenuSaveLayoutClick(wxCommandEvent& event);

	void OnMenuResetLayoutClick(wxCommandEvent& event);

	void OnMenuTransparencyClick(wxCommandEvent &event);

	void OnMenuReverseMouseButtonsClick(wxCommandEvent &event);

	void OnShowHeroSymbolClick(wxCommandEvent &event);

	void OnActivateEasyHud(wxCommandEvent &event);

	void OnActivateExtendedHud(wxCommandEvent &event);

	void setHeroSymbol(bool val);

	// new seat for hero
	void OnTakeNewSeat(wxCommandEvent& event);

	// returns the menu
	wxMenu* GetMenu();

	// set transparency for all subwindows
	void setWindowTransparency(byte transval);

	// reverse mouse buttons
	void setMouseReverse(bool reverse);

	// s
	void setExtendedOverlay(bool val);

	ProviderConf* getProviderConfig();

	void ReadConfig();

	void OnConfigChange(ConfigRecord confrec);

////@begin PokerReaderOverlay event handler declarations

    /// wxEVT_PAINT event handler for ID_POKERREADEROVERLAY
    void OnPaint( wxPaintEvent& event );

    /// wxEVT_IDLE event handler for ID_POKERREADEROVERLAY
    void OnIdle( wxIdleEvent& event );

    /// wxEVT_ERASE_BACKGROUND event handler for ID_POKERREADEROVERLAY
    void OnEraseBackground( wxEraseEvent& event );

////@end PokerReaderOverlay event handler declarations

////@begin PokerReaderOverlay member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end PokerReaderOverlay member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin PokerReaderOverlay member variables
    OverlayMenu* m_menu;
////@end PokerReaderOverlay member variables

private:
	ProviderConf* pConfig;
	wxRegion m_WindowRegion;
	wxWindowListNode* m_WinListNode;
	int m_nProvider;

	// record which holds player values
	//PlayerValueRecord* m_PlayerValues;

	// overlay array for players (easy overlay)
	EasyOverlay EasyPlayerOverlays;

	ExtOverlay ExtPlayerOverlays;

	bool m_bFirstrun;

	bool m_bMouseReverse;

	bool m_bShowHeroSymbol;

	SimpleDataPopup* m_EasyDataPopup;

	// new mod. time of records
	wxDateTime m_dtLastRecordfileMod;

	// last mod. time of records
	wxDateTime m_dtRecordfileMod;

	// menu for overlay
	wxMenu* m_MenuBar;

	// menu for layouts/overlay style
	wxMenu* m_OverlayStyleMenu;

	//transparency settings
	TransparencyDialog * m_TransDlg;

	//PokerReaderConf* mainConfig;

	//PokerReaderHUDConf* hudConfig;

	HudData Data;

	// visual seat for hero
	int m_nVisualHeroSeat;

	// seat distance
	int m_nSeatDistance;

	// table max
	int m_nTableMax;

#ifdef __WXMSW__
	HWND m_PokerParent;
	RECT m_PokerParentRect;
	RECT m_PokerParentRectOld;
#endif

	wxFileConfig * config;

	ProviderPathUtilities Paths;

	wxString m_PokerParentName;
};

#endif
    // _OKERREADEROVERLAY_H_