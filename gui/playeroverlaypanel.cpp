/////////////////////////////////////////////////////////////////////////////
// Name:        playeroverlaypanel.cpp
// Purpose:
// Author:
// Modified by:
// Created:     10/03/2010 15:19:23
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "playeroverlaypanel.h"
#include "pokerreaderoverlay.h"

////@begin XPM images
////@end XPM images

/*
 * PlayerOverlayPanel type definition
 */

IMPLEMENT_DYNAMIC_CLASS( PlayerOverlayPanel, OverlayPanel )

/*
 * PlayerOverlayPanel event table definition
 */

BEGIN_EVENT_TABLE( PlayerOverlayPanel, OverlayPanel )

////@begin PlayerOverlayPanel event table entries
    EVT_LEFT_DOWN( PlayerOverlayPanel::OnLeftDown )
    EVT_LEFT_UP( PlayerOverlayPanel::OnLeftUp )
    EVT_RIGHT_DOWN( PlayerOverlayPanel::OnRightDown )
    EVT_RIGHT_UP( PlayerOverlayPanel::OnRightUp )
    EVT_MOTION( PlayerOverlayPanel::OnMotion )

////@end PlayerOverlayPanel event table entries

	//EVT_ERASE_BACKGROUND( PlayerOverlayPanel::OnEraseBackground )

END_EVENT_TABLE()

/*
 * PlayerOverlayPanel constructors
 */

PlayerOverlayPanel::PlayerOverlayPanel()
{
    Init();
}

PlayerOverlayPanel::PlayerOverlayPanel( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create(parent, id, pos, size, style);
}

/*
 * wxPlayerOverlayPanel creator
 */

bool PlayerOverlayPanel::Create( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style )
{
////@begin PlayerOverlayPanel creation
    OverlayPanel::Create( parent, id, pos, size, style );

    CreateControls();
    Centre();
////@end PlayerOverlayPanel creation
    return true;
}

/*
 * PlayerOverlayPanel destructor
 */

PlayerOverlayPanel::~PlayerOverlayPanel()
{
////@begin PlayerOverlayPanel destruction
////@end PlayerOverlayPanel destruction
}

/*
 * Member initialisation
 */

void PlayerOverlayPanel::Init()
{
////@begin PlayerOverlayPanel member initialisation
////@end PlayerOverlayPanel member initialisation
}

/*
 * Control creation for wxPlayerOverlayPanel
 */

void PlayerOverlayPanel::CreateControls()
{
////@begin PlayerOverlayPanel content construction
////@end PlayerOverlayPanel content construction
}

/*
 * Should we show tooltips?
 */

bool PlayerOverlayPanel::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap PlayerOverlayPanel::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin PlayerOverlayPanel bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end PlayerOverlayPanel bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon PlayerOverlayPanel::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin PlayerOverlayPanel icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end PlayerOverlayPanel icon retrieval
}

/*
 * wxEVT_LEFT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL
 */

void PlayerOverlayPanel::OnLeftDown( wxMouseEvent& event )
{
////@begin wxEVT_LEFT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
	OverlayPanel::OnLeftDown(event);
}

/*
 * wxEVT_LEFT_UP event handler for ID_WXPLAYEROVERLAYPANEL
 */

void PlayerOverlayPanel::OnLeftUp( wxMouseEvent& event )
{
////@begin wxEVT_LEFT_UP event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_UP event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
	OverlayPanel::OnLeftUp(event);

	PokerReaderOverlay* theParent = wxDynamicCast(GetParent(), PokerReaderOverlay);
	theParent->getProviderConfig()->SetSeatCoordX(GetVisualSeat(), GetXCoord());
	theParent->getProviderConfig()->SetSeatCoordY(GetVisualSeat(), GetYCoord());
}

/*
 * wxEVT_RIGHT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL
 */

void PlayerOverlayPanel::OnRightDown( wxMouseEvent& event )
{
////@begin wxEVT_RIGHT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_DOWN event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
	OverlayPanel::OnRightDown(event);
}

/*
 * wxEVT_RIGHT_UP event handler for ID_WXPLAYEROVERLAYPANEL
 */

void PlayerOverlayPanel::OnRightUp( wxMouseEvent& event )
{
////@begin wxEVT_RIGHT_UP event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_UP event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
	OverlayPanel::OnRightUp(event);

	PokerReaderOverlay* theParent = wxDynamicCast(GetParent(), PokerReaderOverlay);
	theParent->getProviderConfig()->SetSeatCoordX(GetVisualSeat(), GetXCoord());
	theParent->getProviderConfig()->SetSeatCoordY(GetVisualSeat(), GetYCoord());
}

/*
 * wxEVT_MOTION event handler for ID_WXPLAYEROVERLAYPANEL
 */

void PlayerOverlayPanel::OnMotion( wxMouseEvent& event )
{
////@begin wxEVT_MOTION event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_MOTION event handler for ID_WXPLAYEROVERLAYPANEL in wxPlayerOverlayPanel.
	OverlayPanel::OnMotion(event);
}

// return the visual seat/pref-seat
int PlayerOverlayPanel::GetVisualSeat()
{
	return m_nVisualSeat;
}

// return the visual seat/pref-seat
void PlayerOverlayPanel::SetVisualSeat(int seat)
{
	m_nVisualSeat = seat;
}

// sets the username
void PlayerOverlayPanel::FillUserName()
{
}

// set the window-username
void PlayerOverlayPanel::SetUserName(wxString value)
{
	m_sUserName = value ;
}

// Paints the standard window
void PlayerOverlayPanel::PreparePlayerOverlay()
{
}

// get value from struct
wxString PlayerOverlayPanel::GetValue(wxString getvalue)
{
	wxString retval;
	if(!getvalue.IsEmpty())
	{
		if(getvalue == _T("af"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.af);
		}
		else if(getvalue == _T("pfr"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.pfr);
		}
		else if(getvalue == _T("vpip"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.vpip);
		}
		else if(getvalue == _T("handcount"))
		{
			retval = wxString::Format(_T("(%d)"), m_PlayerStruct.handcount);
		}
		else if(getvalue == _T("aff"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.flop_af);
		}
		else if(getvalue == _T("aft"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.turn_af);
		}
		else if(getvalue == _T("afr"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.river_af);
		}
		else if(getvalue == _T("ats"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.ats);
		}
		else if(getvalue == _T("afq"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.afq);
		}
		else if(getvalue == _T("afqf"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.flop_afq);
		}
		else if(getvalue == _T("afqr"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.river_afq);
		}
		else if(getvalue == _T("afqt"))
		{
			retval = wxString::Format(_T("%.1lf"), m_PlayerStruct.turn_afq);
		}
		retval.Replace(_T(","), _T("."));
		return retval;
	}
	else
	{
		return wxEmptyString;
	}
}

PlayerValueRecord PlayerOverlayPanel::GetPlayerValue()
{
	return m_PlayerStruct;
}

void PlayerOverlayPanel::SetPlayerValues(PlayerValueRecord playervalues)
{
	m_PlayerStruct = playervalues;
}

void PlayerOverlayPanel::OnEraseBackground( wxEraseEvent& event )
{

}

void PlayerOverlayPanel::DragDone()
{
	Refresh(true);
}