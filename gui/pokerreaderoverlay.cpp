/////////////////////////////////////////////////////////////////////////////
// Name:        pokerreaderoverlay.cpp
// Purpose:
// Author:
// Modified by:
// Created:     26/02/2010 16:17:44
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
#include "overlaymenu.h"
////@end includes

#include "pokerreaderoverlay.h"
#include "../pokerreaderhudapp.h"
#include <wx/numdlg.h>
#include "easyplayeroverlay.h"
#include "extendedplayeroverlay.h"

////@begin XPM images
////@end XPM images

/*
* PokerReaderOverlay type definition
*/

IMPLEMENT_CLASS( PokerReaderOverlay, wxFrame )

	/*
	* PokerReaderOverlay event table definition
	*/

	BEGIN_EVENT_TABLE( PokerReaderOverlay, wxFrame )

	////@begin PokerReaderOverlay event table entries
	EVT_PAINT( PokerReaderOverlay::OnPaint )
	EVT_IDLE( PokerReaderOverlay::OnIdle )
	EVT_ERASE_BACKGROUND( PokerReaderOverlay::OnEraseBackground )

	////@end PokerReaderOverlay event table entries

	EVT_MENU( ID_MENUSAVELAYOUT, PokerReaderOverlay::OnMenuSaveLayoutClick )
	EVT_MENU( ID_MENUITEMRESETLAYOUT, PokerReaderOverlay::OnMenuResetLayoutClick )
	EVT_MENU( ID_MENUSETTRANSPARENCY, PokerReaderOverlay::OnMenuTransparencyClick )
	EVT_MENU(ID_MENUSETREVERSEMOUSEBUTTONS, PokerReaderOverlay::OnMenuReverseMouseButtonsClick)
	EVT_MENU(ID_MENUSETOWNPLAYERSYMBOL, PokerReaderOverlay::OnShowHeroSymbolClick)
	EVT_MENU(ID_MENUSETNEWHEROSEAT, PokerReaderOverlay::OnTakeNewSeat)
	EVT_MENU(ID_MENUSETEXTENDEDEASYPLAYEROVERLAY, PokerReaderOverlay::OnActivateExtendedHud)
	EVT_MENU(ID_MENUSETEASYPLAYEROVERLAY, PokerReaderOverlay::OnActivateEasyHud)
	END_EVENT_TABLE()

	/*
	* PokerReaderOverlay constructors
	*/

PokerReaderOverlay::PokerReaderOverlay()
{
	Init();
}

PokerReaderOverlay::PokerReaderOverlay( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
	Init();
	Create( parent, id, caption, pos, size, style);
	//SetDoubleBuffered(true);
}

/*
* wxPokerReaderOverlay creator
*/

bool PokerReaderOverlay::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
	////@begin PokerReaderOverlay creation
	wxFrame::Create( parent, id, caption, pos, size, style );

	this->SetBackgroundColour(wxColour(255, 0, 255));
	CreateControls();
	Centre();
	////@end PokerReaderOverlay creation
	//::SetLayeredWindowAttributes(GetHwnd(), RGB(255,0,255),255, LWA_COLORKEY | LWA_ALPHA);
	//SetTransparent(150);
	//m_nVisualSeat = -1;

	//wxFileConfig* config = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);

	//wxByte alphaval = static_cast<wxByte>(config->ReadLong(CONF_HUDTRANS, 230));
	///wxGetApp().alpha = alphaval;
	//delete config;

	// TODO: Replace
	//SetWindowTransparency(wxGetApp().alpha);
	//SetMouseReverse(wxConfig::Get()->ReadBool(CONF_HUDMOUSEREVERSE, false));
	//SetHeroSymbol(wxConfig::Get()->ReadBool(CONF_HUDPLAYERSYMBOL, true));
	//ReadConfigAndUpdate();
	return true;
}

/*
* PokerReaderOverlay destructor
*/

PokerReaderOverlay::~PokerReaderOverlay()
{
	////@begin PokerReaderOverlay destruction
	////@end PokerReaderOverlay destruction

	m_EasyDataPopup->Destroy();
	m_TransDlg->Destroy();
	//delete m_PlayerValues;
	delete pConfig;
}

/*
* Member initialisation
*/

void PokerReaderOverlay::Init()
{
	////@begin PokerReaderOverlay member initialisation
	m_menu = NULL;
	////@end PokerReaderOverlay member initialisation
	pConfig = NULL;
	m_bFirstrun = true;
}

/*
* Control creation for wxPokerReaderOverlay
*/

void PokerReaderOverlay::CreateControls()
{
	////@begin PokerReaderOverlay content construction
	PokerReaderOverlay* itemFrame1 = this;

	m_menu = new OverlayMenu( itemFrame1, ID_PANELMENUOVERLAY, wxPoint(150, 150), wxSize(29, 25), wxNO_BORDER|wxTAB_TRAVERSAL );

	////@end PokerReaderOverlay content construction

	pConfig = new ProviderConf();

	wxLogMessage(wxT("Creating controls"));

	m_OverlayStyleMenu = new wxMenu;
	m_OverlayStyleMenu->Append(ID_MENUSETEASYPLAYEROVERLAY, _("Einfach"), _("Einfache HUD-Anzeige"), wxITEM_CHECK);
	m_OverlayStyleMenu->Append(ID_MENUSETEXTENDEDEASYPLAYEROVERLAY, _("Erweitert"), _("Erweiterte HUD-Anzeige"), wxITEM_CHECK);

	m_MenuBar = new wxMenu;
	m_MenuBar->Append(ID_MENUSAVELAYOUT, _("Layout speichern"), _("Speichert die Positionen der Fenster"), wxITEM_NORMAL);
	m_MenuBar->Append(ID_MENUITEMRESETLAYOUT, _("Layout zur�cksetzen"), _("Layout auf Standard zurueck setzen"), wxITEM_NORMAL);
	m_MenuBar->AppendSubMenu(m_OverlayStyleMenu, _("HUD-Stil"), wxEmptyString);
	m_MenuBar->Append(ID_MENUSETTRANSPARENCY, _("Transparenz ver�ndern"), _("�ndern Sie die Transparenz des HUD"), wxITEM_NORMAL);
	m_MenuBar->Append(ID_MENUSETREVERSEMOUSEBUTTONS, _("Maustasten tauschen"), _("Tauscht die Maustasten um"), wxITEM_CHECK);
	m_MenuBar->Append(ID_MENUSETOWNPLAYERSYMBOL, _("Eigenes Spielersymbol anzeigen"), _("Zeigt die Einsch�tzung des HERO an"), wxITEM_CHECK);
	m_MenuBar->Append(ID_MENUSETNEWHEROSEAT, _("Ich habe mich umgesetzt"), _("Der eigene Spieler hat sich umgesetzt"), wxITEM_NORMAL);
	//m_EasyDataPopup->SetSize(0,0);

	//Refresh();
	//Update();

	//m_menu->SetPosition(wxPoint(50,50));
	//SetSize(29,25);
	//StartHook();

	SetSize(400,300);

	m_EasyDataPopup = new SimpleDataPopup(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxFRAME_NO_TASKBAR);
	m_EasyDataPopup->Show(false);

#ifdef __WXMSW__
	//::SetWindowLong(GetHwnd(), GWL_EXSTYLE, GetWindowLong(GetHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	//::SetLayeredWindowAttributes(GetHwnd(), RGB(255, 0, 255), wxGetApp().alpha, LWA_COLORKEY | LWA_ALPHA);
	//::SetWindowLongPtr(GetHwnd(),GWLP_HWNDPARENT,(long)m_PokerParent);
	//m_parentchild = ::GetWindow(GetParentHWND(), GW_HWNDFIRST);
	//::SetWindowPos(m_EasyDataPopup->GetHWND(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE);

	//::SetWindowPos(GetPokerParent(), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOSIZE);
#endif

	m_TransDlg = new TransparencyDialog(this);
	m_TransDlg->Show(false);

	m_bFirstrun = true;

	wxPanel* m_pan = new wxPanel(this, wxID_ANY, wxPoint(120,120), wxSize(0,0));
	m_pan->Show();
}

/*
* Should we show tooltips?
*/

bool PokerReaderOverlay::ShowToolTips()
{
	return true;
}

/*
* Get bitmap resources
*/

wxBitmap PokerReaderOverlay::GetBitmapResource( const wxString& name )
{
	// Bitmap retrieval
	////@begin PokerReaderOverlay bitmap retrieval
	wxUnusedVar(name);
	return wxNullBitmap;
	////@end PokerReaderOverlay bitmap retrieval
}

/*
* Get icon resources
*/

wxIcon PokerReaderOverlay::GetIconResource( const wxString& name )
{
	// Icon retrieval
	////@begin PokerReaderOverlay icon retrieval
	wxUnusedVar(name);
	return wxNullIcon;
	////@end PokerReaderOverlay icon retrieval
}

/*
* wxEVT_PAINT event handler for ID_wxPokerReaderOverlay
*/

void PokerReaderOverlay::OnPaint( wxPaintEvent& event )
{
	////@begin wxEVT_PAINT event handler for ID_wxPokerReaderOverlay in wxPokerReaderOverlay.
	// Before editing this code, remove the block markers.
	wxPaintDC dc(this);
	////@end wxEVT_PAINT event handler for ID_wxPokerReaderOverlay in wxPokerReaderOverlay.
	event.Skip();
	//dc = wxGCDC(dc);

	/*
	wxBufferedPaintDC dc(this);
	//dc.SetClippingRegion(m_EasyDataPopup->GetWindowRegion());
	unsigned int count = 0;

	m_WindowRegion.Clear();
	//event.Skip();

	for(count=0; count < GetChildren().GetCount(); count++)
	{
	m_WinListNode = GetChildren().Item(count);

	if(m_WinListNode->GetData()->GetName() != _T("OUTEROVERLAY"))
	{
	if (m_WinListNode->GetData()->IsShown())
	{
	wxRegion region(m_WinListNode->GetData()->GetRect());
	//if(!region.IsEqual(m_EasyDataPopup->GetWindowRegion()))
	//{
	m_WindowRegion.Union(region);
	//}
	//dc.DrawBitmap(region.ConvertToBitmap(), m_WinListNode->GetData()->GetPosition());
	}
	}
	}
	dc.DrawBitmap(m_WindowRegion.ConvertToBitmap(), 0, 0);

	SetShape(m_WindowRegion);
	Refresh(true);
	event.Skip();

	//SetWindowShape(GetWindowBitmap());
	//event.Skip();
	*/
}

/*
* wxEVT_ERASE_BACKGROUND event handler for ID_wxPokerReaderOverlay
*/

void PokerReaderOverlay::OnEraseBackground( wxEraseEvent& event )
{
	/*
	////@begin wxEVT_ERASE_BACKGROUND event handler for ID_wxPokerReaderOverlay in wxPokerReaderOverlay.
	// Before editing this code, remove the block markers.
	event.Skip();
	////@end wxEVT_ERASE_BACKGROUND event handler for ID_wxPokerReaderOverlay in wxPokerReaderOverlay.
	*/
	event.Skip();
}

void PokerReaderOverlay::FindAndOverlay()
{
#ifdef __WXMSW__
	if(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.empty())
		wxGetApp().ConfigSetting[CONF_HUDTRANS].value = L"200";

	::SetWindowLong(GetHwnd(), GWL_EXSTYLE, GetWindowLong(GetHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	::SetLayeredWindowAttributes(GetHwnd(), RGB(255, 0, 255), static_cast<byte>(_wtoi(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.c_str())), LWA_COLORKEY | LWA_ALPHA);
	::SetWindowLongPtr(GetHwnd(),GWLP_HWNDPARENT,(long)m_PokerParent);
	//m_parentchild = ::GetWindow(GetParentHWND(), GW_HWNDFIRST);
	::SetWindowPos(::GetWindow(getPokerParent(), GW_HWNDFIRST), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE);
	//m_EasyDataPopup->SetNewParent(m_PokerParent);
	//::SetWindowPos(GetPokerParent(), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOREPOSITION | SWP_NOSIZE);
	m_EasyDataPopup->SetPokerParent(m_PokerParent);
#endif
}

/*
* wxEVT_IDLE event handler for ID_WXPOKERREADEROVERLAY
*/

void PokerReaderOverlay::OnIdle( wxIdleEvent& event )
{
	/*
	////@begin wxEVT_IDLE event handler for ID_WXPOKERREADEROVERLAY in wxPokerReaderOverlay.
	// Before editing this code, remove the block markers.
	event.Skip();
	////@end wxEVT_IDLE event handler for ID_WXPOKERREADEROVERLAY in wxPokerReaderOverlay.
	*/
#ifdef __WXMSW__ // windows specific code
	wxMSWWindowPositioning();
#endif

	event.Skip();
}

void PokerReaderOverlay::setPokerParent(HWND parent)
{
	m_PokerParent = parent;
}

HWND PokerReaderOverlay::getPokerParent()
{
	return m_PokerParent;
}

RECT PokerReaderOverlay::getPokerParentRect()
{
	return m_PokerParentRect;
}

void PokerReaderOverlay::setProvider(int provider)
{
	m_nProvider = provider;
}

int PokerReaderOverlay::getProvider()
{
	return m_nProvider;
}

#ifdef __WXMSW__
void PokerReaderOverlay::wxMSWWindowPositioning()
{
	::GetWindowRect(m_PokerParent, &m_PokerParentRect);

	if( (m_PokerParentRectOld.right != m_PokerParentRect.right) || (m_PokerParentRectOld.top != m_PokerParentRect.top) )
	{
		SetSize(wxSize((m_PokerParentRect.right - m_PokerParentRect.left) + 200, (m_PokerParentRect.bottom - m_PokerParentRect.top) + 200));
		SetPosition(wxPoint(m_PokerParentRect.left - (200 / 2), m_PokerParentRect.top - (200 / 2)));

		//menu->m_ParentRect=parentrect;

		/*int i;
		if(PlayerOverlays.GetCount() > 0)
		for(i=0; i<PlayerOverlays.GetCount(); i++)
		PlayerOverlays.Item(i)->m_ParentRect=parentrect;*/

		UpdateControlPositions();

		m_PokerParentRectOld.right = m_PokerParentRect.right;
		m_PokerParentRectOld.top = m_PokerParentRect.top;
		m_PokerParentRectOld.left = m_PokerParentRect.left;
		m_PokerParentRectOld.bottom = m_PokerParentRect.bottom;
	}
}
#endif

void PokerReaderOverlay::PrepareToShow()
{
	FindAndOverlay();
	pConfig->SetTableMax(Data.TableData.maxseats);
	pConfig->SetProvider(getProvider());
	pConfig->LoadFile();
	pConfig->ReadPrefSeat(getProvider(), getTableMax(), (!Data.TableData.tournamentname.empty()));
	setVisualHeroSeat(pConfig->GetPrefSeatNumber());
	InitMenuCoordinates(true);
	InitSeatCoordinates(true);  // seat coordinates initiation
	ReadConfig();
	UpdatePlayerOverlayData();
	wxMSWWindowPositioning();
}

wxRegion PokerReaderOverlay::getRegion()
{
	return m_WindowRegion;
}

void PokerReaderOverlay::UpdatePlayerOverlayData()
{
	/*
	in Data werden alle Spieler die am letzen Spiel am Tisch teilgenommen haben als statisches Array �bergeben
	- Die Reihenfolge spielt keine Rolle; Daten
	- beispw. Sitznummer werden zus�tzich �bergeben
	- im Data befinden sich keine L�cken
	- unbesetzte Items sind mit 0 initialisiert

	Item=Data[i]

	0. Maximale Anzahl der Sitze des Tisches setzen

	1. F�r jedes Item f�r das noch kein PlayerOverlay-Objekt vorhanden ist (Item.seat nicht in der Menge von allen PlayerOverlay.GetSeatNumber() )
	a) neu erstellen
	b) Daten f�llen
	c) Position

	2. F�r jedes Items f�r das bereits ein PlayerOverlay-Objekt vorhanden ist (Item.seat ist in der Menge von allen PlayerOverlay.GetSeatNumber() )
	b) Daten f�llen

	3. F�r alle PlayerOverlay-Objekte f�r die keine Items �bergeben woden sind (PlayerOverlay.GetSeatNumber() nicht in der Menge von allen Item.seat )
	- l�schen
	- -> aus Anzeige entfernen

	4. Alle PlayerOverlay-Objekte
	- Anzeigen

	struct PlayerValues
	{
	int playercnt;
	int maxseats;
	long playerid;
	char name[100];
	int playertype; // rock, callst, lag, tag, fish etc.
	int seat;
	float vpip;
	float pfr;
	float af;
	float afonturn;
	float afonflop;
	float afonriver;
	int handcount;
	};

	*/

	Freeze();

	wxLogMessage(wxT("Updating playeroverlays (data)"));

	int x,x2;
	int seats[10]; // new seats by prefseat
	int seatdistance = 0, seatdistance2 = 0; // distance of regular and gui-seat
	wxPoint pos;
	EasyPlayerOverlay *EasyOverlay;
	ExtPlayerOverlay *ExtOverlay;
	bool PlayerOverlayExists;

	// parent window size
	double parentwidth=(m_PokerParentRect.right - m_PokerParentRect.left);
	double parentheight=(m_PokerParentRect.bottom - m_PokerParentRect.top);

	if(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value.find(L"true") == 0)
	{ 
		// show the extended OVERLAY
		for(int po=0; po < EasyPlayerOverlays.Count(); po++)
		{
			EasyOverlay = EasyPlayerOverlays.Item(po);
			EasyOverlay->Destroy();
		}
		EasyPlayerOverlays.Clear();

		// get hero and setup seat
		for(x=0; x<Data.TableData.playercnt; x++)
		{
			if (Data.TableData.PlayerVal[x].isownplayer == 1) // pref seat is for hero only !!!
			{
				setSeatDistance(getHeroSeatDistance(getVisualHeroSeat(), Data.TableData.PlayerVal[x].seat, pConfig->GetTableMax()));
			}
		}

		// 1.
		for(x=0; x<Data.TableData.playercnt; x++)
		{
			PlayerOverlayExists=false;

			for(x2=0; x2<ExtPlayerOverlays.Count(); x2++)
				if(ExtPlayerOverlays.Item(x2)->GetSeatNumber() == Data.TableData.PlayerVal[x].seat)
				{
					PlayerOverlayExists=true;
					ExtOverlay=ExtPlayerOverlays.Item(x2); // set the overlay
					ExtOverlay->SetVisualSeat(getVisualSeat(getSeatDistance(), Data.TableData.PlayerVal[x].seat, getTableMax())); // visual seat osd (pref.seating)
					//PlayerOverlay->ReverseMouse(m_bMouseReverse);
				}

				if(!PlayerOverlayExists)
				{
					// 1. create new
					ExtOverlay = new ExtPlayerOverlay(this);
					ExtPlayerOverlays.Add(ExtOverlay); // add to array holder
					ExtOverlay->SetVisualSeat(getVisualSeat(getSeatDistance(), Data.TableData.PlayerVal[x].seat, getTableMax())); // visual seat osd (pref.seating)
					ExtOverlay->SetXCoord(pConfig->GetSeatCoordX(ExtOverlay->GetVisualSeat())); // get coordinates for seats
					ExtOverlay->SetYCoord(pConfig->GetSeatCoordY(ExtOverlay->GetVisualSeat()));
					pos.x=ExtOverlay->GetXCoord()*parentwidth + 200/2; // recalculate new position (outsize to drag overlay outside of poker window)
					pos.y=ExtOverlay->GetYCoord()*parentheight + 200/2;
					ExtOverlay->SetPosition(pos);
				}

				// 1. und 2. b
				ExtOverlay->SetPlayerValues(Data.TableData.PlayerVal[x]); // playervalues
				ExtOverlay->SetSeatNumber(Data.TableData.PlayerVal[x].seat); // set seat number
				ExtOverlay->SetUserName(Data.TableData.PlayerVal[x].name); // set the username
				//PlayerOverlay->ReverseMouse(m_bMouseReverse);

		}

		// 3.
		for(x=ExtPlayerOverlays.Count()-1; x>=0; x--)
		{
			PlayerOverlayExists=false;

			for(x2=0; x2<Data.TableData.playercnt; x2++)
				if(ExtPlayerOverlays.Item(x)->GetSeatNumber() == Data.TableData.PlayerVal[x2].seat) // seat is equal
					PlayerOverlayExists=true; // the overlay already exists

			if(!PlayerOverlayExists) // player did not participate
			{
				ExtOverlay=ExtPlayerOverlays.Item(x);
				ExtOverlay->Destroy();
				ExtPlayerOverlays.Remove(ExtOverlay); // remove from list of overlays
			}
		}

		// 4.
		for(x=0; x<ExtPlayerOverlays.Count(); x++) // each overlay
		{
			ExtOverlay = ExtPlayerOverlays.Item(x);
			ExtOverlay->Show();
			ExtOverlay->PreparePlayerOverlay(); //prepare data
		}
	}
	else // show the easy overlay
	{
		// show the extended OVERLAY
		for(int po=0; po < ExtPlayerOverlays.Count(); po++)
		{
			ExtOverlay = ExtPlayerOverlays.Item(po);
			ExtOverlay->Destroy();
		}
		ExtPlayerOverlays.Clear();

		// get hero and setup seat
		for(x=0; x<Data.TableData.playercnt; x++)
		{
			if (Data.TableData.PlayerVal[x].isownplayer == 1) // pref seat is for hero only !!!
			{
				setSeatDistance(getHeroSeatDistance(getVisualHeroSeat(), Data.TableData.PlayerVal[x].seat, pConfig->GetTableMax()));
			}
		}

		// 1.
		for(x=0; x<Data.TableData.playercnt; x++)
		{
			PlayerOverlayExists=false;

			for(x2=0; x2<EasyPlayerOverlays.Count(); x2++)
				if(EasyPlayerOverlays.Item(x2)->GetSeatNumber() == Data.TableData.PlayerVal[x].seat)
				{
					PlayerOverlayExists=true;
					EasyOverlay=EasyPlayerOverlays.Item(x2); // set the overlay
					EasyOverlay->SetVisualSeat(getVisualSeat(getSeatDistance(), Data.TableData.PlayerVal[x].seat, getTableMax())); // visual seat osd (pref.seating)
					//PlayerOverlay->ReverseMouse(m_bMouseReverse);
				}

				if(!PlayerOverlayExists)
				{
					// 1. create new
					EasyOverlay = new EasyPlayerOverlay(this);
					EasyPlayerOverlays.Add(EasyOverlay); // add to array holder
					EasyOverlay->SetVisualSeat(getVisualSeat(getSeatDistance(), Data.TableData.PlayerVal[x].seat, getTableMax())); // visual seat osd (pref.seating)
					EasyOverlay->SetXCoord(pConfig->GetSeatCoordX(EasyOverlay->GetVisualSeat())); // get coordinates for seats
					EasyOverlay->SetYCoord(pConfig->GetSeatCoordY(EasyOverlay->GetVisualSeat()));
					pos.x=EasyOverlay->GetXCoord()*parentwidth + 200/2; // recalculate new position (outsize to drag overlay outside of poker window)
					pos.y=EasyOverlay->GetYCoord()*parentheight + 200/2;
					EasyOverlay->SetPosition(pos);
				}

				// 1. und 2. b
				EasyOverlay->SetPlayerValues(Data.TableData.PlayerVal[x]); // playervalues
				EasyOverlay->SetSeatNumber(Data.TableData.PlayerVal[x].seat); // set seat number
				EasyOverlay->SetUserName(Data.TableData.PlayerVal[x].name); // set the username
				//PlayerOverlay->ReverseMouse(m_bMouseReverse);

		}

		// 3.
		for(x=EasyPlayerOverlays.Count()-1; x>=0; x--)
		{
			PlayerOverlayExists=false;

			for(x2=0; x2<Data.TableData.playercnt; x2++)
				if(EasyPlayerOverlays.Item(x)->GetSeatNumber() == Data.TableData.PlayerVal[x2].seat) // seat is equal
					PlayerOverlayExists=true; // the overlay already exists

			if(!PlayerOverlayExists) // player did not participate
			{
				EasyOverlay=EasyPlayerOverlays.Item(x);
				EasyOverlay->Destroy();
				EasyPlayerOverlays.Remove(EasyOverlay); // remove from list of overlays
			}
		}

		// 4.
		for(x=0; x<EasyPlayerOverlays.Count(); x++) // each overlay
		{
			EasyOverlay = EasyPlayerOverlays.Item(x);
			EasyOverlay->Show();
			EasyOverlay->PreparePlayerOverlay(); //prepare data
		}
		//// 0. 0. Maximale Anzahl der Sitze des Tisches setzen
		//if(!pConfig->GetTableMax()) // max. seats on table
		//{
		//	pConfig->SetTableMax(getTableMax());
		//	InitSeatCoordinates(true);  // seat coordinates initiation
		//}

		//for(int po=0; po < ExtPlayerOverlays.Count(); po++)
		//{
		//	ExtOverlay = ExtPlayerOverlays.Item(po);
		//	ExtOverlay->Destroy();
		//}
		//ExtPlayerOverlays.Clear();

		////if(m_bFirstrun)
		////{
		//// read and init pref seat for hero by provider
		//pConfig->ReadPrefSeat(getProvider(), getTableMax(), (!Data.TableData.tournamentname.empty()));
		////}
		//// set the virtual seat
		////SetVisualSeat(pConfig->GetPrefSeatNumber());

		///*if(GetVisualSeat() == Definitions::AutoCenter::ISTRUE) // autorotate (FULLTILT)
		//{
		//switch(GetProvider())
		//{
		//case Definitions::Providers::FULLTILT:
		//if(Data.maxseats == 9)
		//SetVisualSeat(5);
		//if(Data.maxseats == 6)
		//SetVisualSeat(3);
		//break;
		//}
		//m_bFirstrun = false;
		//}*/

		//// get hero and setup seat
		//for(x=0; x<Data.TableData.playercnt; x++)
		//{
		//	seats[x] = Data.TableData.PlayerVal[x].seat; // set every seat to original seat
		//	if (Data.TableData.PlayerVal[x].isownplayer == 1) // pref seat is for hero only !!!
		//	{
		//		// pref seat set
		//		/*if((GetVisualSeat() > -1) && (GetVisualSeat() < 11)) // pref seat set ?
		//		{
		//		//seats[x] = GetVisualSeat(); // set hero's seat (pref seat) from config

		//		seatdistance = GetVisualSeat() - Data.PlayerVal[x].seat;
		//		}*/
		//	}
		//}

		//// setup all seats
		//for(x=0; x<Data.TableData.playercnt; x++)
		//{
		//	if(seatdistance != 0) // seatdistance min 1 !!!
		//	{
		//		if (seatdistance > 0)
		//		{
		//			seats[x] = Data.TableData.PlayerVal[x].seat + seatdistance; // set new seat

		//			if(seats[x] > getTableMax())
		//			{
		//				seats[x] = (getTableMax() - seats[x]) * -1;
		//			}
		//		}
		//		else
		//		{
		//			seats[x] = getTableMax() - (((seatdistance* -1) - (Data.TableData.PlayerVal[x].seat))); // set new seat by subtraction of max

		//			if(seats[x] > getTableMax())
		//			{
		//				seats[x] = (pConfig->GetTableMax() - seats[x]) * -1;
		//			}
		//		}

		//	}
		//}

		//// 1.
		//for(x=0; x<Data.TableData.playercnt; x++)
		//{
		//	PlayerOverlayExists=false;

		//	for(x2=0; x2<EasyPlayerOverlays.Count(); x2++)
		//		if(EasyPlayerOverlays.Item(x2)->GetSeatNumber() == Data.TableData.PlayerVal[x].seat)
		//		{
		//			PlayerOverlayExists=true;
		//			EasyOverlay=EasyPlayerOverlays.Item(x2); // set the overlay
		//			//PlayerOverlay->ReverseMouse(m_bMouseReverse);
		//		}

		//		if(!PlayerOverlayExists)
		//		{
		//			// 1. create new
		//			EasyOverlay = new EasyPlayerOverlay(this);
		//			//PlayerOverlay->m_ParentRect=parentrect; // set rect
		//			//PlayerOverlay->ReverseMouse(m_bMouseReverse);

		//			// overlay is for the own player and a pref seat was set, set the visual seat for ech player
		//			EasyOverlay->SetVisualSeat(seats[x]); // visual seat osd (pref.seating)

		//			EasyPlayerOverlays.Add(EasyOverlay); // add to array holder
		//		}

		//		// 1. und 2. b
		//		EasyOverlay->SetPlayerValues(Data.TableData.PlayerVal[x]); // playervalues
		//		EasyOverlay->SetSeatNumber(Data.TableData.PlayerVal[x].seat); // set seat number
		//		EasyOverlay->SetUserName(Data.TableData.PlayerVal[x].name); // set the username
		//		//PlayerOverlay->SetProvider(GetProvider());
		//		//PlayerOverlay->SetMenu(menu);
		//		//PlayerOverlay->ReverseMouse(m_bMouseReverse);

		//		if(!PlayerOverlayExists)
		//		{
		//			// 1. c
		//			EasyOverlay->SetXCoord(pConfig->GetSeatCoordX(seats[x])); // get coordinates for seats
		//			EasyOverlay->SetYCoord(pConfig->GetSeatCoordY(seats[x]));

		//			pos.x=EasyOverlay->GetXCoord()*parentwidth + 200/2; // recalculate new position (outsize to drag overlay outside of poker window)
		//			pos.y=EasyOverlay->GetYCoord()*parentheight + 200/2;

		//			EasyOverlay->SetPosition(pos);
		//			//PlayerOverlay->ReverseMouse(m_bMouseReverse);
		//		}
		//}

		//// 3.
		//for(x=EasyPlayerOverlays.Count()-1; x>=0; x--)
		//{
		//	PlayerOverlayExists=false;

		//	for(x2=0; x2<Data.TableData.playercnt; x2++)
		//		if(EasyPlayerOverlays.Item(x)->GetSeatNumber() == Data.TableData.PlayerVal[x2].seat) // seat is equal
		//			PlayerOverlayExists=true; // the overlay already exists

		//	if(!PlayerOverlayExists) // player did not participate
		//	{
		//		EasyOverlay=EasyPlayerOverlays.Item(x);
		//		//PlayerOverlay->ReverseMouse(m_bMouseReverse);

		//		EasyOverlay->Destroy();

		//		EasyPlayerOverlays.Remove(EasyOverlay); // remove from list of overlays
		//	}
		//}

		//// 4.
		//for(x=0; x<EasyPlayerOverlays.Count(); x++) // each overlay
		//{
		//	EasyOverlay = EasyPlayerOverlays.Item(x);
		//	//PlayerOverlay->ReverseMouse(m_bMouseReverse);
		//	EasyOverlay->Show();
		//	EasyOverlay->PreparePlayerOverlay(); //prepare data
		//}
	}

	UpdateControlPositions(); // finally reposition
	Thaw();
	Refresh();
	//ClearContainer();
}

// update the position of controls
void PokerReaderOverlay::UpdateControlPositions()
{
	int i=0, xm=0, ym=0, xc=0, yc=0;

	double parentwidth, parentheight;
	double xm_d, ym_d;
	double xc_d, yc_d;

	wxString fmt;

	parentwidth = (getPokerParentRect().right - getPokerParentRect().left);

	parentheight = (getPokerParentRect().bottom - getPokerParentRect().top);

	xm_d = m_menu->GetXCoord()*parentwidth;
	ym_d = m_menu->GetYCoord()*parentheight;

	xm=xm_d;
	ym=ym_d;

	xm=xm_d;
	ym=ym_d;

	m_menu->SetPosition(wxPoint(xm+100,ym+100));

	if(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value.find(L"true") == 0)
	{
		if(ExtPlayerOverlays.GetCount() > 0)
		{
			for(i=0; i<=(ExtPlayerOverlays.GetCount() -1); i++)
			{
				xc_d = ExtPlayerOverlays.Item(i)->GetXCoord()*parentwidth;
				yc_d = ExtPlayerOverlays.Item(i)->GetYCoord()*parentheight;

				xc=xc_d;
				yc=yc_d;

				ExtPlayerOverlays.Item(i)->SetPosition(wxPoint(xc+100,yc+100));
				ExtPlayerOverlays.Item(i)->Refresh();
			}
		}
	}
	else
	{
		if(EasyPlayerOverlays.GetCount() > 0)
		{
			for(i=0; i<=(EasyPlayerOverlays.GetCount() -1); i++)
			{
				xc_d = EasyPlayerOverlays.Item(i)->GetXCoord()*parentwidth;
				yc_d = EasyPlayerOverlays.Item(i)->GetYCoord()*parentheight;

				xc=xc_d;
				yc=yc_d;

				EasyPlayerOverlays.Item(i)->SetPosition(wxPoint(xc+100,yc+100));
				EasyPlayerOverlays.Item(i)->Refresh();
			}
		}
	}

	m_menu->Refresh();
}

// initialize the menu-coordinates
void PokerReaderOverlay::InitMenuCoordinates(bool fromfile)
{
	//pConfig->SetTableMax(nSeats);
	pConfig->ReadMenuCoords(fromfile);
	//pConfig->ReadSeatCoords(fromfile);

	int x,y;

	m_menu->SetXCoord(pConfig->GetMenuX());
	m_menu->SetYCoord(pConfig->GetMenuY());

	double parentwidth=(getPokerParentRect().right - getPokerParentRect().left);
	double parentheight=(getPokerParentRect().bottom - getPokerParentRect().top);

	x=m_menu->GetXCoord()*parentwidth;
	y=m_menu->GetYCoord()*parentheight;

	m_menu->SetPosition(wxPoint(x,y));

	// set the position
	/*
	x = pConfig->GetMenuX();
	y = pConfig->GetMenuY();

	SetPosition(wxPoint(x,y));

	double parentwidth=(m_ParentRect.right - m_ParentRect.left);
	double parentheight=(m_ParentRect.bottom - m_ParentRect.top);

	double mposx=this->GetPosition().x-100;
	double mposy=this->GetPosition().y-100;

	this->Coord1X=mposx/parentwidth;
	this->Coord1Y=mposy/parentheight;
	*/
}

// initialize the seat-coordinates
void PokerReaderOverlay::InitSeatCoordinates(bool fromfile)
{
	//pConfig->SetTableMax(nSeats);
	pConfig->ReadSeatCoords(fromfile);
}

// return player data-popup
SimpleDataPopup* PokerReaderOverlay::GetEasyDataPopup()
{
	return m_EasyDataPopup;
}

/*
// file was modified ?
bool PokerReaderOverlay::RecordFileModified()
{
bool result = false;
//wxFileName* file = new wxFileName(); // MOD 24.08.09 JS: old code

// MOD 24.08.09 JS: new code added
//if(m_PlayerValues->GetFileName().IsEmpty()) // file name is empty
//{
// set file name
m_PlayerValues->SetFileByTableName(GetWindowName(), GetProvider()); // MOD 24.08.09 JS: added, find filename by tablename and provider
//}

return CheckIfFileWasModified();
}

// check if file was modified
bool PokerReaderOverlay::CheckIfFileWasModified()
{
bool result = false;
wxFileName* file = new wxFileName();

file->SetName(m_PlayerValues->GetFileName());
if(file->FileExists())
{
m_dtRecordfileMod = file->GetModificationTime();

if (m_dtRecordfileMod != m_dtLastRecordfileMod)
{
m_dtLastRecordfileMod = m_dtRecordfileMod;
result = true;
}
else
{
result  = false;
}
}
return result;
}
*/

// get the window name
wxString PokerReaderOverlay::getWindowName()
{
	return GetTitle();
}

void PokerReaderOverlay::OnMenuSaveLayoutClick( wxCommandEvent& event )
{
	pConfig->SaveLayout();
	wxLogMessage(wxT("Layout saved"));
}

/*!
* wxEVT_COMMAND_MENU_SELECTED event handler for ID_MENUITEMRESETLAYOUT
*/

void PokerReaderOverlay::OnMenuResetLayoutClick( wxCommandEvent& event )
{
	EasyPlayerOverlay * EasyOverlay;
	ExtPlayerOverlay * ExtOverlay;
	int x;
	//Freeze();
	InitMenuCoordinates(true);
	pConfig->SetTableMax(getTableMax());
	InitSeatCoordinates(true);

	//wxFileConfig* config = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);

	UpdateSeatCoordinates();

	//delete config;

	UpdateControlPositions();
	UpdatePlayerOverlayData();

	wxLogMessage(wxT("Layout reset"));
	//ClearContainer();
	//Thaw();
}

void PokerReaderOverlay::OnMenuTransparencyClick( wxCommandEvent& event )
{
	m_TransDlg->Show();
}

void PokerReaderOverlay::OnMenuReverseMouseButtonsClick(wxCommandEvent &event)
{
	//wxFileConfig* config = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);

	bool val = wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0;
	val = !val; // inverse

	if(val)
		wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value = L"true";
	else
		wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value = L"false";

	setMouseReverse(val);

	wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].settingname = CONF_HUDMOUSEREVERSE;
	OnConfigChange(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE]);
}

void PokerReaderOverlay::OnShowHeroSymbolClick(wxCommandEvent &event)
{
	//wxFileConfig* config = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);

	bool val = wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") == 0;
	val = !val; // inverse

	if(val)
		wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value = L"true";
	else
		wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value = L"false";

	setHeroSymbol(val);

	wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].settingname = CONF_HUDPLAYERSYMBOL;
	OnConfigChange(wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL]);

	//delete config;
}

// returns the menu
wxMenu* PokerReaderOverlay::GetMenu()
{
	return m_MenuBar;
}

// reverse mouse buttons
void PokerReaderOverlay::setMouseReverse(bool reverse)
{
	if(reverse)
		wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value = L"true";
	else
		wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value = L"false";

	m_MenuBar->Check(ID_MENUSETREVERSEMOUSEBUTTONS, reverse);
}

ProviderConf* PokerReaderOverlay::getProviderConfig()
{
	return pConfig;
}


void PokerReaderOverlay::setHudData(HudData data)
{
	this->Data = data;
}

HudData PokerReaderOverlay::getHudData()
{
	return Data;
}

/*
// return the virtual seat
int PokerReaderOverlay::GetVisualSeat()
{
//return m_nVisualSeat;
}
*/

// new seat for hero
void PokerReaderOverlay::OnTakeNewSeat(wxCommandEvent& event)
{
	long getnumber = 0;

	getnumber = wxGetNumberFromUser(_("Bitte geben Sie Ihren neuen Sitzplatz an"), _("Neuer Sitzplatz"), _("Umsetzung"), 1, 1, pConfig->GetTableMax(), this, wxDefaultPosition); // pokerstars has max 10 seats

	int visualseat = static_cast<int>(getnumber);

	if((visualseat > 0) && (visualseat <= pConfig->GetTableMax()))
	{
		setVisualHeroSeat(visualseat);
		removeOverlays();
		UpdatePlayerOverlayData();
	}
}

void PokerReaderOverlay::setHeroSymbol(bool val)
{
	m_MenuBar->Check(ID_MENUSETOWNPLAYERSYMBOL, val);
}

// set transparency for all subwindows
void PokerReaderOverlay::setWindowTransparency(byte transval)
{
	for(int count=0; count < GetChildren().GetCount(); count++)
	{
		m_WinListNode = GetChildren().Item(count);
		m_WinListNode->GetData()->SetTransparent(transval);
	}

	::SetLayeredWindowAttributes(GetHwnd(), RGB(255, 0, 255), transval, LWA_COLORKEY | LWA_ALPHA);
}

void PokerReaderOverlay::ReadConfig()
{

	if(wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"true") != std::wstring::npos)
		setHeroSymbol(true);
	else if(wxGetApp().ConfigSetting[CONF_HUDPLAYERSYMBOL].value.find(L"false") != std::wstring::npos)
		setHeroSymbol(false);

	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") != std::wstring::npos)
		setMouseReverse(true);
	else if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"false") != std::wstring::npos)
		setMouseReverse(false);

	if(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value.find(L"true") != std::wstring::npos)
		setExtendedOverlay(true);
	else if(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value.find(L"false") != std::wstring::npos)
		setExtendedOverlay(false);

	setWindowTransparency(static_cast<byte>(_wtoi(wxGetApp().ConfigSetting[CONF_HUDTRANS].value.c_str())));

}

void PokerReaderOverlay::OnActivateEasyHud(wxCommandEvent &event)
{
	/*
	wxFileConfig* config = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);

	config->Write(CONF_HUDEXTENDEDOVERLAY, false);
	config->Flush();

	delete config;
	*/
	wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value = L"false";
	OnConfigChange(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY]);
}

void PokerReaderOverlay::OnActivateExtendedHud(wxCommandEvent &event)
{
	wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value = L"true";
	OnConfigChange(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY]);
}

void PokerReaderOverlay::setExtendedOverlay(bool val)
{
	if(val)
	{
		m_OverlayStyleMenu->Check(ID_MENUSETEXTENDEDEASYPLAYEROVERLAY, true);
		m_OverlayStyleMenu->Check(ID_MENUSETEASYPLAYEROVERLAY, false);
	}
	else
	{
		m_OverlayStyleMenu->Check(ID_MENUSETEXTENDEDEASYPLAYEROVERLAY, false);
		m_OverlayStyleMenu->Check(ID_MENUSETEASYPLAYEROVERLAY, true);
	}

	//wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY] = wxGetApp().BuildConfigDataForHUD(CONF_HUDEXTENDEDOVERLAY, val);
	//wxGetApp().SendConfigSettingToGui(wxGetApp().BuildConfigDataForHUD(CONF_HUDEXTENDEDOVERLAY, val));
}

bool PokerReaderOverlay::ParentTitleChanged()
{
#ifdef __WXMSW__
	TCHAR charbuffer[MAX_PATH];
	::GetWindowText(getPokerParent(), charbuffer, MAX_PATH);

	if(wxString(charbuffer).Find(wxString(this->Data.TableData.tablename)) > wxNOT_FOUND )
	{
		return true;
	}
	else
		return false;
#endif
}

void PokerReaderOverlay::setPokerParentWindowname(wxString windowname)
{
	m_PokerParentName = windowname;
}

void PokerReaderOverlay::OnConfigChange(ConfigRecord confrec)
{
	wxGetApp().SendConfigSettingToGui(confrec);
}

void PokerReaderOverlay::setVisualHeroSeat(int vseat)
{
	m_nVisualHeroSeat = vseat;
}

int PokerReaderOverlay::getVisualHeroSeat()
{
	return m_nVisualHeroSeat;
}

int PokerReaderOverlay::getVisualSeat(int seatdistance, int originalseat, int tablemax)
{
	int visualseat = 0;

	visualseat = originalseat + seatdistance;

	if(visualseat > tablemax)
		visualseat -= tablemax;
	return visualseat;
}

int PokerReaderOverlay::getHeroSeatDistance(int visualseat, int originalseat, int tablemax)
{
	int distance = 0;

	// if no visual seat available, return a 0 distance
	if(visualseat <= 0)
		return 0;

	if(visualseat > originalseat)
		distance = visualseat - originalseat;
	else
		distance = tablemax - (originalseat - visualseat);

	return distance;
}

void PokerReaderOverlay::setSeatDistance(int distance)
{
	m_nSeatDistance = distance;
}

int PokerReaderOverlay::getSeatDistance()
{
	return m_nSeatDistance;
}

// set max player on table
void PokerReaderOverlay::setTableMax(int max)
{
	m_nTableMax = max;
}

// max player at table
int PokerReaderOverlay::getTableMax()
{
	return m_nTableMax;
}

void PokerReaderOverlay::UpdateSeatCoordinates()
{
	EasyPlayerOverlay * EasyOverlay;
	ExtPlayerOverlay * ExtOverlay;
	int x = 0;

	if(wxGetApp().ConfigSetting[CONF_HUDEXTENDEDOVERLAY].value.find(L"true") == 0)
	{
		for(x=0; x<ExtPlayerOverlays.Count(); x++)
		{
			ExtOverlay = ExtPlayerOverlays.Item(x);
			if (ExtOverlay->GetVisualSeat() < 0) // pref seat not set
			{
				ExtOverlay->SetXCoord(pConfig->GetSeatCoordX(ExtOverlay->GetSeatNumber()));
				ExtOverlay->SetYCoord(pConfig->GetSeatCoordY(ExtOverlay->GetSeatNumber()));
			}
			else
			{
				ExtOverlay->SetXCoord(pConfig->GetSeatCoordX(ExtOverlay->GetVisualSeat()));
				ExtOverlay->SetYCoord(pConfig->GetSeatCoordY(ExtOverlay->GetVisualSeat()));
			}
		}
	}
	else
	{
		for(x=0; x<EasyPlayerOverlays.Count(); x++)
		{
			EasyOverlay = EasyPlayerOverlays.Item(x);
			if (EasyOverlay->GetVisualSeat() < 0) // pref seat not set
			{
				EasyOverlay->SetXCoord(pConfig->GetSeatCoordX(EasyOverlay->GetSeatNumber()));
				EasyOverlay->SetYCoord(pConfig->GetSeatCoordY(EasyOverlay->GetSeatNumber()));
			}
			else
			{
				EasyOverlay->SetXCoord(pConfig->GetSeatCoordX(EasyOverlay->GetVisualSeat()));
				EasyOverlay->SetYCoord(pConfig->GetSeatCoordY(EasyOverlay->GetVisualSeat()));
			}
		}
	}
}

void PokerReaderOverlay::removeOverlays()
{
	for(int i = 0; i < ExtPlayerOverlays.GetCount(); i++)
	{
		ExtPlayerOverlays.Item(i)->Destroy();
	}

	for(int x = 0; x < EasyPlayerOverlays.GetCount(); x++)
	{
		EasyPlayerOverlays.Item(x)->Destroy();
	}

	ExtPlayerOverlays.Clear();
	EasyPlayerOverlays.Clear();
}