/////////////////////////////////////////////////////////////////////////////
// Name:        overlaymenu.cpp
// Purpose:
// Author:
// Modified by:
// Created:     09/03/2010 15:46:02
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "overlaymenu.h"
#include "pokerreaderoverlay.h"
#include "../pokerreaderhudapp.h"
#include "../../pokerreadercommon/helper/imagehandler.h"

////@begin XPM images
////@end XPM images

/*
 * OverlayMenu type definition
 */

IMPLEMENT_DYNAMIC_CLASS( OverlayMenu, OverlayPanel )

/*
 * OverlayMenu event table definition
 */

BEGIN_EVENT_TABLE( OverlayMenu, OverlayPanel )

////@begin OverlayMenu event table entries
    EVT_PAINT( OverlayMenu::OnPaint )
    EVT_ERASE_BACKGROUND( OverlayMenu::OnEraseBackground )
    EVT_LEFT_DOWN( OverlayMenu::OnLeftDown )
    EVT_LEFT_UP( OverlayMenu::OnLeftUp )
    EVT_RIGHT_DOWN( OverlayMenu::OnRightDown )
    EVT_RIGHT_UP( OverlayMenu::OnRightUp )
    EVT_MOTION( OverlayMenu::OnMotion )

////@end OverlayMenu event table entries

END_EVENT_TABLE()

/*
 * OverlayMenu constructors
 */

OverlayMenu::OverlayMenu()
{
    Init();
	m_bmpMenu = NULL;
}

OverlayMenu::OverlayMenu(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
    Init();
    Create(parent, id, pos, size, style);
}

/*
 * wxOverlayMenu creator
 */

bool OverlayMenu::Create(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style)
{
////@begin OverlayMenu creation
    OverlayPanel::Create(parent, id, pos, size, style);
    CreateControls();
////@end OverlayMenu creation
    return true;
}

/*
 * OverlayMenu destructor
 */

OverlayMenu::~OverlayMenu()
{
////@begin OverlayMenu destruction
////@end OverlayMenu destruction
}

/*
 * Member initialisation
 */

void OverlayMenu::Init()
{
////@begin OverlayMenu member initialisation
    m_bmpMenu = NULL;
////@end OverlayMenu member initialisation
}

/*
 * Control creation for wxOverlayMenu
 */

void OverlayMenu::CreateControls()
{
////@begin OverlayMenu content construction
    OverlayMenu* itemOverlayPanel1 = this;

    m_bmpMenu = new wxStaticBitmap( itemOverlayPanel1, wxID_MENUBITMAP, wxNullBitmap, wxPoint(0, 0), wxSize(29, 25), wxNO_BORDER );

    // Connect events and objects
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_LEFT_DOWN, wxMouseEventHandler(OverlayMenu::OnLeftDown), NULL, this);
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_LEFT_UP, wxMouseEventHandler(OverlayMenu::OnLeftUp), NULL, this);
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_LEFT_DCLICK, wxMouseEventHandler(OverlayMenu::OnLeftDClick), NULL, this);
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_RIGHT_DOWN, wxMouseEventHandler(OverlayMenu::OnRightDown), NULL, this);
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_RIGHT_UP, wxMouseEventHandler(OverlayMenu::OnRightUp), NULL, this);
    m_bmpMenu->Connect(wxID_MENUBITMAP, wxEVT_MOTION, wxMouseEventHandler(OverlayMenu::OnMotion), NULL, this);
////@end OverlayMenu content construction


	//wxBitmap()
	//m_bmpMenu = new wxBitmap(menu_normal_xpm);

	m_bmpMenu->SetBitmap(wxGetApp().GetImages()->GetBitmap(wxT("images/overlay/menu_normal.png"))); // TODO: reimplement
	SetBackgroundColour(wxColour(255,0,255));
}

/*
 * Should we show tooltips?
 */

bool OverlayMenu::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap OverlayMenu::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin OverlayMenu bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end OverlayMenu bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon OverlayMenu::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin OverlayMenu icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end OverlayMenu icon retrieval
}

/*
 * wxEVT_LEFT_DOWN event handler for wxID_STATIC
 */

void OverlayMenu::OnLeftDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_DOWN event handler for wxID_STATIC in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DOWN event handler for wxID_STATIC in wxOverlayMenu.
	*/

	OverlayPanel::OnLeftDown(event);
	if(!(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0))
	{
		PokerReaderOverlay* overlay = wxDynamicCast(GetParent(), PokerReaderOverlay);
		PopupMenu(overlay->GetMenu());
	}
	event.Skip();
}

/*
 * wxEVT_LEFT_UP event handler for wxID_STATIC
 */

void OverlayMenu::OnLeftUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_LEFT_UP event handler for wxID_STATIC in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_UP event handler for wxID_STATIC in wxOverlayMenu.
	*/
	OverlayPanel::OnLeftUp(event);

	PokerReaderOverlay* theParent = wxDynamicCast(GetParent(), PokerReaderOverlay);
	theParent->getProviderConfig()->SetMenuCoord(GetXCoord(), GetYCoord());
}

/*
 * wxEVT_RIGHT_DOWN event handler for wxID_STATIC
 */

void OverlayMenu::OnRightDown( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_DOWN event handler for wxID_STATIC in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_DOWN event handler for wxID_STATIC in wxOverlayMenu.
	*/

	OverlayPanel::OnRightDown(event);
	if(wxGetApp().ConfigSetting[CONF_HUDMOUSEREVERSE].value.find(L"true") == 0)
	{
		PokerReaderOverlay* overlay = wxDynamicCast(GetParent(), PokerReaderOverlay);
		PopupMenu(overlay->GetMenu());
	}
	event.Skip();
}

/*
 * wxEVT_RIGHT_UP event handler for wxID_STATIC
 */

void OverlayMenu::OnRightUp( wxMouseEvent& event )
{
	/*
////@begin wxEVT_RIGHT_UP event handler for wxID_STATIC in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_RIGHT_UP event handler for wxID_STATIC in wxOverlayMenu.
	*/
	OverlayPanel::OnRightUp(event);
	PokerReaderOverlay* theParent = wxDynamicCast(GetParent(), PokerReaderOverlay);
	theParent->getProviderConfig()->SetMenuCoord(GetXCoord(), GetYCoord());
}

/*
 * wxEVT_MOTION event handler for wxID_STATIC
 */

void OverlayMenu::OnMotion( wxMouseEvent& event )
{
	/*
////@begin wxEVT_MOTION event handler for wxID_STATIC in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_MOTION event handler for wxID_STATIC in wxOverlayMenu.
	*/
	OverlayPanel::OnMotion(event);//
}

/*
 * wxEVT_PAINT event handler for ID_PANELMENUOVERLAY
 */

void OverlayMenu::OnPaint( wxPaintEvent& event )
{
////@begin wxEVT_PAINT event handler for ID_PANELMENUOVERLAY in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    wxPaintDC dc(this);
////@end wxEVT_PAINT event handler for ID_PANELMENUOVERLAY in wxOverlayMenu.
	//dc.DrawBitmap(m_bmpMenu->GetSubBitmap(wxRect(wxPoint(0,0), )), 0, 0);
	event.Skip();
}

/*
 * wxEVT_ERASE_BACKGROUND event handler for ID_PANELMENUOVERLAY
 */

void OverlayMenu::OnEraseBackground( wxEraseEvent& event )
{
////@begin wxEVT_ERASE_BACKGROUND event handler for ID_PANELMENUOVERLAY in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_ERASE_BACKGROUND event handler for ID_PANELMENUOVERLAY in wxOverlayMenu.
}

/*
 * wxEVT_LEFT_DCLICK event handler for wxID_MENUBITMAP
 */

void OverlayMenu::OnLeftDClick( wxMouseEvent& event )
{
////@begin wxEVT_LEFT_DCLICK event handler for wxID_MENUBITMAP in wxOverlayMenu.
    // Before editing this code, remove the block markers.
    event.Skip();
////@end wxEVT_LEFT_DCLICK event handler for wxID_MENUBITMAP in wxOverlayMenu.
}