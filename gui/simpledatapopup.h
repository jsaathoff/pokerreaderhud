/////////////////////////////////////////////////////////////////////////////
// Name:        simpledatapopup.h
// Purpose:
// Author:
// Modified by:
// Created:     22/06/2011 16:13:33
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _SIMPLEDATAPOPUP_H_
#define _SIMPLEDATAPOPUP_H_

/*!
 * Includes
 */

////@begin includes
#include "wx/frame.h"
////@end includes

#include "../../pokerreadercommon/helper/playerrecord.h"
#include "../../pokerreadercommon/helper/valuedescription.h"
#include "outeroverlaypanel.h"

/*!
 * Forward declarations
 */

////@begin forward declarations
class wxFlexGridSizer;
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
#define ID_SIMPLEDATAPOPUP 10001
#define SYMBOL_SIMPLEDATAPOPUP_STYLE wxFRAME_NO_TASKBAR
#define SYMBOL_SIMPLEDATAPOPUP_TITLE _T("Simple Data Popup")
#define SYMBOL_SIMPLEDATAPOPUP_IDNAME ID_SIMPLEDATAPOPUP
#define SYMBOL_SIMPLEDATAPOPUP_SIZE wxDefaultSize
#define SYMBOL_SIMPLEDATAPOPUP_POSITION wxDefaultPosition
////@end control identifiers

/*!
 * SimpleDataPopup class declaration
 */

class SimpleDataPopup: public OuterOverlayPanel
{
    DECLARE_CLASS( SimpleDataPopup )
    DECLARE_EVENT_TABLE()

public:
    /// Constructors
    SimpleDataPopup();
    SimpleDataPopup( wxWindow* parent, wxWindowID id = SYMBOL_SIMPLEDATAPOPUP_IDNAME, const wxString& caption = SYMBOL_SIMPLEDATAPOPUP_TITLE, const wxPoint& pos = SYMBOL_SIMPLEDATAPOPUP_POSITION, const wxSize& size = SYMBOL_SIMPLEDATAPOPUP_SIZE, long style = SYMBOL_SIMPLEDATAPOPUP_STYLE );

    bool Create( wxWindow* parent, wxWindowID id = SYMBOL_SIMPLEDATAPOPUP_IDNAME, const wxString& caption = SYMBOL_SIMPLEDATAPOPUP_TITLE, const wxPoint& pos = SYMBOL_SIMPLEDATAPOPUP_POSITION, const wxSize& size = SYMBOL_SIMPLEDATAPOPUP_SIZE, long style = SYMBOL_SIMPLEDATAPOPUP_STYLE );

    /// Destructor
    ~SimpleDataPopup();

    /// Initialises member variables
    void Init();

    /// Creates the controls and sizers
    void CreateControls();

	// set the values
	void SetValues(PlayerValueRecord* val);

	void CreateTable();

	// set new parent of window
	void SetNewParent(HWND window);

////@begin SimpleDataPopup event handler declarations

    /// wxEVT_PAINT event handler for ID_SIMPLEDATAPOPUP
    void OnPaint( wxPaintEvent& event );

////@end SimpleDataPopup event handler declarations

////@begin SimpleDataPopup member function declarations

    /// Retrieves bitmap resources
    wxBitmap GetBitmapResource( const wxString& name );

    /// Retrieves icon resources
    wxIcon GetIconResource( const wxString& name );
////@end SimpleDataPopup member function declarations

    /// Should we show tooltips?
    static bool ShowToolTips();

////@begin SimpleDataPopup member variables
    wxStaticText* m_lblPlayername;
    wxFlexGridSizer* m_MainSizer;
////@end SimpleDataPopup member variables

	// the positions
	wxPoint m_start_object_pos;
	wxPoint m_start_mouse_pos;
	RECT m_MainRect;
	PlayerValueRecord* m_Values;
	wxStaticText* m_lblLabel[30];
	wxStaticText* m_lblValue[30];
	wxStaticText* m_lblDesc[30];
	wxFont m_FontLabel;
	wxColour m_ColourLabel;
	//ValueDescription m_Descriptions;
	bool m_bIsShown;

	HWND m_PokerParent;
};

#endif
    // _SIMPLEDATAPOPUP_H_