/////////////////////////////////////////////////////////////////////////////
// Name:        simpledatapopup.cpp
// Purpose:     
// Author:      
// Modified by: 
// Created:     22/06/2011 16:13:33
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "simpledatapopup.h"

////@begin XPM images
////@end XPM images


/*
 * SimpleDataPopup type definition
 */

IMPLEMENT_CLASS( SimpleDataPopup, OuterOverlayPanel )


/*
 * SimpleDataPopup event table definition
 */

BEGIN_EVENT_TABLE( SimpleDataPopup, OuterOverlayPanel )

////@begin SimpleDataPopup event table entries
    EVT_PAINT( SimpleDataPopup::OnPaint )

////@end SimpleDataPopup event table entries

END_EVENT_TABLE()


/*
 * SimpleDataPopup constructors
 */

SimpleDataPopup::SimpleDataPopup()
{
    Init();
}

SimpleDataPopup::SimpleDataPopup( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
    Init();
    Create( parent, id, caption, pos, size, style );
}


/*
 * SimpleDataPopup creator
 */

bool SimpleDataPopup::Create( wxWindow* parent, wxWindowID id, const wxString& caption, const wxPoint& pos, const wxSize& size, long style )
{
////@begin SimpleDataPopup creation
    OuterOverlayPanel::Create( parent, id, caption, pos, size, style );

    this->SetForegroundColour(wxColour(255, 255, 255));
    this->SetBackgroundColour(wxColour(0, 0, 0));
    CreateControls();
    if (GetSizer())
    {
        GetSizer()->SetSizeHints(this);
    }
////@end SimpleDataPopup creation
    return true;
}


/*
 * SimpleDataPopup destructor
 */

SimpleDataPopup::~SimpleDataPopup()
{
////@begin SimpleDataPopup destruction
////@end SimpleDataPopup destruction
}


/*
 * Member initialisation
 */

void SimpleDataPopup::Init()
{
////@begin SimpleDataPopup member initialisation
    m_lblPlayername = NULL;
    m_MainSizer = NULL;
////@end SimpleDataPopup member initialisation
}


/*
 * Control creation for SimpleDataPopup
 */

void SimpleDataPopup::CreateControls()
{    
////@begin SimpleDataPopup content construction
    SimpleDataPopup* itemOuterOverlayPanel1 = this;

    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    itemOuterOverlayPanel1->SetSizer(itemBoxSizer2);

    m_lblPlayername = new wxStaticText( itemOuterOverlayPanel1, wxID_STATIC, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    m_lblPlayername->SetFont(wxFont(11, wxSWISS, wxNORMAL, wxBOLD, false, wxT("Tahoma")));
    itemBoxSizer2->Add(m_lblPlayername, 0, wxGROW|wxALL, 3);

    m_MainSizer = new wxFlexGridSizer(22, 3, 0, 2);
    itemBoxSizer2->Add(m_MainSizer, 1, wxGROW|wxALL, 2);

////@end SimpleDataPopup content construction

	m_lblPlayername->Connect(wxID_ANY, wxEVT_RIGHT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnRightDown), 0, this);
	m_lblPlayername->Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(SimpleDataPopup::OnMotion), 0, this);
	m_lblPlayername->Connect(wxID_ANY, wxEVT_RIGHT_UP, wxMouseEventHandler(SimpleDataPopup::OnRightUp), 0, this);
	m_lblPlayername->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnLeftDown), 0, this);
	m_FontLabel = wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma"));
	m_ColourLabel = wxColour(255, 255, 255);
	//m_bIsShown = false;
	//m_bPosCheck = false;
	CreateTable();
}


/*
 * Should we show tooltips?
 */

bool SimpleDataPopup::ShowToolTips()
{
    return true;
}

/*
 * Get bitmap resources
 */

wxBitmap SimpleDataPopup::GetBitmapResource( const wxString& name )
{
    // Bitmap retrieval
////@begin SimpleDataPopup bitmap retrieval
    wxUnusedVar(name);
    return wxNullBitmap;
////@end SimpleDataPopup bitmap retrieval
}

/*
 * Get icon resources
 */

wxIcon SimpleDataPopup::GetIconResource( const wxString& name )
{
    // Icon retrieval
////@begin SimpleDataPopup icon retrieval
    wxUnusedVar(name);
    return wxNullIcon;
////@end SimpleDataPopup icon retrieval
}

// create the table for values
void SimpleDataPopup::CreateTable()
{
	int x = 0, i = 0, y = 0;

	m_MainSizer->AddGrowableCol(0);
	m_MainSizer->AddGrowableCol(1);
	m_MainSizer->AddGrowableCol(2);

	for(x=0; x<m_MainSizer->GetRows(); x++)
	{
		m_MainSizer->AddGrowableRow(x);
		m_lblLabel[x] = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE);
		m_lblLabel[x]->SetFont(m_FontLabel);
		m_lblLabel[x]->SetForegroundColour(m_ColourLabel);
		m_lblLabel[x]->Connect(wxID_ANY, wxEVT_RIGHT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnRightDown), 0, this);
		m_lblLabel[x]->Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(SimpleDataPopup::OnMotion), 0, this);
		m_lblLabel[x]->Connect(wxID_ANY, wxEVT_RIGHT_UP, wxMouseEventHandler(SimpleDataPopup::OnRightUp), 0, this);
		m_lblLabel[x]->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnLeftDown), 0, this);
		m_MainSizer->Add(m_lblLabel[x], 1, wxGROW|wxGROW|wxALL, 3);

		m_lblValue[x] = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE);
		m_lblValue[x]->SetFont(m_FontLabel);
		m_lblValue[x]->SetForegroundColour(m_ColourLabel);
		m_lblValue[x]->Connect(wxID_ANY, wxEVT_RIGHT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnRightDown), 0, this);
		m_lblValue[x]->Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(SimpleDataPopup::OnMotion), 0, this);
		m_lblValue[x]->Connect(wxID_ANY, wxEVT_RIGHT_UP, wxMouseEventHandler(SimpleDataPopup::OnRightUp), 0, this);
		m_lblValue[x]->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnLeftDown), 0, this);
		m_MainSizer->Add(m_lblValue[x], 1, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, 3);


		m_lblDesc[x] = new wxStaticText(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE);
		m_lblDesc[x]->SetFont(m_FontLabel);
		m_lblDesc[x]->SetForegroundColour(m_ColourLabel);
		m_lblDesc[x]->Connect(wxID_ANY, wxEVT_RIGHT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnRightDown), 0, this);
		m_lblDesc[x]->Connect(wxID_ANY, wxEVT_MOTION, wxMouseEventHandler(SimpleDataPopup::OnMotion), 0, this);
		m_lblDesc[x]->Connect(wxID_ANY, wxEVT_RIGHT_UP, wxMouseEventHandler(SimpleDataPopup::OnRightUp), 0, this);
		m_lblDesc[x]->Connect(wxID_ANY, wxEVT_LEFT_DOWN, wxMouseEventHandler(SimpleDataPopup::OnLeftDown), 0, this);
		m_MainSizer->Add(m_lblDesc[x], 1, wxGROW|wxGROW|wxALL, 3);
	}
}

// set the values
void SimpleDataPopup::SetValues(PlayerValueRecord* val)
{
	wxString tmp;
	m_Values = val;
	m_lblPlayername->SetLabel(wxString::Format(_T("%s"), m_Values->name));
	int i = 0;

	// set total hand count
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELHANDS));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%d"), m_Values->handcount));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCHANDS));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set VPIP
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELVPIP));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->vpip));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCVPIP));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set PFR
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELPFR));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->pfr));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCPFR));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set CCPF
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELCCPF));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->ccpf));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCCCPF));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set 3Bet
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABEL3BET));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->pf_3bet));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESC3BET));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set F3Bet
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELF3BET));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->pf_f3bet));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCF3BET));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set ats
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELATS));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->ats));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCATS));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set fold bb 2 steal
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELFBBTS));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->fbbts));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCFBBTS));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set fold sb 2 steal
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELFSBTS));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->fsbts));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCFSBTS));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAF));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->af));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAF));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af flop
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFF));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->flop_af));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFF));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af turn
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFT));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->turn_af));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFT));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af river
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFR));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->river_af));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFR));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af cbet flop
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELCBET));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->flop_cbet));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCCBET));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set af fold 2 cbet flop
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELFCBET));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->flop_fcbet));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCFCBET));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set went to showdown
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELWTSD));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->wtsd));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCWTSD));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set won money at showdown
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELWMSD));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->wmsd));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCWMSD));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set afq overal
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFQ));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->afq));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFQ));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set afq preflop
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFQP));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->pf_afq));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFQP));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set afq flop
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFQF));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->flop_afq));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFQF));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set afq turn
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFQT));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->turn_afq));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFQT));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	// set afq river
	m_lblLabel[i]->SetLabel(wxString::Format(_T("%s"), LABELAFQR));
	m_lblValue[i]->SetLabel(wxString::Format(_T("%.2lf"), m_Values->river_afq));
	m_lblDesc[i]->SetLabel(wxString::Format(_T("%s"), DESCAFQR));
	tmp = m_lblValue[i]->GetLabel();
	tmp.Replace(_T(","), _T("."));
	m_lblValue[i]->SetLabel(tmp);
	i++;

	Fit();
	Layout();
}

// set new parent of window
void SimpleDataPopup::SetNewParent(HWND window)
{
	m_PokerParent = window;
	::SetWindowLongPtr(GetHwnd(),GWLP_HWNDPARENT,(long)m_PokerParent);
	//m_parentchild = ::GetWindow(GetParentHWND(), GW_HWNDFIRST);
	::SetWindowPos(::GetWindow(m_PokerParent, GW_HWNDFIRST), GetHwnd(), 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE);
}


/*
 * wxEVT_PAINT event handler for ID_SIMPLEDATAPOPUP
 */

void SimpleDataPopup::OnPaint( wxPaintEvent& event )
{
////@begin wxEVT_PAINT event handler for ID_SIMPLEDATAPOPUP in SimpleDataPopup.
    // Before editing this code, remove the block markers.
    wxPaintDC dc(this);
////@end wxEVT_PAINT event handler for ID_SIMPLEDATAPOPUP in SimpleDataPopup. 

	int linestart = 0, spacer = 0;

	dc.SetBrush(* wxTRANSPARENT_BRUSH);
	dc.SetPen(wxPen(wxColour(200, 200, 200), 1, wxPENSTYLE_SOLID));
	spacer = GetSize().GetWidth() - 3;

	// draw seperator lines
	linestart = (m_lblLabel[0]->GetPosition().y ) + (m_lblLabel[0]->GetSize().GetHeight() +2);
	dc.DrawLine(3, linestart, spacer, linestart);

	linestart = (m_lblLabel[8]->GetPosition().y ) + (m_lblLabel[8]->GetSize().GetHeight() +2);
	dc.DrawLine(3, linestart, spacer, linestart);

	linestart = (m_lblLabel[12]->GetPosition().y ) + (m_lblLabel[12]->GetSize().GetHeight() +2);
	dc.DrawLine(3, linestart, spacer, linestart);

	linestart = (m_lblLabel[14]->GetPosition().y ) + (m_lblLabel[14]->GetSize().GetHeight() +2);
	dc.DrawLine(3, linestart, spacer, linestart);

	linestart = (m_lblLabel[16]->GetPosition().y ) + (m_lblLabel[16]->GetSize().GetHeight() +2);
	dc.DrawLine(3, linestart, spacer, linestart);
	event.Skip();
}

