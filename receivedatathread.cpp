#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/serialization/string.hpp>
#ifdef DATA_TO_XML
#include <boost/archive/xml_wiarchive.hpp>
#else
#include <boost/archive/text_wiarchive.hpp>
#endif

#include "../pokerreadercommon/helper/tabledata.h"
#include "../pokerreadercommon/helper/huddata.h"
#include "pokerreaderhudapp.h"
#include "../pokerreadercommon/helper/applicationpaths.h"
#include <fstream>
#include <boost/filesystem.hpp>

#include "receivedatathread.h"

ReceiveDataThread::ReceiveDataThread(void)
{
	data_lastwritetime = 1;
	data_currenttime = 1;

	conf_lastwritetime = 1;
	conf_currenttime = 1;
}

ReceiveDataThread::~ReceiveDataThread(void)
{
}

void ReceiveDataThread::ReceiveConfigData()
{
	namespace ipc = boost::interprocess;

	unsigned int priority;  ///< The priority

	ipc::message_queue::size_type recvd_size;

	std::wstring commandtype;	///< The messagetype (Tabledata for HUD or config-data)

	//boost::scoped_ptr<ipc::message_queue> mq;

	//std::wifstream configfile(wxString::Format(_T("%sconfigsetting.prd"), paths.GetUserLocalDataDir()).ToStdWstring().c_str());

	try
	{
		ipc::message_queue mqcommand(ipc::open_only, MQ_HUDCOMMAND);
		commandtype.resize(mqcommand.get_max_msg_size());

		if(mqcommand.try_receive(&commandtype[0], commandtype.size(), recvd_size, priority))
		{
			if(commandtype.compare(L"closehud") >= 0) // Main GUI send e
			{
				exit(0);
			}
			else if(commandtype.find(L"CONFIG:") >= 0)
			{
				ParseAndSetConfigData(commandtype);
			}
		}

		//ipc::message_queue::remove("pokerreader_msg");
	}
	catch (boost::thread_interrupted&)
	{
		//OutputDebugString("message thread closed");
	}
	catch (ipc::interprocess_exception& ipcex)
	{
		//ipc::message_queue::remove("pokerreader_msg");
		//OutputDebugString(ipcex.what());
	}
	catch(boost::archive::archive_exception& ex)
	{
		ex.what();
	}
	catch(boost::thread_exception& tex)
	{
		tex.what();
	}
}

void ReceiveDataThread::ReceiveHudData()
{
	ApplicationPaths paths;
	HudData huddata;

	namespace ipc = boost::interprocess;

	unsigned int priority;  ///< The priority

	ipc::message_queue::size_type recvd_size;

	std::wstring messagetype;

	try
	{
		ipc::message_queue mqdata(ipc::open_only, MQ_HUDDATA);
		messagetype.resize(mqdata.get_max_msg_size());
		
		if(mqdata.try_receive(&messagetype[0], messagetype.size(), recvd_size, priority))
		{
			if(messagetype.compare(L"huddata") >= 0) // data send by GUI-App
			{
				
				if(boost::filesystem::exists(wxString::Format(_T("%shuddata.prd"), paths.GetUserLocalDataDir()).ToStdWstring().c_str()))
				{
					wxLogMessage(L"Restore HudData");
					RestoreTableData(huddata, wxString::Format(_T("%shuddata.prd"), paths.GetUserLocalDataDir()).ToStdWstring().data());
					wxLogMessage(L"Try to set table data");
					wxGetApp().SetHudData(huddata);
				}
			}
		}
	}
	catch (boost::thread_interrupted&)
	{
		//OutputDebugString("message thread closed");
	}
	catch (ipc::interprocess_exception& ipcex)
	{
		//ipc::message_queue::remove("pokerreader_msg");
		//OutputDebugString(ipcex.what());
	}
	catch(boost::archive::archive_exception& ex)
	{
		ex.what();
	}
	catch(boost::thread_exception& tex)
	{
		tex.what();
	}
}

void ReceiveDataThread::RestoreTableData(HudData &s, const wchar_t * filename)
{
	// open the archive
	std::wifstream ifs(filename);
	if (!ifs.good())
	{
		std::cout << "Could not open file to read: " << filename << std::endl;
		return;
	}

	boost::archive::text_wiarchive ia(ifs);

	// restore the schedule from the archive
	ia >> BOOST_SERIALIZATION_NVP(s);

	//std::cout << "Restored, var1=" << s.var1
#if USE_VERSION == 1
	//       << " var2=" << s.var2
#endif
	// << std::endl;
}

// copy data
/*
void ReceiveDataThread::CopyTableData(TableData &dest, TableData *source)
{
	dest.maxseats = source->maxseats;
	dest.playercnt = source->playercnt;
	dest.provider = source->provider;

	dest.tournamentname = source->tournamentname;
	dest.tablename = source->tablename;

	dest.PlayerVal[0] = source->PlayerVal[0];
	dest.PlayerVal[1] = source->PlayerVal[1];
	dest.PlayerVal[2] = source->PlayerVal[2];
	dest.PlayerVal[3] = source->PlayerVal[3];
	dest.PlayerVal[4] = source->PlayerVal[4];
	dest.PlayerVal[5] = source->PlayerVal[5];
	dest.PlayerVal[6] = source->PlayerVal[6];
	dest.PlayerVal[7] = source->PlayerVal[7];
	dest.PlayerVal[8] = source->PlayerVal[8];
	dest.PlayerVal[9] = source->PlayerVal[9];
}*/

void ReceiveDataThread::ParseAndSetConfigData(std::wstring configstring)
{
	int sepcount = 0;
	std::wstring settingname, datatype, strvalue;
	std::wstring rpl = L"CONFIG:";

	//configstring.replace(configstring.begin(), configstring.end(), L"CONFIG:",L"");

	for(int i=0; i < configstring.size(); i++)
	{
		if(configstring.at(i) != L'|')
		{
			if (sepcount < 1)
				settingname.push_back(configstring[i]);
			else if(sepcount == 1)
				datatype.push_back(configstring[i]);
			else if( (configstring.at(i) == L'\n') || ( configstring.size() == (i+1)) )
			{
				sepcount = 0; // reset
				
				// set settings
				wxGetApp().ConfigSetting[settingname].datatype = datatype;
				wxGetApp().ConfigSetting[settingname].settingname = settingname;
				wxGetApp().ConfigSetting[settingname].value = strvalue;

				// and reset ... 
				datatype = L"";
				settingname = L"";
				strvalue = L"";
			}
			else
				strvalue.push_back(configstring[i]);

			if(settingname == L"CONFIG:")
				settingname = L"";

		}
		else
			sepcount++;

	}

	wxGetApp().ReadConfigAndUpdate();
}