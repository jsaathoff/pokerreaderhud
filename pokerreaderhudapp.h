/////////////////////////////////////////////////////////////////////////////
// Name:        pokerreaderhudapp.h
// Purpose:
// Author:
// Modified by:
// Created:     14/05/2009 09:39:46
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _POKERREADERHUDAPP_H_
#define _POKERREADERHUDAPP_H_
#define ID_TIMER 109934
#define SERVER_ID 109935

/*!
* Includes
*/

#if !wxUSE_DEBUGREPORT
    #error "This can't be built without wxUSE_DEBUGREPORT"
#endif // wxUSE_DEBUGREPORT

#if !wxUSE_ON_FATAL_EXCEPTION
    #error "This can't be built without wxUSE_ON_FATAL_EXCEPTION"
#endif // wxUSE_ON_FATAL_EXCEPTION

////@begin includes
#include "wx/image.h"
////@end includes
//#include "mainframe.h"

#include "../PokerReaderCommon/Helper/Common.h"
//#include "../PokerReader_HE/app_resources.h"
//#include "../PokerReaderCommon/Database/DBase.h"
//#include "gui/menuoverlay.h"

#ifdef __WXMSW__
#include "common/winpokersoftwarefinder.h"
#endif

#ifdef _DEBUG
#include <wx/debugrpt.h>
#endif

#include <wx/socket.h>
#include <wx/url.h>
#include <wx/wfstream.h>
#include <wx/process.h>
#include <wx/sysopt.h>
#include "../pokerreadercommon/helper/valuedescription.h"
#include "../pokerreadercommon/helper/playerrecord.h"
#include "../pokerreadercommon/helper/configrecord.h"
#include "../pokerreadercommon/helper/huddata.h"

/*!
* Forward declarations
*/

////@begin forward declarations
////@end forward declarations

namespace boost {
	class thread;
};

class CommunicationClient;
class PokerReaderImageHandler;
class ReceiveDataThread;

/*!
* Control identifiers
*/

////@begin control identifiers
////@end control identifiers

/*!
* PokerReaderHUDApp class declaration
*/

class PokerReaderHUDApp: public wxApp
{
	DECLARE_CLASS( PokerReaderHUDApp )
	DECLARE_EVENT_TABLE()

public:
	/// Constructor
	PokerReaderHUDApp();

	~PokerReaderHUDApp();

	void Init();

	/// Initialises the application
	virtual bool OnInit();

	/// Called on exit
	virtual int OnExit();

	// timer triggered
	void OnTimer(wxTimerEvent& event);

	//checks for another instance of this app
	bool AnotherInstanceIsRunning();

	// setup logging
	void SetupLog();

	// log message
	void Log(const wxString& logmsg);

	// search and overlay the window
	void SearchAndOverlay(wxString table, int provider);

	// sends the config data to the main gui process
	void SendConfigSettingToGui(ConfigRecord setting);

	// request config from main gui
	void RequestConfigFromGui();

	//bool StartClient();

	void StartAdvises();

	void ReadConfigAndUpdate();

	void SetConfigSetting(ConfigRecord * configData);

	PokerReaderImageHandler* GetImages();

	ConfigRecord BuildConfigDataForHUD(std::wstring settingname, bool value);

	ConfigRecord BuildConfigDataForHUD(std::wstring settingname, std::wstring value);

	ConfigRecord BuildConfigDataForHUD(std::wstring settingname, double value);

	ConfigRecord BuildConfigDataForHUD(std::wstring settingname, int value);

	void SetHudData(HudData huddata);

	void LoadLanguage();

	void ParseAndSetConfig(std::string input);

	int m_OverlayOutsize;

	// the main window MOD 22.08.09 JS: added
	//MainFrame* m_MainWindow;

	////@begin PokerReaderHUDApp event handler declarations

	////@end PokerReaderHUDApp event handler declarations

	////@begin PokerReaderHUDApp member function declarations

	////@end PokerReaderHUDApp member function declarations

	////@begin PokerReaderHUDApp member variables
	////@end PokerReaderHUDApp member variables

	// timer object
	wxTimer m_timer; //

	//ValueDescription m_ValDesc;

	//OverlayMenu* m_Menu;

	// the instance-checker object
	wxSingleInstanceChecker *m_InstanceChecker;

#ifdef __WXMSW__
	CWinPokerSoftwareFinder Finder;
#endif

	// file to write log
	FILE * m_logFile;

	// log object
	wxLog* m_StandardLog;

	// loggging enabled
	bool m_bEnableLogging;

	//CommunicationClient * CLIENT;

	std::map<std::wstring, HudData> OverlayData;

	PokerReaderImageHandler * Images;

	bool m_bExtendedHud;

	ConfigRecord configData;

	std::map<std::wstring, ConfigRecord> ConfigSetting;

	boost::thread * receivedatathread;
	boost::thread * receiveconfigthread;

	wxLocale * m_Locale;

	ReceiveDataThread * m_receivedata;

private:

	// called when a crash occurs in this application
	virtual void OnFatalException();

	virtual void OnUnhandledException();

#ifdef _DEBUG
	// this is where we really generate the debug report
	void GenerateReport(wxDebugReport::Context ctx);
#endif
};

/*!
* Application instance declaration
*/

////@begin declare app
DECLARE_APP(PokerReaderHUDApp)
////@end declare app

#endif
// _POKERREADERHUDAPP_H_