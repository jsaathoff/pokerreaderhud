/////////////////////////////////////////////////////////////////////////////
// Name:        client.cpp
// Purpose:     DDE sample: client
// Author:      Julian Smart
// Modified by:    Jurgen Doornik
// Created:     25/01/99
// RCS-ID:      $Id$
// Copyright:   (c) Julian Smart
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

// ============================================================================
// declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

// Settings common to both executables: determines whether
// we're using TCP/IP or real DDE.
#include "../../pokerreadercommon/serverclient/ipcsetup.h"

#include "wx/datetime.h"
#include "client.h"
#include "../pokerreaderhudapp.h"

#if !defined(__WXMSW__) && !defined(__WXPM__)
    #include "../sample.xpm"
#endif

// ----------------------------------------------------------------------------
// wxWin macros
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// MyClient
// ----------------------------------------------------------------------------
CommunicationClient::CommunicationClient() : wxClient()
{
    m_connection = NULL;
}

bool CommunicationClient::Connect(const wxString& sHost, const wxString& sService, const wxString& sTopic)
{
    // suppress the log messages from MakeConnection()
    wxLogNull nolog;

    m_connection = (MyConnection *)MakeConnection(sHost, sService, sTopic);
    return m_connection    != NULL;
}

wxConnectionBase *CommunicationClient::OnMakeConnection()
{
    return new MyConnection;
}

void CommunicationClient::Disconnect()
{
    if (m_connection)
    {
        m_connection->Disconnect();
        wxDELETE(m_connection);
        wxLogMessage(wxT("Client disconnected from server"));
    }
}

CommunicationClient::~CommunicationClient()
{
    Disconnect();
}

// ----------------------------------------------------------------------------
// MyConnection
// ----------------------------------------------------------------------------

bool MyConnection::OnAdvise(const wxString& topic, const wxString& item, const void *data, size_t size, wxIPCFormat format)
{
    Log(wxT("OnAdvise"), topic, item, data, size, format);
	wxLogMessage(wxT("Receiving data"));

	try
	{
		if( (topic == IPC_TOPIC) && (item == IPC_ITEM_DATA) )
		{
			TableData * structual = (TableData*)data;
			tmpval = wxString::wxString(structual->tablename);
			wxLogMessage(wxT("New Data for table: ")+tmpval);
			CopyTableData(wxGetApp().OverlayData[tmpval], structual);
			wxGetApp().SearchAndOverlay(tmpval, structual->provider);
		}
		else if( (topic == IPC_TOPIC) && (item == IPC_ITEM_CONFIG) )
		{
			ConfigRecord * configData = (ConfigRecord*)data;
			wxLogMessage(wxT("Read Config..."));
			wxGetApp().SetConfigSetting(configData);
		}
		else if( (topic == IPC_TOPIC) && (item == IPC_ITEM_CLOSE))
		{
			wxLogMessage(wxT("Close HUD..."));
			wxExit();
		}
		return true;
	}
	catch (...)
	{
		return false;
	}
}

bool MyConnection::OnDisconnect()
{
    wxLogMessage(wxT("OnDisconnect()"));
    Disconnect();
    return true;
}

bool MyConnection::DoExecute(const void *data, size_t size, wxIPCFormat format)
{
    Log(wxT("Execute"), wxEmptyString, wxEmptyString, data, size, format);
    bool retval = wxConnection::DoExecute(data, size, format);
    if (!retval)
    {
        wxLogMessage(wxT("Execute failed!"));
    }
    return retval;
}

const void *MyConnection::Request(const wxString& item, size_t *size, wxIPCFormat format)
{
    const void *data =  wxConnection::Request(item, size, format);
    Log(wxT("Request"), wxEmptyString, item, data, size ? *size : wxNO_LEN, format);
	wxLogMessage(wxT("Requesting data"));
    return data;
}

bool MyConnection::DoPoke(const wxString& item, const void *data, size_t size, wxIPCFormat format)
{
    Log(wxT("Poke"), wxEmptyString, item, data, size, format);
	wxLogMessage(wxT("Poke server"));
    return wxConnection::DoPoke(item, data, size, format);
}

// copy data
void MyConnection::CopyTableData(TableData &dest, TableData *source)
{
	dest.maxseats = source->maxseats;
	dest.playercnt = source->playercnt;
	dest.provider = source->provider;

	wcscpy(dest.tourneyname, source->tourneyname);
	wcscpy(dest.tablename, source->tablename);

	dest.PlayerVal[0] = source->PlayerVal[0];
	dest.PlayerVal[1] = source->PlayerVal[1];
	dest.PlayerVal[2] = source->PlayerVal[2];
	dest.PlayerVal[3] = source->PlayerVal[3];
	dest.PlayerVal[4] = source->PlayerVal[4];
	dest.PlayerVal[5] = source->PlayerVal[5];
	dest.PlayerVal[6] = source->PlayerVal[6];
	dest.PlayerVal[7] = source->PlayerVal[7];
	dest.PlayerVal[8] = source->PlayerVal[8];
	dest.PlayerVal[9] = source->PlayerVal[9];
}