#include "../../PokerReaderHUD/pokerreaderhudapp.h"
#include "WinPokerSoftwareFinder.h"

CWinPokerSoftwareFinder::CWinPokerSoftwareFinder(void)
{
	m_bPokerReaderMainAppFound = true;
	PokerModules[_T("pokerstars.exe")] = 1;
	PokerModules[_T("fulltiltpoker.exe")] = 2;
	PokerModules[_T("partygaming.exe")] = 3;
	PokerModules[_T("pokerapp.exe")] = 4;

	PokerProcessNames[1] = _T("pokerstars.exe");
	PokerProcessNames[2] = _T("fulltiltpoker.exe");
	PokerProcessNames[3] = _T("partygaming.exe");
	PokerProcessNames[4] = _T("pokerapp.exe");
}

CWinPokerSoftwareFinder::~CWinPokerSoftwareFinder(void)
{
	Containers.Empty();
	OverlayContainer = NULL;
	delete OverlayContainer;
}

/*
// scan for poker processes
void CWinPokerSoftwareFinder::ScanPokerProcesses(void)
{
	int count=0, provider=0;
	BOOL bFirstModule = TRUE;
	CProcessIterator itp;
	wxString modulename, windowname, windhwnd;

	for (DWORD pid=itp.First(); pid; pid=itp.Next())
	{
		TCHAR modname[_MAX_PATH]; // the exe filename
		CProcessModuleIterator itm(pid);
		HMODULE hModule = itm.First(); // .EXE
		if(hModule)
		{
			// retrieve the module name + .exe
			GetModuleBaseName(itm.GetProcessHandle(), hModule, modname, _MAX_PATH);
			modulename = wxString(modname); // to wxString

			// search exe-modul in our map
			PokerModules::iterator it = PokerModules.find(modulename.Lower()); // module is a poker module?
			if(it != PokerModules.end()) // if results not at end there must be a match
			{
				BOOL bFirstWindow = TRUE;
				CMainWindowIterator itw(pid);
				for (HWND hwnd = itw.First(); hwnd; hwnd=itw.Next())  // get window handles
				{
					TCHAR charbuffer[MAX_PATH];
					::GetWindowText(hwnd, charbuffer, MAX_PATH);
					windowname = wxString(charbuffer);
					windhwnd = wxString::Format(_T("%d"), (int)hwnd);
					if(IsPlayTable(hwnd)) // is it a playtable window?
					{
						windhwnd = wxString::Format(_T("%d"), (int)hwnd); // hwnd in a string to avoid dupes (hwnd as index)
						if(PokerWindows.Index(windhwnd) == wxNOT_FOUND) // no dupe found
						{
							PokerWindows.Add(windhwnd); // add this window
							CreateOverlay(windowname, it->second); // create an overlay and pos it
						}
					}
				}
			}
		}
	}
	CleanUpWindows();
}
*/
void CWinPokerSoftwareFinder::SearchAndOverlay(wxString tablename, int provider)
{
	int count=0;
	BOOL bFirstModule = TRUE;
	CProcessIterator itp;
	wxString modulename, windowname, windhwnd;

	for (DWORD pid=itp.First(); pid; pid=itp.Next())
	{
		TCHAR modname[_MAX_PATH]; // the exe filename
		CProcessModuleIterator itm(pid);
		HMODULE hModule = itm.First(); // .EXE
		if(hModule)
		{
			// retrieve the module name + .exe
			GetModuleBaseName(itm.GetProcessHandle(), hModule, modname, _MAX_PATH);
			modulename = wxString(modname).Lower(); // to wxString

			// search exe-modul in our map
			//PokerModules::iterator it = PokerModules.find(modulename.Lower()); // module is a poker module?
			if(modulename == PokerProcessNames[provider]) // module name is the name of the poker-app?
			{
				BOOL bFirstWindow = TRUE;
				CMainWindowIterator itw(pid);
				for (HWND hwnd = itw.First(); hwnd; hwnd=itw.Next())  // get window handles
				{
					TCHAR charbuffer[MAX_PATH];
					::GetWindowText(hwnd, charbuffer, MAX_PATH);
					windowname = wxString(charbuffer);
					windhwnd = wxString::Format(_T("%d"), (int)hwnd);
					if( IsPlayTable(hwnd, windowname, tablename)) // is it a playtable window?
					{
						windhwnd = wxString::Format(_T("%d"), (int)hwnd); // hwnd in a string to avoid dupes (hwnd as index)

						// does it exist
						if( (PokerWindows.Index(windhwnd) == wxNOT_FOUND) && ((TableContainer[tablename] <= 0) || (TableContainer[tablename] == NULL)) && (windowname.Find(tablename) > wxNOT_FOUND))
						{
							PokerWindows.Add(windhwnd); // add this window
							CreateOverlay(windowname, provider, tablename); // create an overlay and pos it
							UpdateOverlayData(windowname, provider, tablename);
						}
						else
						{
							if(windowname.Find(tablename) > wxNOT_FOUND)
								UpdateOverlayData(windowname, provider, tablename);
						}
					}
				}
			}
		}
	}
	CleanUpWindows();
}

// checks if the window is a playable table window type
bool CWinPokerSoftwareFinder::IsPlayTable(wxString* winname)
{
	if(reg.Compile(wxT("^.+/.+(limit)(.)+"), wxRE_ADVANCED | wxRE_ICASE))
	{
		if(reg.Matches(winname->GetData()))
		{
			return true;
		}
	}
	if(reg.Compile(wxT("^(tournament).+(table)"), wxRE_ADVANCED | wxRE_ICASE))
	{
		if(reg.Matches(winname->GetData()))
		{
			return true;
		}
	}
	if(reg.Compile(wxT("^play.+/.+"), wxRE_ADVANCED | wxRE_ICASE))
	{
		if(reg.Matches(winname->GetData()))
		{
			return true;
		}
	}
	return false;
}

// checks if the window is a playable table window type
bool CWinPokerSoftwareFinder::IsPlayTable(HWND handle, wxString windowname, wxString tablename)
{
	TCHAR winclass[255];
	::GetClassName(handle, winclass, 255);
	wxString tableclass(winclass);
	bool result = false;
	if( tableclass == _T("PokerStarsTableFrameClass") ) // pokerstars
	{
		result = true;
	}
	if(tableclass == _T("QWidget") && (windowname.Find(tablename) > wxNOT_FOUND) ) // fulltiltpoker
	{
		result = true;
	}
	return result;
}

// creates the overlay for the menu
void CWinPokerSoftwareFinder::CreateOverlay(wxString windowname, int provider, wxString tablename)
{
	// MOD 22.08.09 new: added main app-window
	wxString winname=wxString::Format(_T("%s-Overlay"), windowname);
	wxLogMessage(wxT("Creating overlay: ") + winname);

#ifdef __WXMSW__
	OverlayContainer = new PokerReaderOverlay(NULL, wxID_ANY, winname);
	OverlayContainer->setProvider(provider);
	OverlayContainer->setPokerParent(GetHWNDFromWindowName(windowname));
	OverlayContainer->setPokerParentWindowname(windowname);
	OverlayContainer->setHudData(wxGetApp().OverlayData[tablename.ToStdWstring()]);
	OverlayContainer->setTableMax(OverlayContainer->getHudData().TableData.maxseats);
	OverlayContainer->FindAndOverlay();
	OverlayContainer->getProviderConfig()->SetTableMax(OverlayContainer->getTableMax());
	OverlayContainer->getProviderConfig()->SetProvider(provider);
	OverlayContainer->getProviderConfig()->LoadFile();
	OverlayContainer->getProviderConfig()->ReadPrefSeat(provider, OverlayContainer->getTableMax(), (!OverlayContainer->getHudData().TableData.tournamentname.empty()) );
	OverlayContainer->setVisualHeroSeat(OverlayContainer->getProviderConfig()->GetPrefSeatNumber());
	OverlayContainer->InitMenuCoordinates(true);
	OverlayContainer->InitSeatCoordinates(true);
	OverlayContainer->ReadConfig();
	OverlayContainer->UpdatePlayerOverlayData();
	OverlayContainer->wxMSWWindowPositioning();
#endif
	Containers.Add(OverlayContainer);
	TableContainer[tablename] = OverlayContainer;
	wxLogMessage(wxT("Show overlay: ") + windowname);
}

// updates the overlay for the menu
void CWinPokerSoftwareFinder::UpdateOverlayData(wxString windowname, int provider, wxString tablename)
{

	wxLogMessage(wxT("Updating overlay-data for table: ") + tablename);
	PokerReaderOverlay* overlay = TableContainer[tablename];
	overlay->setHudData(wxGetApp().OverlayData[tablename.ToStdWstring()]);
	wxLogMessage(wxT("Overlay-data updated: ") + tablename);
	wxLogMessage(wxT("Windowupdate: ") + windowname);
	//overlay->PrepareToShow();
	overlay->UpdatePlayerOverlayData();
	overlay->Show(true);
}

#ifdef __WXMSW__
// set the window order (poker window and overlay)
void CWinPokerSoftwareFinder::SetWindowOrder(HWND parent, HWND child)
{
	::SetParent(child, parent);
}
#endif

#ifdef __WXMSW__
// get the handle by window-name
HWND CWinPokerSoftwareFinder::GetHWNDFromWindowName(wxString value)
{
	return ::FindWindow(NULL, value.GetData());
}
#endif

// clean the mess, search for non-existent parents and destroy overlays
void CWinPokerSoftwareFinder::CleanUpWindows(bool appclose)
{
	unsigned int x;
	wxString tmp;

#ifdef __WXMSW__
	wxString sHwnd;
	// wxLogMessage(wxT("Cleaning up"));
	if(Containers.GetCount() > 0)
	{
		for( x = 0; x <= (Containers.GetCount() - 1); x++)
		{
			if(!::IsWindow(Containers.Item(x)->getPokerParent()))// get hwnd
			{
				sHwnd = wxString::Format(_T("%d"), (int)Containers.Item(x)->getPokerParent());
				if(PokerWindows.Index(sHwnd) == wxNOT_FOUND)
				{
					//Containers.Item(x)->Close(true);
					//delete Containers.Item(x); // and for sure destroy
					Containers.Item(x)->Hide();
					Containers.Item(x)->Disable();
					Containers.Item(x)->Freeze();
					//Containers.Item(x)->Destroy(); // MOD 22.08.09 JS: - Destroy window
					Containers.RemoveAt(x);
					break;
				}
				else
				{
					PokerWindows.Remove(sHwnd); // remove the unused window by hwnd as text
					//delete Containers.Item(x); // and for sure destroy
					Containers.Item(x)->Hide();
					Containers.Item(x)->Disable();
					Containers.Item(x)->Freeze();
					Containers.Item(x)->Destroy(); // MOD 22.08.09 JS: - Destroy window
					Containers.RemoveAt(x);
					break;
				}
			}
		}
	}
#endif
}

// scan for pokerreader_he main app and return
bool CWinPokerSoftwareFinder::PokerReaderAppFound()
{
	return m_bPokerReaderMainAppFound;
}

// search for pokerreader process
void CWinPokerSoftwareFinder::SearchForPokerReader()
{
	int count=0, provider=0;
	BOOL bFirstModule = TRUE;
	CProcessIterator itp;
	wxString modulename, windowname, windhwnd;

	for (DWORD pid=itp.First(); pid; pid=itp.Next())
	{
		TCHAR modname[_MAX_PATH]; // the exe filename
		CProcessModuleIterator itm(pid);
		HMODULE hModule = itm.First(); // .EXE
		if(hModule)
		{
			// retrieve the module name + .exe
			GetModuleBaseName(itm.GetProcessHandle(), hModule, modname, _MAX_PATH);
			modulename = wxString(modname); // to wxString

			if(modulename.Lower().Find(_T("pokerreader_he.exe")) > wxNOT_FOUND)
			{
				m_bPokerReaderMainAppFound = true;
				return;
			}
			else
			{
				m_bPokerReaderMainAppFound = false;
			}
		}
	}
}