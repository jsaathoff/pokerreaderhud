/////////////////////////////////////////////////////////////////////////////
// Name:        client.h
// Purpose:     DDE sample: client
// Author:      Julian Smart
// Modified by:
// Created:     25/01/99
// RCS-ID:      $Id$
// Copyright:   (c) Julian Smart
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#include "connection.h"
#include "../../../ibpp/src/ibpp/ibpp.h"
#include "../../pokerreadercommon/serverclient/tabledata.h"
#include "../../pokerreadercommon/serverclient/configrecord.h"

#define ID_START        10000
#define ID_DISCONNECT   10001
#define ID_STARTADVISE  10002
#define ID_LOG          10003
#define ID_SERVERNAME   10004
#define ID_STOPADVISE   10005
#define ID_POKE         10006
#define ID_REQUEST      10007
#define ID_EXECUTE      10008
#define ID_TOPIC		10009
#define ID_HOSTNAME     10010



// Define a new application
class MyConnection : public MyConnectionBase
{
public:
    virtual bool DoExecute(const void *data, size_t size, wxIPCFormat format);
    virtual const void *Request(const wxString& item, size_t *size = NULL, wxIPCFormat format = wxIPC_TEXT);
    virtual bool DoPoke(const wxString& item, const void* data, size_t size, wxIPCFormat format);
    virtual bool OnAdvise(const wxString& topic, const wxString& item, const void *data, size_t size, wxIPCFormat format);
    virtual bool OnDisconnect();
	// copy data
	void CopyTableData(TableData &dest, TableData *source);

	wxString tmpval;
};

class CommunicationClient: public wxClient
{
public:
    CommunicationClient();
    ~CommunicationClient();
    bool Connect(const wxString& sHost, const wxString& sService, const wxString& sTopic);
    void Disconnect();
    wxConnectionBase *OnMakeConnection();
    bool IsConnected() { return m_connection != NULL; };
    MyConnection *GetConnection() { return m_connection; };

protected:
    MyConnection     *m_connection;
};
