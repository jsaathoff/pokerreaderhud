#pragma once


#ifndef CWINPOKERSOFTWAREFINDER_H
#define CWINPOKERSOFTWAREFINDER_H

#include <wx/dynarray.h>
#include <wx/arrimpl.cpp>
#include "../../pokerreadercommon/helper/winprocesshelp.h"
#include "../../pokerreaderhud/gui/pokerreaderoverlay.h"
//#include "../gui/overlaymenu.h"
#include <wx/regex.h>


class CWinPokerSoftwareFinder
{
public:
	
	CWinPokerSoftwareFinder(void);
	~CWinPokerSoftwareFinder(void);
	
	WX_DECLARE_STRING_HASH_MAP(int, PokerProvider);
	//WX_DECLARE_STRING_HASH_MAP(HWND, PokerWindow);
	WX_DECLARE_STRING_HASH_MAP(int, PokerModules);
	WX_DEFINE_ARRAY_PTR(PokerReaderOverlay*, Container);

	// process names of poker-applications
	std::map<int, wxString> PokerProcessNames; 

	// container for overlays
	std::map<wxString, PokerReaderOverlay*> TableContainer;
	
	// the modules (EXE)
	PokerModules PokerModules;

	wxArrayString PokerWindows;
	
	// the reg exer
	wxRegEx reg;
	
	// fill 
	bool m_bPokerReaderMainAppFound;
	
	// scan for poker processes
	void ScanPokerProcesses(void);

	// search a window and overlay it
	void SearchAndOverlay(wxString tablename, int provider);

	// scan for pokerreader_he main app and return
	bool PokerReaderAppFound();

	// search for pokerreader process
	void SearchForPokerReader();

	// checks if the window is a playable table window type
	bool IsPlayTable(wxString* winname);

	// checks if the window is a playable table window type
	bool IsPlayTable(HWND handle, wxString windowname, wxString tablename);
	
	// creates the overlay for the menu
	void CreateOverlay(wxString windowname, int provider, wxString tablename);

	// updates the overlay for the menu
	void UpdateOverlayData(wxString windowname, int provider, wxString tablename);

	// Array for overlay-windows
	Container Containers;

	// an overlay container
	PokerReaderOverlay* OverlayContainer;

#ifdef __WXMSW__
	// set the window order (poker window and overlay)
	void SetWindowOrder(HWND parent, HWND child);
#endif

#ifdef __WXMSW__
	// get the handle by window-name
	HWND GetHWNDFromWindowName(wxString value);
#endif
	
	// clean the mess, search for non-existent parents and destroy overlays
	void CleanUpWindows(bool appclose=false);
};

#endif //CWINPOKERSOFTWAREFINDER_H