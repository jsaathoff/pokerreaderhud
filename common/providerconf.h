#pragma once


#ifndef CPROVIDERCONF_H
#define CPROVIDERCONF_H


//#include <wx/stdpaths.h>
#include <wx/config.h>
#include <wx/confbase.h>
#include <wx/fileconf.h>
#include <wx/regex.h>
#include "../../pokerreadercommon/helper/definitions.h"
#include "../../pokerreadercommon/helper/providerpathutilities.h"


class ProviderConf
{
public:
	ProviderConf(void);
	~ProviderConf(void);

	// sets the poker provider
	void SetProvider(int value);

	// read the tables standard size
	void ReadSize();
	
	// get the poker provider
	int GetProvider();

	// sets the maximum on table
	void SetTableMax(int value);
	
	// get the maximum on table
	int GetTableMax();
	
	// loads the config file
	bool LoadFile();

	// read coords from file
	void ReadSeatCoords(bool fromfile=false);

	// read menucoords from file
	void ReadMenuCoords(bool fromfile=false);

	// set the menu coords
	void SetMenuCoord(double x, double y);

	// get da coordinates for y
	double GetSeatCoordY(int seatnumber);

	// get da coordinates for x
	double GetSeatCoordX(int seatnumber);
	
	// save layout to file
	void SaveLayout();

	// set da coordinates for y
	void SetSeatCoordY(int seatnumber, double ycoord);
	
	// set da coordinates for x
	void SetSeatCoordX(int seatnumber, double xcoord);

	// returns the x-coord for the menu
	double GetMenuX(){
		return nMenuX;
	}
	
	// returns the y-coord for the menu
	double GetMenuY(){
		return nMenuY;
	}
	
	// sets the x-coord for the menu
	void SetMenuX(double value){
		nMenuX = value;
	}
	
	// sets the y-coord for the menu
	void SetMenuY(double value){
		nMenuX = value;
	}

	int GetWidth()
	{
		return width;
	}

	int GetHeight()
	{
		return height;
	}

	// read preferred seat for hero
	void ReadPrefSeat(int provider, int tablemax, bool istournament = false);

	// read pokerstars pref seat
	void ReadPokerStarsPrefSeat(wxString file, int tablemax, bool istournament);

	// read fulltilt pref seat
	//void ReadFullTiltPrefSeat(wxString file);

	// get install directory of pokerstars
	wxString GetPokerStarsInstallDirectory();

	// get path to pokerstars handhistory
	wxString GetPokerStarsHandHistory();

	// get path to import directory
	wxString GetPokerStarsManualImportDirectory();

	// set path to import directory
	bool SetPokerStarsManualImportDirectory(wxString importpath);

	// get app data path
	//wxString GetPokerStarsAppData();

	// get install directory of fulltilt poker
	//wxString GetFullTiltInstallDirectory();

	// get the number of pref seat
	int GetPrefSeatNumber();

	// read fulltilt layout
	//void ReadFullTiltRaceTrackLayout(wxString file);

	// set the numer of pref seat
	void SetPrefSeatNumber(int prefseat)
	{
		m_nPrefSeat = prefseat;
	};

private:

	// get the ini file of PokerStars
	wxString GetPokerStarsFile();

	// get the ini file of Full Tilt
	//wxString ProviderConf::GetFullTiltFile();

	// the provider identifier
	int nProvider;

	// the reg exer
	wxRegEx regEx;

	// the max amount of seats at the table
	int nTableMax;

	// config-object
	wxFileConfig* Config;

	// x-Coord
	double nSeatX[11];

	// y-Coord
	double nSeatY[11];

	// x-Coord of menu
	double nMenuX;

	// y-Coord of menu
	double nMenuY;

	int m_nPrefSeat;
	
	int width;

	int height;

	int m_nStandardTableLayout;

	ProviderPathUtilities Paths;
};

#endif //CPROVIDERCONF_H