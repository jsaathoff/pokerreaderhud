#include "providerconf.h"
#include "../../pokerreadercommon/helper/myregex.h"
#include <wx/xml/xml.h>
#include <wx/ffile.h>
#include <wx/dir.h>
#include "../pokerreaderhudapp.h"


ProviderConf::ProviderConf(void)
{
	Config = NULL;
	nTableMax=0;
	m_nStandardTableLayout = 1;
	m_nPrefSeat = -1;
}

ProviderConf::~ProviderConf(void)
{
	Config = NULL;
	delete Config;
}

// sets the provider
void ProviderConf::SetProvider(int value)
{
	nProvider = value;
}

// get the provider
int ProviderConf::GetProvider()
{
	return this->nProvider;
}

// sets the maximum on table
void ProviderConf::SetTableMax(int value)
{
	nTableMax = value;
}

// get the maximum on table
int ProviderConf::GetTableMax()
{
	return this->nTableMax;
}

// loads the config file
bool ProviderConf::LoadFile()
{
	bool result = false;

	switch(GetProvider())
	{
	case 1: // pokerstars
		Config = new wxFileConfig(wxEmptyString, wxEmptyString, GetPokerStarsFile(), wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
		Config->GetNumberOfEntries(true) > 0 ? result = true : result = false;
		return result;
		break;
		/*case 2: // full tilt
		Config = new wxFileConfig(wxEmptyString, wxEmptyString, GetFullTiltFile(), wxEmptyString, wxCONFIG_USE_LOCAL_FILE, wxConvAuto());
		Config->GetNumberOfEntries(true) > 0 ? result = true : result = false;
		return result;
		break;*/
	default:
		return false;
		break;
	}
}

// get the ini file of PokerStars
wxString ProviderConf::GetPokerStarsFile()
{
	// initialize a file for the user (user-specified settings)
	wxString UserIniFile = wxString::Format(_T("%spokerstars.ini"), Paths.GetUserHudLayoutDir()); // user config...append username

	//wxString MainIniFile = wxString::Format(_T("%spokerstars.ini"),wxGetApp().m_Common.GetAppConfPath()); // standard config

	return UserIniFile;
}

/*
// get the ini file of Full Tilt
wxString ProviderConf::GetFullTiltFile()
{
// initialize a file for the user (user-specified settings)
// read fulltilt layout

// for racetracklayout
ReadPrefSeat(Definitions::Providers::FULLTILT);

wxString UserIniFile;
wxString MainIniFile;

if(m_nStandardTableLayout == 0) // no racetraCK
{
UserIniFile = wxString::Format(_T("%s/fulltilt.ini"), Paths.GetUserHudLayoutDir()); // user config...append username
}
else if(m_nStandardTableLayout == 1) // racetrack
{
UserIniFile = wxString::Format(_T("%s/fulltilt_racetrack.ini"), Paths.GetUserHudLayoutDir()); // user config...append username
}
return UserIniFile;
}
*/

// read the tables standard size
void ProviderConf::ReadSize()
{
	wxString path, key;
	int x = 0;
	long nwidth, nheight;

	path = wxString::Format(_T("/size"));

	if( Config->HasGroup(path))
	{
		Config->SetPath(path); // set the path
		key = wxString::Format(_T("width")); // format the entry
		Config->Get();
		Config->Read(key,&nwidth); // read the width from file

		key = wxString::Format(_T("height")); // format the entry
		Config->Get();
		Config->Read(key,&nheight); // read the width from file
	}

	width = (int) nwidth;
	height = (int) nheight;
}

// read coords from file
void ProviderConf::ReadSeatCoords(bool fromfile)
{
	wxString path, key, value;
	int x = 0;

	path = wxString::Format(_T("/%dmax"),GetTableMax());
	// xxxx path = wxString::Format(_T("/%dmax"),6);



	LoadFile();
	if( Config->HasGroup(path))
	{
		Config->SetPath(path); // set the path
		while(x < GetTableMax())
		{
			key = wxString::Format(_T("Seat%d"),(x+1)); // format the entry
			Config->Get();
			if(Config->Read(key,&value)) // read the value from file
			{
				value.Trim(); // trim the value
				if(regEx.Compile(_T("^([\\d.]+)\\|([\\d.]+)"),wxRE_ADVANCED | wxRE_ICASE))
				{
					if(regEx.Matches(value))
					{
						regEx.GetMatch(value,1).ToCDouble(&nSeatX[x]);
						regEx.GetMatch(value,2).ToCDouble(&nSeatY[x]);
					}
					else
					{
						nSeatX[x] = 0;
						nSeatY[x] = 0;
					}
				}
				else
				{
					nSeatX[x] = 0;
					nSeatY[x] = 0;
				}
			}
			else
			{
				nSeatX[x] = 0;
				nSeatY[x] = 0;
			}
			//nSeatY[x] += (wxGetApp().m_OverlayOutsize / 2);
			//nSeatX[x] += (wxGetApp().m_OverlayOutsize / 2); 
			x++;
		}
	}
}

// read menucoords from file
void ProviderConf::ReadMenuCoords(bool fromfile)
{
	wxString path, key;
	double value;

	path = wxString::Format(_T("/menucoords"));
	if( Config->HasGroup(path))
	{
		Config->SetPath(path); // set the path
		Config->Get(); // get the config values
		if(Config->Read(_T("x"),&value)) // read the value from file
		{
			nMenuX = value;
			if(Config->Read(_T("y"),&value))
				nMenuY = value;
			else
				nMenuY = 0; // set to standard
		}
		else
		{
			nMenuX = 0; // set to standard
		}
	}
	else
	{
		nMenuX = 0;
		nMenuY = 0;
	}
	//nMenuY += (wxGetApp().m_OverlayOutsize / 2);
	//nMenuX += (wxGetApp().m_OverlayOutsize / 2);
}

// set the menu coords
void ProviderConf::SetMenuCoord(double x, double y)
{
	nMenuY = y;
	nMenuX = x;
}

// get da coordinates for y
double ProviderConf::GetSeatCoordY(int seatnumber)
{
	double ycoord;
	ycoord = nSeatY[seatnumber-1];
	return ycoord;
}

// get da coordinates for x
double ProviderConf::GetSeatCoordX(int seatnumber)
{
	double xcoord;
	xcoord = nSeatX[seatnumber-1];
	return xcoord;
}

// set da coordinates for y
void ProviderConf::SetSeatCoordY(int seatnumber, double ycoord)
{
	nSeatY[seatnumber-1] = (double)ycoord;
}

// set da coordinates for x
void ProviderConf::SetSeatCoordX(int seatnumber, double xcoord)
{
	nSeatX[seatnumber-1] = (double)xcoord;
}

// save layout to file
void ProviderConf::SaveLayout()
{
	wxString path, key, wrtvalue;
	int x = 0;
	double xm, ym, yc, xc;

	path = wxString::Format(_T("/%dmax"),GetTableMax());

	if( Config->HasGroup(path))
	{
		Config->SetPath(path); // set the path
		while(x < GetTableMax())
		{
			key = wxString::Format(_T("Seat%d"),(x+1)); // format the entry

			xc = nSeatX[x];
			//xc = (xc - (wxGetApp().m_OverlayOutsize / 2));

			yc = nSeatY[x];
			//yc = (yc - (wxGetApp().m_OverlayOutsize / 2));


			wrtvalue = wxString::Format(_T("%s|%s"), wxString::FromCDouble(xc), wxString::FromCDouble(yc) );
			Config->Write(key, wrtvalue);
			x++;
		}
	}

	path = wxString::Format(_T("/menucoords"));
	if( Config->HasGroup(path))
	{
		Config->SetPath(path); // set the path
		xm = nMenuX;
		//xm  = (xm - (wxGetApp().m_OverlayOutsize / 2));

		ym = nMenuY;
		//ym = (ym - (wxGetApp().m_OverlayOutsize / 2));

		Config->Write(_T("x"), xm ); 
		Config->Write(_T("y"), ym );
	}
	Config->Flush();
}

// init and read preferred seat for hero
void ProviderConf::ReadPrefSeat(int provider, int tablemax, bool istournament)
{
	wxString fullpath;
	//TextFileFinder* finder = new TextFileFinder();

	if(provider == Definitions::Providers::POKERSTARS) // pokerstars ???!!!!!
	{
		fullpath = wxString(wxGetApp().ConfigSetting[wxString::Format(_T("%s%s"), ProviderToString(Definitions::Providers::POKERSTARS), CONF_PROVIDERINIFILE1).ToStdWstring()].value);
		ReadPokerStarsPrefSeat(fullpath, tablemax, istournament); // read pref seat from ini file		
	}
	/*if(provider == Definitions::Providers::FULLTILT) // Full Tilt ???!!!!!
	{
	if(::wxDirExists(Paths.GetProviderConfigDir(provider)))
	{
	fullpath = Paths.GetProviderConfigDir(provider);  // app data path for user (vista, win7)
	}
	else if(::wxDirExists(Paths.GetProviderProgramDir(provider)))
	{
	fullpath = Paths.GetProviderProgramDir(provider); // main program path (xp)
	}
	else if(::wxDirExists(GetFullTiltInstallDirectory()))
	{
	fullpath = GetPokerStarsInstallDirectory();
	}
	else
	{
	m_nPrefSeat = -1;
	}

	fullpath.Append(_T("user.prefs")); // the user ini contains the pref seat
	//ReadFullTiltPrefSeat(fullpath); // read pref seat from ini file	
	//ReadFullTiltRaceTrackLayout(fullpath);
	}*/
}



// read pokerstars pref seat
void ProviderConf::ReadPokerStarsPrefSeat(wxString file, int tablemax, bool istournament)
{
	MyRegEx reg;
	MyRegEx reg1;
	MyRegEx reg2;
	wxFFile inifile;
	long seat2table = -1, seat6table = -1, seat8table = -1, seat9table = -1, seat10table = -1, seat4table = -1, seat7table = -1;
	unsigned int x=0;
	bool readprefset = true;

	wxString content = wxEmptyString;
	if(::wxFileExists(file))
	{
		if(inifile.Open(file, _T("r")))
		{
			reg.Compile(_T("SeatPref=\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*(\\d|-\\d)\\s*"), wxRE_ADVANCED | wxRE_ICASE);
			reg1.Compile(_T("AutoCenterTableSeat=1"), wxRE_ADVANCED | wxRE_ICASE);
			reg2.Compile(_T("AutoCenterTournSeat=1"), wxRE_ADVANCED | wxRE_ICASE);


			if(inifile.ReadAll(&content, wxConvAuto()))
			{
				if(reg.Matches(content))
				{
					reg.GetMatch(content, 1).ToLong(&seat2table);
					reg.GetMatch(content, 2).ToLong(&seat6table);
					reg.GetMatch(content, 3).ToLong(&seat8table);
					reg.GetMatch(content, 4).ToLong(&seat9table);
					reg.GetMatch(content, 5).ToLong(&seat10table);
					reg.GetMatch(content, 6).ToLong(&seat4table);
					reg.GetMatch(content, 7).ToLong(&seat7table);

					switch (GetTableMax())
					{
					case 2:
						m_nPrefSeat = (int)seat2table;
						break;

					case 6:
						m_nPrefSeat = (int)seat6table;
						break;

					case 8:
						m_nPrefSeat = (int)seat8table;
						break;

					case 9:
						m_nPrefSeat = (int)seat9table;
						break;

					case 10:
						m_nPrefSeat = (int)seat10table;
						break;

					case 4:
						m_nPrefSeat = (int)seat4table;
						break;

					case 7:
						m_nPrefSeat = (int)seat7table;
						break;
					}

					if(m_nPrefSeat > -1)
						m_nPrefSeat++;
				}
				if(m_nPrefSeat < 0)
				{
					if(reg1.Matches(content) && (!istournament))
					{
						if(tablemax >= 9)
							m_nPrefSeat = 5;
						else if(tablemax >= 6)
							m_nPrefSeat = 3;
						else if(tablemax >= 2)
							m_nPrefSeat = 2;
					}
					if(reg2.Matches(content) && (istournament))
					{
						if(tablemax >= 9)
							m_nPrefSeat = 5;
						else if(tablemax >= 6)
							m_nPrefSeat = 3;
						else if(tablemax >= 2)
							m_nPrefSeat = 2;
					}
				}
			}
		}
		inifile.Close();
	}
}

// read fulltilt pref seat
/*
void ProviderConf::ReadFullTiltPrefSeat(wxString file)
{
wxFFile filetopen;
wxXmlDocument doc;
wxString filestream;
wxString ValueAutoRotate;
wxString valuetoget;
wxXmlNode *child;

if(doc.Load(file))
{
// start processing the XML file
//if (doc.GetRoot()->GetName().Lower() != "settings")
//m_nPrefSeat = ISFALSE;

// autorotate
child = doc.GetRoot()->GetChildren();
while (child) 
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _("Application"))
{
child = child->GetChildren();
while (child)
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _T("Preferences"))
{
child = child->GetChildren();
while (child)
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _T("AutoRotate"))
{
child = child->GetChildren();
child->GetAttribute(wxString("Value"), &ValueAutoRotate);
break;
}
child = child->GetNext();
}
}
child = child->GetNext();
}
break;
}
child = child->GetNext();
}

// autorotate
}

//delete child;

if(ValueAutoRotate == _T("true"))
m_nPrefSeat = Definitions::AutoCenter::ISTRUE;
else
m_nPrefSeat = Definitions::AutoCenter::ISFALSE;
}*/

// read fulltilt layout
/*void ProviderConf::ReadFullTiltRaceTrackLayout(wxString file)
{
wxFFile filetopen;
wxXmlDocument doc;
wxString filestream;
wxString ValueRacetrack;
wxString valuetoget;
wxXmlNode *child;

if(doc.Load(file))
{
// tablestyle racetrack ?
child = doc.GetRoot()->GetChildren();
while (child) 
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _("Table"))
{
child = child->GetChildren();
while (child)
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _T("Display"))
{
child = child->GetChildren();
while (child)
{
child->GetAttribute(_T("Name"), &valuetoget);
if(valuetoget == _T("Style"))
{
child = child->GetChildren();
child->GetAttribute(wxString("Value"), &ValueRacetrack);
break;
}
child = child->GetNext();
}
}
child = child->GetNext();
}
break;
}
child = child->GetNext();
}
// tablestyle racetrack ?
}

//delete child;

if(ValueRacetrack == _T("0"))
m_nStandardTableLayout = 0;
else
m_nStandardTableLayout = 1;
}*/

// get the number of pref seat
int ProviderConf::GetPrefSeatNumber()
{
	return m_nPrefSeat;
}

// get install directory of pokerstars
wxString ProviderConf::GetPokerStarsInstallDirectory()
{
	//
	wxString path;
	wxRegKey* regkey = new wxRegKey(_T("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\PokerStars\\"));
	wxRegKey* regkey2 = new wxRegKey(_T("HKEY_LOCAL_MACHINE\\SOFTWARE\\Classes\\pokerstars\\DefaultIcon\\"));

	if(regkey->Exists())
	{
		regkey->Open(wxRegKey::AccessMode::Read);
		if(regkey->IsOpened())
		{
			if(regkey->QueryValue(_T("InstallLocation"), path))
			{
				path.Append(_T("\\"));
			}
		}
	}
	else if(regkey2->Exists())
	{
		regkey2->Open(wxRegKey::AccessMode::Read);
		if(regkey2->IsOpened())
		{
			path = regkey2->QueryDefaultValue();
			path = path.Left(path.Find(_T("PokerStars.exe")));
		}
	}
	else
	{
		path = wxEmptyString;
	}


	delete regkey;
	delete regkey2;
	return path;
}

// get install directory of fulltilt poker
/*wxString ProviderConf::GetFullTiltInstallDirectory()
{
//
wxString path;
wxRegKey* regkey = new wxRegKey(_T("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Full Tilt Poker\\Full Tilt Poker\\"));

if(regkey->Exists())
{
regkey->Open(wxRegKey::AccessMode::Read);
if(regkey->IsOpened())
{
if(regkey->QueryValue(_T("BinaryImage"), path))
{
path = path.Left(path.Find(_T("FullTiltPoker.exe")));
}
}
}
else
{
path = wxEmptyString;
}

delete regkey;

return path;
}
*/

// get app data path
/*wxString ProviderConf::GetPokerStarsAppData()
{
wxString path;

path = wxGetApp().m_Common.GetAppDataDir();
path.Replace(_T("PokerReader\\"), wxEmptyString);
path.Replace(_T("PokerReaderHUD\\"), wxEmptyString);
path.Append(_T("PokerStars\\"));

return path;
}*/

/*
// get path to pokerstars handhistory
wxString ProviderConf::GetPokerStarsHandHistory()
{
wxFFile conffile;
wxString inifile;
wxString userini;
wxString userini2;
wxString hhpath;
wxString content;
MyRegEx reg;
wxFileName hhdir;
wxDir directory;
userini = GetPokerStarsAppData();
userini.Append(_T("user.ini"));

userini2 = GetPokerStarsInstallDirectory();
userini2.Append(_T("user.ini"));

if(::wxFileExists(userini))
{
inifile = userini;
}
else if(::wxFileExists(userini2))
{
inifile = userini2;
}

if((!conffile.IsOpened()) && (!inifile.IsEmpty()))
{
conffile.Open(inifile, _T("r"));
conffile.ReadAll(&content, wxConvAuto());
if (reg.Compile(_T("(?w)SaveMyHandsPath=(.*?)\n{1}"), wxRE_ICASE | wxRE_ADVANCED))
{
if(reg.Matches(content))
{
hhpath = reg.GetMatch(content, 1);
}
else
{
hhpath = GetPokerStarsInstallDirectory();
hhpath.Append(_T("HandHistory\\"));
}
}
else
{
hhpath = GetPokerStarsInstallDirectory();
hhpath.Append(_T("HandHistory\\"));
}
}
conffile.Close();
return hhpath;
}
*/

// get path to import directory
wxString ProviderConf::GetPokerStarsManualImportDirectory()
{
	wxString path, importpath = wxEmptyString;
	if (Config != NULL)
	{
		path = wxString::Format(_T("/import"));
		if( Config->HasGroup(path))
		{
			Config->SetPath(path);
			Config->Get();
			importpath = Config->Read(_T("path"));
		}
		return importpath;
	}
	else
		return wxEmptyString;
}

// set path to import directory
bool ProviderConf::SetPokerStarsManualImportDirectory(wxString importpath)
{
	bool result = false;
	wxString path;
	path = wxString::Format(_T("/import"));
	Config->SetPath(path);
	result = Config->Write(_T("path"), importpath);
	Config->Flush();
	return result;
}