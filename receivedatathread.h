#ifndef RECEIVEDATA_H
#define RECEIVEDATA_H

#include <ctime>

class TableData;
class HudData;

#pragma once
class ReceiveDataThread
{
public:
	ReceiveDataThread(void);

	~ReceiveDataThread(void);

	void ReceiveHudData();
	void RestoreTableData(HudData &s, const wchar_t * filename);
	void ReceiveConfigData();
	void ParseAndSetConfigData(std::wstring configstring);
	//void CopyTableData(TableData &dest, TableData *source);

private:

	std::time_t data_lastwritetime;
	std::time_t data_currenttime;

	std::time_t conf_lastwritetime;
	std::time_t conf_currenttime;
};

#endif // RECEIVEDATA_H