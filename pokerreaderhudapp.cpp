/////////////////////////////////////////////////////////////////////////////
// Name:        pokerreaderhudapp.cpp
// Purpose:
// Author:
// Modified by:
// Created:     14/05/2009 09:39:46
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "pokerreaderhudapp.h"
//#include "otherclasses/client.h"
#include "../pokerreadercommon/helper/imagehandler.h"
//#include "../pokerreadercommon/helper/wxXmlConfig.h"
#include <wx/fileconf.h>
#include "../pokerreadercommon/helper/applicationpaths.h"
#include <sstream>
#include <boost/thread.hpp>
#include "receivedatathread.h"
#include <boost/interprocess/ipc/message_queue.hpp>

////@begin XPM images
////@end XPM images

/*!
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( PokerReaderHUDApp )
////@end implement app

/*!
 * PokerReaderHUDApp type definition
 */

IMPLEMENT_CLASS( PokerReaderHUDApp, wxApp )

/*!
 * PokerReaderHUDApp event table definition
 */

BEGIN_EVENT_TABLE( PokerReaderHUDApp, wxApp )

////@begin PokerReaderHUDApp event table entries
////@end PokerReaderHUDApp event table entries
	EVT_TIMER(ID_TIMER, PokerReaderHUDApp::OnTimer)
END_EVENT_TABLE()

/*!
 * Constructor for PokerReaderHUDApp
 */

PokerReaderHUDApp::PokerReaderHUDApp()
{
    Init();
}

/*!
 * Member initialisation
 */

void PokerReaderHUDApp::Init()
{
////@begin PokerReaderHUDApp member initialisation
////@end PokerReaderHUDApp member initialisation

	// finally start the timer, 10 seconds
	//DB.Connect();

	m_timer.SetOwner(this, ID_TIMER);
	//CLIENT = NULL;

	m_bExtendedHud = false;

	receiveconfigthread = NULL;
	receivedatathread = NULL;

	// MOD 22.08.09  JS: timer creation failed ?????
	//m_timer.Start(5000, false);
}

/*!
 * Initialisation for PokerReaderHUDApp
 */

bool PokerReaderHUDApp::OnInit()
{
	m_OverlayOutsize = 200;

	//wxSystemOptions::SetOption(wxT("msw.window.no-clip-children"),wxT("1"));

	// quit if another instance is running
	if(AnotherInstanceIsRunning())
		return false;

#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif

	Images = new PokerReaderImageHandler();

	m_receivedata = new ReceiveDataThread();

	LoadLanguage();

	SetupLog();

	// disable, because there are more than one toplevelwindows
	SetExitOnFrameDelete(false);

	// set config object
	ApplicationPaths Paths;
	//wxFileConfig* pConfig = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), Paths.GetUserConfigDir() + wxT("config.ini"), Paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);
	//wxLogMessage(wxT("Init Config"));
	//wxConfig::Set(pConfig);
	//delete pConfig;
	//m_Common.InitImages();

	// new client
	//CLIENT = new CommunicationClient();

	//wxLogMessage(wxT("Created Client"));

	/*
	if(StartClient())
	{
		wxLogMessage(wxT("Client connected!"));
		//StartAdvises();
	}
	*/

	RequestConfigFromGui(); // need config, request from gui. GUI sends config after incoming call ;-)
	wxSleep(5);
	m_timer.Start(500);

	wxLogMessage(wxT("Created Timer"));

	// Search for pokerreader MOD 22.08.09 - JS
	//Finder.SearchForPokerReader();

	/*if (!Finder.PokerReaderAppFound()) // MOD 22.08.09 JS: main app not found
	{
		wxExit();
	}
	else
	{
		// start the timer
		m_timer.Start(5000);
	}*/

	/*
////@begin PokerReaderHUDApp initialisation
	// Remove the comment markers above and below this block
	// to make permanent changes to the code.

#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif
////@end PokerReaderHUDApp initialisation
	*/

	//m_MainWindow = new MainFrame();

    return true;
}

/*!
 * Cleanup for PokerReaderHUDApp
 */
int PokerReaderHUDApp::OnExit()
{
	/*CLIENT->GetConnection()->StopAdvise(IPC_ITEM_DATA);
	CLIENT->GetConnection()->StopAdvise(IPC_ITEM_CONFIG);
	CLIENT->GetConnection()->StopAdvise(IPC_ITEM_CLOSE);
	CLIENT->Disconnect();
*/

	wxLogMessage(wxT("OnExit"));

////@begin PokerReaderHUDApp cleanup
	return wxApp::OnExit();
////@end PokerReaderHUDApp cleanup
}

// timer triggered
void PokerReaderHUDApp::OnTimer(wxTimerEvent& event)
{
	/* original
	Finder.ScanPokerProcesses();
	Finder.CleanUpWindows();
	*/

	// MOD 22.08.09 JS: timer stops, scan for poker processes, search pokerreader, not found = exit, else run
	m_timer.Stop();

	try
	{
		m_receivedata->ReceiveConfigData();
		m_receivedata->ReceiveHudData();
	}
	catch(...)
	{
	}
	
	//Finder.ScanPokerProcesses();
	Finder.CleanUpWindows();

	// start the client if not connected

	/*if(!CLIENT->IsConnected())
	{
		if(!StartClient())
		{
			wxLogMessage(wxT("EXIT"));
			wxExit();
		}
		else
		{
			StartAdvises();
		}
	}*/

	m_timer.Start(500);
	wxLogMessage(wxT("Timer restarted"));
}

// checks for another instance of this app
bool PokerReaderHUDApp::AnotherInstanceIsRunning()
{
	const wxString name = wxString::Format(_T("%s-%s"), wxGetApp().GetAppName(), wxGetUserId().c_str());
	m_InstanceChecker = new wxSingleInstanceChecker(name);
	if( m_InstanceChecker->IsAnotherRunning() )
	{
		::wxExit();
		return true;
	}
	else
	{
		return false;
	}
}

PokerReaderHUDApp::~PokerReaderHUDApp()
{
	if(receivedatathread != NULL)
	{
		receivedatathread->interrupt();
		receivedatathread->join();
		delete receivedatathread;
	}

	if(receiveconfigthread != NULL)
	{
		receiveconfigthread->interrupt();
		receiveconfigthread->join();
		delete receiveconfigthread;
	}

	delete m_receivedata;
}

void PokerReaderHUDApp::ReadConfigAndUpdate()
{
	int i = 0;
	for(i=0; i < Finder.Containers.Count(); i++)
	{
		Finder.Containers.Item(i)->ReadConfig();
		Finder.Containers.Item(i)->UpdatePlayerOverlayData();
	}
}

void PokerReaderHUDApp::SetConfigSetting(ConfigRecord * configRec)
{
	ConfigSetting[configRec->settingname].datatype = configRec->datatype;
	ConfigSetting[configRec->settingname].value = configRec->value;
	ConfigSetting[configRec->settingname].settingname = configRec->settingname;

	ReadConfigAndUpdate();
}

// setup logging
void PokerReaderHUDApp::SetupLog()
{
	wxString day, month, year;
	wxString logfilename;

	ApplicationPaths LogPaths;

	// setup logging
	wxDateTime m_logDt = wxDateTime::UNow();

	if(m_logDt.GetDay() < 10)
	{
		day = wxString::Format(_T("0%d"), m_logDt.GetDay());
	}
	else
	{
		day = wxString::Format(_T("%d"), m_logDt.GetDay());
	}

	if(m_logDt.GetMonth() < 10)
	{
		month = wxString::Format(_T("0%d"), m_logDt.GetMonth()+1);
	}
	else
	{
		month = wxString::Format(_T("%d"), m_logDt.GetMonth()+1);
	}

	// TODO Fix the code
	logfilename = wxString::Format(_T("%spokerreaderhud_%s-%s-%d.txt"), LogPaths.GetLogDir(), day, month, m_logDt.GetYear());

	// Initialising a persistant logging to file
	m_logFile = fopen(logfilename.GetData(), "w+");
	m_StandardLog = new wxLogStderr(m_logFile);
	wxLog::SetActiveTarget(m_StandardLog);
}

// log message
void PokerReaderHUDApp::Log(const wxString& logmsg)
{
	if(m_bEnableLogging)
	{
		wxLogMessage(wxString::Format(_T("**********\n\n%s\n**********\n\n"), logmsg));
	}
}

/*
bool PokerReaderHUDApp::StartClient()
{
	return CLIENT->Connect(IPC_HOST, IPC_SERVICE, IPC_TOPIC);
}*/

// search and overlay the window
void PokerReaderHUDApp::SearchAndOverlay(wxString table, int provider)
{
	wxLogMessage(wxT("Searching tables"));
	Finder.SearchAndOverlay(table, provider);
}

PokerReaderImageHandler* PokerReaderHUDApp::GetImages()
{
	return Images;
}

// sends the config data to the main gui process
void PokerReaderHUDApp::SendConfigSettingToGui( ConfigRecord setting )
{
	std::wostringstream streamstr;
	namespace ipc = boost::interprocess;

	streamstr << L"CONFIG:";
	streamstr << setting.settingname;
	streamstr << L"|";
	streamstr << setting.datatype;
	streamstr << L"|";
	streamstr << setting.value;

	size_t size = ((streamstr.str().length()) * sizeof(streamstr.str().data()) );
	try
	{
		ipc::message_queue::remove(MQ_GUICOMMAND);
		ipc::message_queue mq(ipc::create_only, MQ_GUICOMMAND, 1, size);
		mq.try_send(streamstr.str().data(), size, 1000);
	}
	catch (ipc::interprocess_exception &e)
	{
	}
}

// request config from main gui
void PokerReaderHUDApp::RequestConfigFromGui()
{
	std::wostringstream streamstr;
	namespace ipc = boost::interprocess;

	streamstr << L"SENDCONFIGTOHUD";
	size_t size = ((streamstr.str().length()) * sizeof(streamstr.str().data()) );
	try
	{
		ipc::message_queue::remove(MQ_GUICOMMAND);
		ipc::message_queue mq(ipc::create_only, MQ_GUICOMMAND, 1, size);
		mq.try_send(streamstr.str().data(), size, 1000);
	}
	catch (ipc::interprocess_exception &e)
	{
	}
}

void PokerReaderHUDApp::StartAdvises()
{
	/*wxLogMessage(wxT("Create Advises ..."));

	CLIENT->GetConnection()->StartAdvise(IPC_ITEM_DATA);
	wxLogMessage(wxT("Advise IPC_ITEM_DATA created"));

	CLIENT->GetConnection()->StartAdvise(IPC_ITEM_CONFIG);
	wxLogMessage(wxT("Advise IPC_ITEM_CONFIG created"));

	CLIENT->GetConnection()->StartAdvise(IPC_ITEM_CLOSE);
	wxLogMessage(wxT("Advise IPC_ITEM_CLOSE created"));

	wxLogMessage(wxT("Creation of Advises - OK"));
	RequestConfigFromGui();*/
}

ConfigRecord PokerReaderHUDApp::BuildConfigDataForHUD(std::wstring settingname, int value)
{
	ConfigRecord confRec;
	std::wostringstream ss;

	ss << value;

	confRec.settingname = settingname;
	confRec.datatype = L"int";
	confRec.value = ss.str();

	return confRec;
}

ConfigRecord PokerReaderHUDApp::BuildConfigDataForHUD(std::wstring settingname, double value)
{
	ConfigRecord confRec;
	std::wostringstream ss;

	ss << value;

	confRec.settingname = settingname;
	confRec.datatype = L"double";
	confRec.value = ss.str();

	return confRec;
}

ConfigRecord PokerReaderHUDApp::BuildConfigDataForHUD(std::wstring settingname, bool value)
{
	ConfigRecord confRec;

	confRec.settingname = settingname;
	confRec.datatype = L"bool";

	if(value)
		confRec.value = L"true";
	else
		confRec.value = L"false";

	return confRec;
}

ConfigRecord PokerReaderHUDApp::BuildConfigDataForHUD(std::wstring settingname, std::wstring value)
{
	ConfigRecord confRec;

	confRec.settingname = settingname;
	confRec.datatype = L"string";
	confRec.value = value;

	return confRec;
}

void PokerReaderHUDApp::SetHudData(HudData huddata )
{	
	wxLogMessage(L"Searching and overlaying...");
	if(huddata.TableData.tournamentname.empty())
	{
		OverlayData[huddata.TableData.tablename] = huddata;
		SearchAndOverlay(wxString(huddata.TableData.tablename), huddata.provider);
	}
	else
	{
		OverlayData[huddata.TableData.tournamentname] = huddata;
		SearchAndOverlay(wxString(huddata.TableData.tournamentname), huddata.provider);
	}
}

void PokerReaderHUDApp::LoadLanguage()
{
	ApplicationPaths paths;
	long m_AppLanguage;
	wxFileConfig * pConfig = new wxFileConfig(_T("PokerReader"), _T("Sharktoolz"), paths.GetUserConfigDir() + wxT("config.ini"), paths.GetAppDir() + wxT("config.ini"), wxCONFIG_USE_LOCAL_FILE);
	wxConfig::Set(pConfig);

	if(!pConfig->Exists(CONF_GUILANGUAGE))
	{
		m_AppLanguage = wxLANGUAGE_ENGLISH;
	}
	else
	{
		if(!wxConfig::Get()->Read(CONF_GUILANGUAGE, &m_AppLanguage))
			m_AppLanguage = wxLANGUAGE_ENGLISH;
	}

	if(wxLocale::IsAvailable(m_AppLanguage))
	{
		//m_Locale = new wxLocale( m_AppLanguage, wxLOCALE_CONV_ENCODING );
		m_Locale = new wxLocale(m_AppLanguage);

#ifdef __WXGTK__
		// add locale search paths
		m_Locale->AddCatalogLookupPathPrefix(wxT("/usr"));
		m_Locale->AddCatalogLookupPathPrefix(wxT("/usr/local"));
		wxStandardPaths* paths = (wxStandardPaths*) &wxStandardPaths::Get();
		wxString prefix = paths->GetInstallPrefix();
		m_Locale->AddCatalogLookupPathPrefix( prefix );
#elif defined(__WXMSW__)
		m_Locale->AddCatalogLookupPathPrefix(wxString::Format(_T("%s\\lang\\"), wxStandardPaths::Get().GetDataDir()));
#endif

		m_Locale->AddCatalog(wxT("pokerreader"));

		if(! m_Locale->IsOk() )
		{
			std::cerr << "selected language is wrong" << std::endl;
			delete m_Locale;
			m_Locale = new wxLocale( wxLANGUAGE_ENGLISH );
			m_AppLanguage = wxLANGUAGE_ENGLISH;
		}
	}
	else
	{
		std::cout << "The selected language is not supported by your system."
			<< "Try installing support for this language." << std::endl;
		m_Locale = new wxLocale( wxLANGUAGE_ENGLISH );
		m_AppLanguage = wxLANGUAGE_ENGLISH;
	}

	delete pConfig;
}

#ifdef _DEBUG
void PokerReaderHUDApp::GenerateReport(wxDebugReport::Context ctx)
{
    wxDebugReportCompress *report = new wxDebugReportCompress;

    // add all standard files: currently this means just a minidump and an
    // XML file with system info and stack trace
    report->AddAll(ctx);

    // you can also call report->AddFile(...) with your own log files, files
    // created using wxRegKey::Export() and so on, here we just add a test
    // file containing the date of the crash
    wxFileName fn(report->GetDirectory(), _T("timestamp.my"));
    wxFFile file(fn.GetFullPath(), _T("w"));
    if ( file.IsOpened() )
    {
        wxDateTime dt = wxDateTime::Now();
        file.Write(dt.FormatISODate() + _T(' ') + dt.FormatISOTime());
        file.Close();
    }

    report->AddFile(fn.GetFullName(), _("timestamp of this report"));

    // can also add an existing file directly, it will be copied
    // automatically
#ifdef __WXMSW__
    //report->AddFile(_T("c:\\autoexec.bat"), _T("DOS startup file"));
#else
    //report->AddFile(_T("/etc/motd"), _T("Message of the day"));
#endif

    // calling Show() is not mandatory, but is more polite
    if ( wxDebugReportPreviewStd().Show(*report) )
    {
        if ( report->Process() )
        {
			wxLogMessage(wxString::Format(_T("Report generated in \"%s\"."), report->GetCompressedFileName().c_str()));
			report->Reset();
		}
    }
    //else: user cancelled the report

    delete report;
}
#endif

void PokerReaderHUDApp::OnFatalException()
{
#ifdef _DEBUG
	wxLogMessage(_T("Fatal error"));
	GenerateReport(wxDebugReport::Context_Exception);
#endif
}

void PokerReaderHUDApp::OnUnhandledException()
{
#ifdef _DEBUG
	wxLogMessage(_T("Unhandled error"));
	GenerateReport(wxDebugReport::Context_Exception);
#endif
}
